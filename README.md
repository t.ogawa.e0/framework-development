**Framework Development**
====

**Overview**
====

*開発中のフレームワークです。*

**Requirement**
====

*  [IDE (Integrated Development Environment)](https://my.visualstudio.com/Downloads?q=Visual%20Studio%202017&pgroup=) - Visual Studio 2017
*  [End-User Runtime](https://www.microsoft.com/ja-jp/download/details.aspx?id=34429) - DirectX 9.0c End-User Runtime
*  [Effekseer](https://effekseer.github.io/) - Effekseer
*  [ImGui](https://github.com/ocornut/imgui) - ImGui
##

![VisualStudio2017](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/VisualStudio2017.png)
![c++](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/C%EF%BC%8B%EF%BC%8B.png)
![DirectX9SDK](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/DirectX9SDK.png)
![Effekseer](https://gitlab.com/t.ogawa.e0/asset/raw/master/ICO/Effekseer.png)