//==========================================================================
// トゥーンシェーダー [ToonShader.fx]
// author: tatsuya ogawa
//==========================================================================

//==========================================================================
// グローバル変数
//==========================================================================
float4x4 World; // ワールド変換行列
float4x4 View; // カメラ変換行列
float4x4 Projection; // プロジェクション変換行列

bool TexFlag; // テクスチャのありなし false:なし true:あり
float4 CameraPos; // カメラ位置

// 光
float4 Diffuse; // ディフューズ
float4 Ambient; // 環境光
float4 Specular; // スペキュラー
float4 Emmisive; // エミッシブ

float3 LightDir; // 平行光源の方向

// マテリアル
float4 DiffuseMatrix; // ディフューズ光
float4 EmmisiveMatrix; // エミッシブ光
float4 AmbientMatrix; // 環境光
float4 SpecularMatrix; // スペキュラー
float Power; // スペキュラー光のパワー値

texture Texture; // テクスチャ
texture ToonTexture; // テクスチャ

//==========================================================================
// サンプラー
//==========================================================================
sampler Sampler = sampler_state
{
    texture = <Texture>;
    MinFilter = LINEAR; // リニアフィルタ（縮小時）
    MagFilter = LINEAR; // リニアフィルタ（拡大時）
    AddressU = CLAMP;
    AddressV = CLAMP;
};

//==========================================================================
// サンプラー2(トゥーンテクスチャ用サンプラー)
//==========================================================================
sampler ToonSampler = sampler_state
{
    texture = <ToonTexture>;
    MinFilter = LINEAR; // リニアフィルタ（縮小時）
    MagFilter = LINEAR; // リニアフィルタ（拡大時）
    AddressU = CLAMP;
    AddressV = CLAMP;
};

struct VS_OUTPUT
{
    float4 pos : POSITION;
    float2 tex : TEXCOORD0;
    float3 normal : TEXCOORD1;
    float3 posforps : TEXCOORD2;
};

//==========================================================================
// 頂点シェーダ
//==========================================================================
VS_OUTPUT main(
in float3 in_pos : POSITION,
in float3 in_normal : NORMAL,
in float2 in_tex1 : TEXCOORD0)
{
    VS_OUTPUT Out;

    // 座標変換
    Out.pos = mul(float4(in_pos, 1.0f), World);
    Out.posforps = Out.pos.xyz;
    Out.pos = mul(Out.pos, View);
    Out.pos = mul(Out.pos, Projection);

    // テクスチャ座標をそのまま出力する
    Out.tex = in_tex1;

    // 法線をワールド空間上のベクトルに変換して、単位ベクトル化
    Out.normal = normalize(mul(in_normal, (float3x3) World));

    return Out;
}

//==========================================================================
// ピクセルシェーダ
//==========================================================================
float4 psToonShader(in VS_OUTPUT In) : COLOR0
{
    float3 N; // ワールド空間上の法線ベクトル
    float3 L; // 光の差し込む方向	
    float3 P; // 頂点座標
    float3 V; // 視線ベクトル
    float3 H; // ハーフベクトル
    float4 Out; // 色

    P = In.posforps;
    N = normalize(In.normal);

	// 平行光の差し込む方向	単位ベクトル化
    L = normalize(-LightDir);

    // 視線ベクトルを求める
    V = CameraPos.xyz - P; // カメラ座標-頂点座標

    // 正規化
    V = normalize(V);

    // ハーフベクトルを求める
    H = normalize(V + L);

    // ハーフランバート
    float d = dot(L, N);
    d = d * 0.5f + 0.5f;
    d = d * d;

    // 出力カラーを決める
    float4 diffuse = Ambient * AmbientMatrix + Diffuse * DiffuseMatrix * d;

    float s = pow(max(0, dot(N, H)), 300);

    // スペキュラー光による色を計算
    float4 specular = Specular * SpecularMatrix * s;

    float2 toonuv;
    toonuv.y = d;
    toonuv.x = 0.0f;

    float4 toon = tex2D(ToonSampler, toonuv);

    if (TexFlag)
    {
        float4 tex_color = tex2D(Sampler, In.tex);

        // テクスチャの色と頂点の色を掛け合わせる
        Out = saturate(toon * diffuse * tex_color + specular);
    }
    else
    {
        Out = saturate(toon * diffuse + specular);
    }

    return Out;

}


//==========================================================================
// vs Anime paint
//==========================================================================
void vsAnimepaint(
in float4 in_pos : POSITION,
in float3 in_normal : NORMAL,
out float4 out_pos : POSITION)
{
    float3 P;
    
	// 法線ベクトルを正規化
    in_normal = normalize(in_normal);

	// 法線方向に頂点座標を0.03大きくする
    P = (float3) in_pos + in_normal * 0.001f; // ※法線方向に大きく処理する計算を入れる
	// ワールド、カメラ、プロジェクション変換
    out_pos = mul(float4(P, 1.0f), World);
    out_pos = mul(out_pos, View);
    out_pos = mul(out_pos, Projection);
}

//==========================================================================
// ps Anime paint
//==========================================================================
void psAnimepaint(out float4 out_color : COLOR0)
{
    out_color = float4(0.3f, 0.3f, 0.3f, 0.5f); // 輪郭線の色
}

//==========================================================================
// シェーダーのパッケージ
//==========================================================================
technique ToonShader
{
    pass P0
    {
        // 使用するシェーダーを渡す
        VertexShader = compile vs_3_0 main();
        PixelShader = compile ps_3_0 psToonShader();
        
        // Effect States
        AlphaBlendEnable = true;
        SrcBlend = SRCALPHA;
        DestBlend = INVSRCALPHA;
    }

    pass P1
    {
        // 使用するシェーダーを渡す
        VertexShader = compile vs_3_0 vsAnimepaint();
        PixelShader = compile ps_3_0 psAnimepaint();
        
        // Effect States
        AlphaBlendEnable = false;
        SrcBlend = SRCALPHA;
        DestBlend = INVSRCALPHA;
    }
}