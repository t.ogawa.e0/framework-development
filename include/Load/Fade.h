//==========================================================================
// フェード[Fade.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

//==========================================================================
//
// class  : CFade
// Content: フェード
//
//==========================================================================
class CFade : public mslib::ObjectManager
{
private:
    // パラメータ
    class CParam
    {
    public:
        CParam()
        {
            m_a = 0;
            m_Change = false;
            m_Key = false;
            m_Draw = false;
            m_In = false;
        }
        ~CParam() {}
    public:
        int m_a; // α
        bool m_Change; // change
        bool m_Key; // 鍵
        bool m_Draw; // 描画判定
        bool m_In; // フェードイン判定
    };
public:
    CFade() :ObjectManager("Fade") {}
    ~CFade() {}
    // 初期化
    void Init(void)override;
    // 更新
    void Update(void)override;
    // デバッグ
    void Debug(void)override;
    // フェードイン
    void In(void);
    // フェードアウト
    void Out(void);
    // フェードイン終了判定
    bool FeadInEnd(void);
    bool GetDraw(void) { return m_Param.m_Draw; }
private:
    CParam m_Param; // パラメータ
};
