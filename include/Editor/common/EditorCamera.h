//==========================================================================
// カメラ [EditorCamera.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Editor
{
    //==========================================================================
    //
    // class  : EditorCamera
    // Content: カメラ
    //
    //==========================================================================
    class EditorCamera : public mslib::ObjectManager
    {
    public:
        EditorCamera();
        ~EditorCamera();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;

        // カメラ取得
        mslib::camera::Camera * GetCamera();
    private:
        // 操作
        void Control();
    };
}

