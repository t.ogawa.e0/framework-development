//==========================================================================
// カメラ [EditorCamera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "EditorCamera.h"

namespace Editor
{
    EditorCamera::EditorCamera() : ObjectManager("EditorCamera")
    {
    }

    EditorCamera::~EditorCamera()
    {
    }

    /**
    @brief 初期化
    */
    void EditorCamera::Init()
    {
        D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 2.0f, -3.0f); // 注視点
        D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 1.0f, 0.0f); // カメラ座標

        auto obj1 = Camera()->Create();
        obj1->Init(Eye, At);
        obj1->SetComponentName(mslib::text("%p", obj1));
        obj1->IsMainCamera();

        auto obj2 = Object()->Create();
        obj2->SetScale(-0.8f, -0.8f, -0.8f);
        obj2->SetComponentName(mslib::text("%p", obj2));
    }

    /**
    @brief 更新
    */
    void EditorCamera::Update()
    {
        Control();
    }

    /**
    @brief デバッグ
    */
    void EditorCamera::Debug()
    {
    }

    // カメラ取得
    mslib::camera::Camera * EditorCamera::GetCamera()
    {
        return Camera()->Get(0);
    }

    // 操作
    void EditorCamera::Control()
    {
        auto obj1 = Camera()->Get(0);
        auto obj2 = Object()->Get(0);

        if (obj1 != obj1->GetCamera())return;

        // ホイールを押したときの処理
        if (GetDInputMouse()->Press(MouseButton::Wheel))
        {
            obj1->AddPosition(-(float)GetDInputMouse()->speed().m_lX*0.01f, (float)GetDInputMouse()->speed().m_lY*0.01f, 0);
        }

        // 右クリックの時の処理
        if (GetDInputMouse()->Press(MouseButton::Right))
        {
            obj1->AddViewRotation((float)GetDInputMouse()->speed().m_lX*0.0025f, (float)GetDInputMouse()->speed().m_lY*0.0025f, 0);
            obj1->AddCameraRotation((float)GetDInputMouse()->speed().m_lX*0.0025f, (float)GetDInputMouse()->speed().m_lY*0.0025f, 0);
            obj2->AddRotation((float)GetDInputMouse()->speed().m_lX*0.005f, 0, 0);
        }

        // マウスの移動速度を加算
        if (obj1->DistanceFromView((float)GetDInputMouse()->speed().m_lZ*0.0025f) < 8.0f)
        {
            obj1->DistanceFromView(-(float)GetDInputMouse()->speed().m_lZ*0.0025f);
        }

        // 右クリック&左クリックの時の処理
        if (GetDInputMouse()->Press(MouseButton::Right) && GetDInputMouse()->Press(MouseButton::Left))
        {
            obj1->AddPosition(0, 0, 0.05f);
        }

        // 視点の移動
        obj2->SetPosition(obj1->GetLook1().at);
    }
}