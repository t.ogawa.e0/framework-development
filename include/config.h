//==========================================================================
// コンフィグ [config.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

#include <string>

namespace FolderStructure {
    const std::string __slash__ = std::string("/");
    const std::string __resource__ = std::string("resource") + __slash__;
    const std::string __level__ = __resource__ + std::string("level") + __slash__;
    const std::string __motion__ = __resource__ + std::string("motion") + __slash__;
}

namespace SceneLevel {
	const std::string __Sample1__ = FolderStructure::__level__ + std::string("Sample1");
	const std::string __Sample2__ = FolderStructure::__level__ + std::string("Sample2");
	const std::string __Sample3__ = FolderStructure::__level__ + std::string("Sample3");
}

