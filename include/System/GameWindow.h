//==========================================================================
// ゲームウィンドウ[GameWindow.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <string>
#include "dxlib.h"

//==========================================================================
//
// class  : CGameWindow
// Content: ゲームウィンドウ
//
//==========================================================================
class CGameWindow : public mslib::component::Component
{
private:
    // コピー禁止 (C++11)
    CGameWindow(const CGameWindow &) = delete;
    CGameWindow &operator=(const CGameWindow &) = delete;
    CGameWindow &operator=(CGameWindow&&) = delete;
public:
    CGameWindow();
    CGameWindow(const std::string &class_name, const std::string &window_name);
    ~CGameWindow();
    // ウィンドウ生成
    int Window(HINSTANCE hInstance, const mslib::int2 & data, bool Mode, int nCmdShow);
private:
    // ウィンドウプロシージャ
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
    // ゲームループ
    int GameLoop(HINSTANCE hInstance, HWND hWnd);
};
