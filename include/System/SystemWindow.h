//==========================================================================
// システムウィンドウ[SystemWindow.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <stdio.h>
#include <sal.h>
#include <vector>
#include "dxlib.h"

//==========================================================================
//
// class  : CSystemWindow
// Content: システムウィンドウ
//
//==========================================================================
class CSystemWindow : public mslib::component::Component
{
private:
    class CData {
    public:
        CData() {
            m_serect = 0;
            m_key = 0;
        };
        CData(int serect, bool key) {
            m_serect = serect;
            m_key = key;
        }
    public:
        int m_serect;
        bool m_key;
    };
public:
    CSystemWindow();
    CSystemWindow(const std::string &class_name, const std::string &window_name);
    ~CSystemWindow();

    // ウィンドウ生成
    bool Window(HINSTANCE hInstance, int nCmdShow);

    // true の時はフルスクリーン
    bool GetWindowMode(void) {
        if (m_asp.Size() == m_data.m_serect) {
            return true;
        }
        return false;
    }

    // ウィンドウサイズの取得
    mslib::int2 GetWindowSize(void) {
        if (m_asp.Size() != m_data.m_serect) {
            return m_asp.Get(m_data.m_serect).size;
        }
        return mslib::int2(0, 0);
    }

    // ウィンドウモードのセット
    void SetAspectRatio(const mslib::AspectRatio::List & Input) {
        m_asp.Search(Input.size, Input.asp);
    }
private:
    static void newtex(void);
    // ボタン
    static void Button(HWND hWnd, LPARAM *lParam, int posx, int posy);
    // 設定
    static void Config(HDC * hDC, mslib::intTexvec * vpos);
    // テキスト描画
    static void Text(HWND hWnd, HDC * hDC, LPCTSTR lpszStr, int posx, int posy);
    // イメージデータ表示場所
    static void ImgeData(HDC * hDC, mslib::intTexvec * vpos);
    // テクスチャ読み込み
    static void LoadTex(LPARAM * lp);
    // テクスチャ描画
    static void DrawTex(HWND hWnd, HDC * hDC, mslib::intTexvec * vpos);
    // ウインドウプロシージャ
    static LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

    static BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
private:
    static HWND m_hWnd;
    static HWND m_combo;
    static HDC m_BackBufferDC;
    static HBITMAP m_BackBufferBMP;
    static HWND m_hWndButton10000;
    static HWND m_hWndButton10001;
    static HWND m_check;

    //ビットマップ
    static HBITMAP m_hBitmap;
    static BITMAP m_Bitmap;

    static CData m_data;

    static mslib::AspectRatio m_asp;
};
