//==========================================================================
// 平面のプリミティブコライダー [PlaneCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <vector>
#include "mslib.hpp"
#include "Component.h"
#include "Transform.h"
#include "Collider.h"

_MSLIB_BEGIN

namespace collider
{
    //==========================================================================
    //
    // class  : PlaneCollider
    // Content: 平面のプリミティブコライダー
    //
    //==========================================================================
    class PlaneCollider : public Collider
    {
    public:
        PlaneCollider();
        ~PlaneCollider();

        /**
        @brief 初期化
        */
        void Init()override;

        /**
        @brief 更新
        */
        void Update()override;

        /**
        @brief 描画
        */
        void Draw(LPDIRECT3DDEVICE9 device)override;

        /**
        @brief 上方向の衝突判定の有無
        */
        void UpCollider(bool flag);

        /**
        @brief 下方向の衝突判定の有無
        */
        void UnderCollider(bool flag);

        /**
        @brief 座標入力モードの有無
        */
        void SetPositionInputMode(bool flag);
    protected:
        /**
        @brief 判定処理
        */
        bool Judgment(
            const D3DXVECTOR3 & target,
            const D3DXVECTOR3 & v01,
            const D3DXVECTOR3 & v12,
            const D3DXVECTOR3 & v20,
            const D3DXVECTOR3 & v0p,
            const D3DXVECTOR3 & v1p,
            const D3DXVECTOR3 & v2p,
            const D3DXVECTOR3 & vpos,
            transform::Transform * obj
        );
    protected:
        bool m_upward; // 上方向
        bool m_under; // 下方向
        bool m_position_input_mode; // 座標入力モード
        std::vector<transform::Transform*> m_vertex; // 頂点
    };
}

_MSLIB_END