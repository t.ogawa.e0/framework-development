//==========================================================================
// 立方体のプリミティブコライダー [BoxCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "BoxCollider.h"

_MSLIB_BEGIN

namespace collider
{
    constexpr const char * __vertex_c__ = "vertex";
    constexpr float VCRCT = 0.5f;
    constexpr const int __total_number_of_vertices__ = 8; // 頂点数
    constexpr const int __number_of_vertices__ = 4; // 一面の頂点数
    constexpr const int __number_of_faces__ = 6; // 面の数

    const D3DXVECTOR3 __vertex_coordinates__[__total_number_of_vertices__] =
    {
        D3DXVECTOR3(-VCRCT, VCRCT,VCRCT),// 0
        D3DXVECTOR3(VCRCT, VCRCT,VCRCT), // 1
        D3DXVECTOR3(VCRCT,VCRCT,-VCRCT), // 2
        D3DXVECTOR3(-VCRCT,VCRCT,-VCRCT),// 3

        D3DXVECTOR3(-VCRCT, -VCRCT,VCRCT),// 4
        D3DXVECTOR3(VCRCT, -VCRCT,VCRCT), // 5
        D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT), // 6
        D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),// 7
    };
    struct VectorDrawData
    {
        D3DXVECTOR3 pos = D3DXVECTOR3(0, 0, 0); // 座標変換が必要
        D3DCOLOR color = D3DCOLOR_RGBA(255, 255, 255, 255); // ポリゴンの色
    };

    BoxCollider::BoxSprite::BoxSprite() {
        m_upward_collision = false;
        m_under_collision = false;
    }
    BoxCollider::BoxSprite::BoxSprite(transform::Transform * v1, transform::Transform * v2, transform::Transform * v3, transform::Transform * v4) {
        m_vertex.reserve(__number_of_vertices__);
        m_vertex.push_back(v1);
        m_vertex.push_back(v2);
        m_vertex.push_back(v3);
        m_vertex.push_back(v4);
        m_upward_collision = false;
        m_under_collision = false;
    }
    BoxCollider::BoxSprite::~BoxSprite() {
        m_vertex.clear();
    }

    bool BoxCollider::BoxSprite::Update(Collider * target)
    {
        m_upward_collision = false;
        m_under_collision = false;

        D3DXVECTOR3 v01, v12, v20, v0p, v1p, v2p;
        auto target_itr = target->GetWarrantyParent<transform::Transform>();
        if (target_itr == nullptr)return false;

        auto target_mat = target_itr->GetWorldMatrix();
        auto mat1 = m_vertex[0]->GetWorldMatrix();
        auto mat2 = m_vertex[1]->GetWorldMatrix();
        auto mat3 = m_vertex[2]->GetWorldMatrix();
        auto mat4 = m_vertex[3]->GetWorldMatrix();

        auto target_position = D3DXVECTOR3(target_mat->_41, target_mat->_42, target_mat->_43);
        auto v0 = D3DXVECTOR3(mat1->_41, mat1->_42, mat1->_43); 
        auto v1 = D3DXVECTOR3(mat2->_41, mat2->_42, mat2->_43); 
        auto v2 = D3DXVECTOR3(mat3->_41, mat3->_42, mat3->_43); 
        auto v3 = D3DXVECTOR3(mat4->_41, mat4->_42, mat4->_43); 

        // 強制的に処理を有効
        target_itr->UnlockUpdate();

        //==========================================================================
        // 頂点0,1,3の三角形の表面と裏面の処理
        //==========================================================================

        // 表面

        // 1 - 0
        v01 = v1 - v0;
        // 3 - 1
        v12 = v3 - v1;
        // 0 - 3
        v20 = v0 - v3;
        // 0
        v0p = target_position - v0;
        // 1
        v1p = target_position - v1;
        // 3
        v2p = target_position - v3;

        if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1))return true;

        // 裏面

        // 0
        v0p = v0 - target_position;
        // 1
        v1p = v1 - target_position;
        // 3
        v2p = v3 - target_position;

        if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1))return true;

        //==========================================================================
        // 頂点1,2,3の三角形の表面と裏面の処理
        //==========================================================================

        // 表面

        // 2 - 1
        v01 = v2 - v1;
        // 3 - 2
        v12 = v3 - v2;
        // 1 - 3
        v20 = v1 - v3;
        // 1
        v0p = target_position - v1;
        // 2
        v1p = target_position - v2;
        // 3
        v2p = target_position - v3;

        if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1))return true;

        // 裏面

        // 0
        v0p = v1 - target_position;
        // 1
        v1p = v2 - target_position;
        // 3
        v2p = v3 - target_position;

        if (Judgment(target_position, v01, v12, v20, v0p, v1p, v2p, v1))return true;

        return false;
    }

    bool BoxCollider::BoxSprite::Judgment(
        const D3DXVECTOR3 & target,
        const D3DXVECTOR3 & v01,
        const D3DXVECTOR3 & v12,
        const D3DXVECTOR3 & v20,
        const D3DXVECTOR3 & v0p,
        const D3DXVECTOR3 & v1p,
        const D3DXVECTOR3 & v2p,
        const D3DXVECTOR3 & vpos)
    {
        auto c0 = v01.x*v0p.z - v01.z*v0p.x;
        auto c1 = v12.x*v1p.z - v12.z*v1p.x;
        auto c2 = v20.x*v2p.z - v20.z*v2p.x;

        if (c0 <= 0.0f && c1 <= 0.0f && c2 <= 0.0f)
        {
            D3DXVECTOR3 N;

            D3DXVec3Cross(&N, &v01, &v12);
            float vec_y = vpos.y - (N.x * (target.x - vpos.x) + N.z * (target.z - vpos.z)) / N.y;

            if (target.y < vec_y)
            {
                m_under_collision = true;
            }
            if (vec_y < target.y)
            {
                m_upward_collision = true;
            }
        }
        return m_under_collision || m_upward_collision ? true : false;
    }

    BoxCollider::BoxCollider()
    {
        SetComponentName("BoxCollider");
        m_transform.reserve(__total_number_of_vertices__ + 1);
        m_sprite.reserve(__number_of_faces__);
    }

    BoxCollider::~BoxCollider()
    {
        m_sprite.clear();
    }

    void BoxCollider::Init()
    {
        auto target = GetWarrantyParent<transform::Transform>();
        if (target == nullptr)return;

        auto parent_vertex = target->AddComponent<transform::Transform>();
        parent_vertex->SetComponentName(GetComponentName() + "_Transform");
        m_transform.push_back(parent_vertex);

        for (int i = 0; i < __total_number_of_vertices__; i++)
        {
            auto obj = parent_vertex->AddComponent<transform::Transform>();
            obj->SetComponentName(__vertex_c__ + std::to_string(i));
            obj->SetPosition(__vertex_coordinates__[i]);
            m_transform.push_back(obj);
        }

        // 上面
        m_sprite.push_back(BoxSprite(m_transform[1], m_transform[2], m_transform[3], m_transform[4]));

        // 下面
        m_sprite.push_back(BoxSprite(m_transform[5], m_transform[6], m_transform[7], m_transform[8]));

        // 右面
        m_sprite.push_back(BoxSprite(m_transform[1], m_transform[5], m_transform[8], m_transform[4]));

        // 左面
        m_sprite.push_back(BoxSprite(m_transform[2], m_transform[6], m_transform[7], m_transform[3]));

        // 正面
        m_sprite.push_back(BoxSprite(m_transform[3], m_transform[4], m_transform[8], m_transform[7]));

        // 後方
        m_sprite.push_back(BoxSprite(m_transform[1], m_transform[2], m_transform[6], m_transform[5]));
    }

    void BoxCollider::Update()
    {
        if (!((int)m_sprite.size() == __number_of_faces__))return;

        // ターゲットの数だけ実行
        for (auto & target_itr : m_target_collider)
        {
            bool upward = false; // 上
            bool under = false; // 下

            if (target_itr->Trigger())continue;
            if (!target_itr->GetUpdateFlag())continue;

            // 面の数だけ実行
            for (auto & sprite_itr : m_sprite)
            {
                // 判定が出ない場合
                if (!sprite_itr.Update(target_itr))continue;

                if (sprite_itr.m_under_collision)
                    under = true;
                if (sprite_itr.m_upward_collision)
                    upward = true;

                // 上下判定が出ない
                if (!(upward&&under)) continue;

                m_collision = true;
                target_itr->SetTrigger(true);
                break;
            }
        }
    }

    void BoxCollider::Draw(LPDIRECT3DDEVICE9 device)
    {
        auto target = GetWarrantyParent<transform::Transform>();
        if (target == nullptr)return;
        if (m_transform.size() == 0)return;

        int size = __number_of_vertices__ + 1;
        auto *pVector = new VectorDrawData[size];

        device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);
        device->SetTransform(D3DTS_WORLD, m_transform[0]->GetWorldMatrix());
        device->SetTexture(0, nullptr);

        // 当たり判定が出たときだけ色を変える
        if (m_collision)
            for (int i = 0; i < size; i++)
                pVector[i].color = D3DCOLOR_RGBA(255, 0, 0, 255);

        // 面の数だけ実行
        for (auto & itr : m_sprite)
        {
            // 座標渡し
            for (int i = 0; i < (int)itr.m_vertex.size(); i++)
            {
                pVector[i].pos = *itr.m_vertex[i]->GetPosition();
            }

            pVector[__number_of_vertices__].pos = *itr.m_vertex[0]->GetPosition();
            device->DrawPrimitiveUP(D3DPT_LINESTRIP, __number_of_vertices__, pVector, sizeof(VectorDrawData));
        }

        delete[] pVector;
    }
    void BoxCollider::Copy(const BoxCollider * in)
    {
        // コピー元のトランスフォームを参照
        for (int i = 0; i < (int)in->m_transform.size(); i++)
        {
            m_transform[i]->SetParameter(*in->m_transform[i]->GetParameter());
        }
    }
}

_MSLIB_END