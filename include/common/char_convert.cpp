//==========================================================================
// Character code conversion [char_convert.cpp]
// author: tatsuya ogawa 
//==========================================================================
#include "char_convert.hpp"

_MSLIB_BEGIN

//==========================================================================
/**
@brief [C] string -> wstring
@param src [in] string
@return wstring
*/
std::wstring char_convert::multi_to_wide_capi(std::string const& src)
{
    std::size_t converted{};
    std::vector<wchar_t> dest(src.size(), L'\0');
    if (::_mbstowcs_s_l(&converted, dest.data(), dest.size(), src.data(), _TRUNCATE, ::_create_locale(LC_ALL, "jpn")) != 0) 
    {
        throw std::system_error{ errno, std::system_category() };
    }
    return std::wstring(dest.begin(), dest.end());
}

//==========================================================================
/**
@brief [Windows API] string -> wstring
@param src [in] string
@return wstring
*/
std::wstring char_convert::multi_to_wide_winapi(std::string const& src)
{
    auto const dest_size = ::MultiByteToWideChar(CP_ACP, 0U, src.data(), -1, nullptr, 0U);
    std::vector<wchar_t> dest(dest_size, L'\0');
    if (::MultiByteToWideChar(CP_ACP, 0U, src.data(), -1, dest.data(), dest.size()) == 0)
    {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::wstring(dest.begin(), dest.end());
}

//==========================================================================
/**
@brief [C/C++] UTF-8 -> Shift_JIS
@param src [in] wstring
@return string
*/
std::string char_convert::wide_to_multi_capi(std::wstring const& src)
{
    std::size_t converted{};
    std::vector<char> dest(src.size() * sizeof(wchar_t) + 1, '\0');
    if (::_wcstombs_s_l(&converted, dest.data(), dest.size(), src.data(), _TRUNCATE, ::_create_locale(LC_ALL, "jpn")) != 0) 
    {
        throw std::system_error{ errno, std::system_category() };
    }
    return std::string(dest.begin(), dest.end());
}

//==========================================================================
/**
@brief [Windows API] UTF-8 -> Shift_JIS
@param src [in] wstring
@return string
*/
std::string char_convert::wide_to_multi_winapi(std::wstring const& src)
{
    auto const dest_size = ::WideCharToMultiByte(CP_ACP, 0U, src.data(), -1, nullptr, 0, nullptr, nullptr);
    std::vector<char> dest(dest_size, '\0');
    if (::WideCharToMultiByte(CP_ACP, 0U, src.data(), -1, dest.data(), dest.size(), nullptr, nullptr) == 0)
    {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::string(dest.begin(), dest.end());
}

//==========================================================================
/**
@brief [C++] wstring -> string
@param src [in] wstring
@return string
*/
std::string char_convert::wide_to_utf8_cppapi(std::wstring const& src)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return converter.to_bytes(src);
}

//==========================================================================
/**
@brief [Windows API] wstring -> string
@param src [in] wstring
@return string
*/
std::string char_convert::wide_to_utf8_winapi(std::wstring const& src)
{
    auto const dest_size = ::WideCharToMultiByte(CP_UTF8, 0U, src.data(), -1, nullptr, 0, nullptr, nullptr);
    std::vector<char> dest(dest_size, '\0');
    if (::WideCharToMultiByte(CP_UTF8, 0U, src.data(), -1, dest.data(), dest.size(), nullptr, nullptr) == 0)
    {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::string(dest.begin(), dest.end());
}

//==========================================================================
/**
@brief [C++] string -> wstring
@param src [in] string
@return wstring
*/
std::wstring char_convert::utf8_to_wide_cppapi(std::string const& src)
{
    std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter;
    return converter.from_bytes(src);
}

//==========================================================================
/**
@brief [Windows API] string -> wstring
@param src [in] string
@return wstring
*/
std::wstring char_convert::utf8_to_wide_winapi(std::string const& src)
{
    auto const dest_size = ::MultiByteToWideChar(CP_UTF8, 0U, src.data(), -1, nullptr, 0U);
    std::vector<wchar_t> dest(dest_size, L'\0');
    if (::MultiByteToWideChar(CP_UTF8, 0U, src.data(), -1, dest.data(), dest.size()) == 0)
    {
        throw std::system_error{ static_cast<int>(::GetLastError()), std::system_category() };
    }
    return std::wstring(dest.begin(), dest.end());
}

//==========================================================================
/**
@brief [C/C++] Shift_JIS -> UTF-8
@param src [in] string
@return string
*/
std::string char_convert::multi_to_utf8_cppapi(std::string const& src)
{
    auto const wide = multi_to_wide_capi(src);
    return wide_to_utf8_cppapi(wide);
}

//==========================================================================
/**
@brief [Windows API] Shift_JIS -> UTF-8
@param src [in] string
@return string
*/
std::string char_convert::multi_to_utf8_winapi(std::string const& src)
{
    auto const wide = multi_to_wide_winapi(src);
    return wide_to_utf8_winapi(wide);
}

//==========================================================================
/**
@brief [C/C++] UTF-8 -> Shift_JIS
@param src [in] string
@return string
*/
std::string char_convert::utf8_to_multi_cppapi(std::string const& src)
{
    auto const wide = utf8_to_wide_cppapi(src);
    return wide_to_multi_capi(wide);
}

//==========================================================================
/**
@brief [Windows API] UTF-8 -> Shift_JIS
@param src [in] string
@return string
*/
std::string char_convert::utf8_to_multi_winapi(std::string const& src)
{
    auto const wide = utf8_to_wide_winapi(src);
    return wide_to_multi_winapi(wide);
}

_MSLIB_END