//==========================================================================
// グリッド[Grid.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================
#include "DX9_Vertex3D.h"
#include "Renderer.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : Grid
// Content: グリッド
//
//==========================================================================
class Grid : private DX9_VERTEX_3D, public component::Component, public renderer::Renderer
{
private:
    // コピー禁止 (C++11)
    Grid(const Grid &) = delete;
    Grid &operator=(const Grid &) = delete;
    Grid &operator=(Grid&&) = delete;
public:
    Grid();
    ~Grid();

    /**
    @brief 初期化
    @param scale [in] グリッドの表示範囲指定
    */
	void Init(int scale);

    /**
    @brief 解放
    */
	void Release(void);

    /**
    @brief 更新
    */
    void Update() override;

    /**
    @brief 描画
    @param device [in] デバイス
    */
    void Draw(LPDIRECT3DDEVICE9 device) override;

    /**
    @brief デバッグ描画
    @param device [in] デバイス
    */
    void DebugDraw(LPDIRECT3DDEVICE9 device) override;
private:
	VERTEX_2 *m_pos; // 頂点の作成
	int m_num; // 線の本数管理
	int m_scale; // サイズの記録
};

_MSLIB_END