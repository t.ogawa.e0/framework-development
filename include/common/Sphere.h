//==========================================================================
// 球体 [Sphere.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <unordered_map>

#include "mslib.hpp"
#include "Component.h"
#include "DX9_Vertex3D.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace sphere
{
    class CreateSphere;

    //==========================================================================
    //
    // class  : SphereData
    // Content: 球体のデータ
    //
    //==========================================================================
    class SphereData : public ireference::ReferenceData<LPD3DXMESH>
    {
    public:
        SphereData();
        ~SphereData();
    public:
        int quality; // 品質
        LPD3DXMESH mesh; // メッシュ
        LPDIRECT3DVERTEXBUFFER9 buffer; // バッファ
        int64_t createID; // 生成ID
        CreateSphere * CreatePtr; // 生成元のポインタ
    };

    //==========================================================================
    //
    // class  : SphereReference
    // Content: 参照用
    //
    //==========================================================================
    class SphereReference : public ireference::ReferenceOperator<SphereData>
    {
    public:
        SphereReference();
        ~SphereReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
    };

    //==========================================================================
    //
    // class  : CreateSphere
    // Content: 球体の生成機能
    //
    //==========================================================================
    class CreateSphere : public component::Component, private DX9_VERTEX_3D
    {
    private:
        // コピー禁止 (C++11)
        CreateSphere(const CreateSphere &) = delete;
        CreateSphere &operator=(const CreateSphere &) = delete;
        CreateSphere &operator=(CreateSphere&&) = delete;
    public:
        CreateSphere();
        CreateSphere(LPDIRECT3DDEVICE9 device, HWND hWnd);
        ~CreateSphere();

        /**
        @brief キューブを生成する。
        @param Quality [in] 品質
        */
        SphereReference Create(int Quality);

        /**
        @brief キューブを破棄する。
        @param data [in] データ
        */
        void Delete(SphereData * data);
    private:
        /**
        @brief 生成
        @param Quality [in] クオリティ
        @param data [out] データ
        */
        void Create(int Quality, SphereData * Out);

        /**
        @brief UVの生成
        @param Out [out] データ
        */
        void CreateUV(SphereData * Out);
    private:
        std::unordered_map<int, SphereData> m_data; // データ
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
        int64_t createIDCount; // カウンタ
    };

    //==========================================================================
    //
    // class  : SetSphere
    // Content: 球体登録クラス
    //
    //==========================================================================
    class SetSphere
    {
    public:
        SetSphere();
        virtual ~SetSphere();

        /**
        @brief 球体の登録
        @param data [in] 球体
        */
        void SetSphereData(const SphereReference & data);

        /**
        @brief 球体の取得
        */
        SphereReference & GetSphereData();
    protected:
        SphereReference m_SphereData; // 球体データ
    };
}

_MSLIB_END