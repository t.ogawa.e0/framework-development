//==========================================================================
// カメラ[Camera.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Camera.h"
#include "Collider.h"
#include "Manager.h"
#include "Renderer.h" // 描画

_MSLIB_BEGIN
namespace camera
{
    std::list<Camera*> Camera::m_AllCamera; // 全てのカメラ
    Camera * Camera::m_MainCamera = nullptr; // メインのカメラ

    Camera::Camera()
    {
        SetComponentName("Camera");
        m_AllCamera.push_back(this);
        auto winsize = manager::Manager::GetDevice()->GetWindowsSize();
        m_BlurTexture = manager::Manager::GetTextureLoader()->CreateTexture(winsize.x, winsize.y, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT);
        m_BlurTexture->GetSurfaceLevel(0, &m_BlurSurface);
    }

    Camera::Camera(const Camera & obj)
    {
        *this = obj;
        SetComponentName("Camera");
        m_AllCamera.push_back(this);

        auto winsize = manager::Manager::GetDevice()->GetWindowsSize();
        m_BlurTexture = manager::Manager::GetTextureLoader()->CreateTexture(winsize.x, winsize.y, 1, D3DUSAGE_RENDERTARGET, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT);
        m_BlurTexture->GetSurfaceLevel(0, &m_BlurSurface);
    }

    Camera::~Camera()
    {
        auto itr = std::find(m_AllCamera.begin(), m_AllCamera.end(), this);
        if (itr != m_AllCamera.end())
            m_AllCamera.erase(itr);

        if (m_MainCamera == this)
            m_MainCamera = nullptr;

        if (m_BlurSurface != nullptr)
        {
            m_BlurSurface->Release();
            m_BlurSurface = nullptr;
        }
    }

    //==========================================================================
    /**
    @brief 初期化
    */
    void Camera::Init(void)
    {
        m_Look1(D3DXVECTOR3(0.0f, 3.0f, -10.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
        m_Look2(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
        m_Vector.Identity();
        m_Vector.Normalize();
        D3DXMatrixIdentity(&m_ViewMatrix);
        D3DXMatrixIdentity(&m_ProjectionMatrix);
    }

    //==========================================================================
    /**
    @brief 初期化
    @param pEye [in] 注視点
    @param pAt [in] 座標
    */
    void Camera::Init(const D3DXVECTOR3 & pEye, const D3DXVECTOR3 & pAt)
    {
        m_Look1(pEye, pAt, D3DXVECTOR3(0.0f, 1.0f, 0.0f));
        m_Look2(D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 0.0f, 0.0f), D3DXVECTOR3(0.0f, 1.0f, 0.0f));
        m_Vector.Identity();
        m_Vector.Normalize();
        D3DXMatrixIdentity(&m_ViewMatrix);
        D3DXMatrixIdentity(&m_ProjectionMatrix);
    }

    void Camera::Rendering(const int2 & win_size, LPDIRECT3DDEVICE9 device)
    {
        auto pdevice = manager::Manager::GetDevice();
        LPDIRECT3DSURFACE9 main_buffer = nullptr;
        device->GetRenderTarget(0, &main_buffer);
        auto old_camera = m_MainCamera;

        for (auto itr = m_AllCamera.begin(); itr != m_AllCamera.end(); ++itr)
        {
            device->SetRenderTarget(0, (*itr)->m_BlurSurface);
            if (pdevice->DrawBegin())
            {
                // プロジェクション行列の作成
                // ズームイン、ズームアウトのような物
                D3DXMatrixPerspectiveFovLH(&(*itr)->m_ProjectionMatrix, D3DXToRadian(60)/*D3DX_PI/3*/, (float)win_size.x / (float)win_size.y, 0.1f/*ニヤ*/, 1000.0f/*ファー*/);
                device->SetTransform(D3DTS_VIEW, (*itr)->CreateView());
                device->SetTransform(D3DTS_PROJECTION, &(*itr)->m_ProjectionMatrix);
                (*itr)->IsMainCamera();
                renderer::Renderer::DrawAll(device);
                device->EndScene();
            }
        }
        m_MainCamera = old_camera;
        device->SetRenderTarget(0, main_buffer);
    }

    //==========================================================================
    /**
    @brief 更新
    @param win_size [in] {x,y}ウィンドウサイズ
    @param pDevice [in] デバイス
    */
    void Camera::UpdateAll(const int2 & win_size, LPDIRECT3DDEVICE9 device)
    {
        if (m_MainCamera == nullptr)return;

        // プロジェクション行列の作成
        // ズームイン、ズームアウトのような物
        D3DXMatrixPerspectiveFovLH(&m_MainCamera->m_ProjectionMatrix, D3DXToRadian(60)/*D3DX_PI/3*/, (float)win_size.x / (float)win_size.y, 0.1f/*ニヤ*/, 1000.0f/*ファー*/);
        device->SetTransform(D3DTS_VIEW, m_MainCamera->CreateView());
        device->SetTransform(D3DTS_PROJECTION, &m_MainCamera->m_ProjectionMatrix);
    }

    Camera * Camera::GetCamera()
    {
        return m_MainCamera;
    }

    //==========================================================================
    /**
    @brief ビュー行列生成
    @return ビュー行列
    */
    D3DXMATRIX * Camera::CreateView(void)
    {
        auto look = m_Look1 + m_Look2;
        look.up = m_Look1.up;

        // ビュー変換行列 ,(LH = 左手座標 ,LR = 右手座標)
        return D3DXMatrixLookAtLH(&m_ViewMatrix, &look.eye, &look.at, &look.up);
    }

    D3DXMATRIX * Camera::GetViewMatrix()
    {
        return &m_ViewMatrix;
    }

    D3DXMATRIX * Camera::GetProjectionMatrix()
    {
        return &m_ProjectionMatrix;
    }

    void Camera::AddViewRotation(const D3DXVECTOR3 & axis)
    {
        D3DXMATRIX Matrix, AddMatrix; //回転行列
        D3DXVECTOR3 Direction;

        // 外積を求めます
        m_Vector.CrossRight();

        // マトリクスの初期化
        D3DXMatrixIdentity(&Matrix);

        // 生成した回転マトリクスを合成していきます
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationY(&AddMatrix, axis.x), &Matrix);
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationAxis(&AddMatrix, &m_Vector.right, axis.y), &Matrix);
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationAxis(&AddMatrix, &m_Vector.front, axis.z), &Matrix);

        // 向きベクトルに合成します
        m_Vector.TransformNormal(Matrix);

        Direction = m_Look1.eye - m_Look1.at;
        D3DXVec3TransformNormal(&Direction, &Direction, &Matrix);
        m_Look1.eye = m_Look1.at + Direction;
        m_Look1.up = m_Vector.up;
    }

    void Camera::AddCameraRotation(const D3DXVECTOR3 & axis)
    {
        D3DXMATRIX Matrix, AddMatrix; //回転行列
        D3DXVECTOR3 Direction;

        // 外積を求めます
        m_Vector.CrossRight();

        // マトリクスの初期化
        D3DXMatrixIdentity(&Matrix);

        // 生成した回転マトリクスを合成していきます
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationY(&AddMatrix, axis.x), &Matrix);
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationAxis(&AddMatrix, &m_Vector.right, axis.y), &Matrix);
        D3DXMatrixMultiply(&Matrix, D3DXMatrixRotationAxis(&AddMatrix, &m_Vector.front, axis.z), &Matrix);

        // 向きベクトルに合成します
        m_Vector.TransformNormal(Matrix);

        Direction = m_Look1.at - m_Look1.eye;
        D3DXVec3TransformNormal(&Direction, &Direction, &Matrix);
        m_Look1.at = m_Look1.eye + Direction;
        m_Look1.up = m_Vector.up;
    }

    void Camera::AddViewRotation(float x, float y, float z)
    {
        AddViewRotation(D3DXVECTOR3(x, y, z));
    }

    void Camera::AddCameraRotation(float x, float y, float z)
    {
        AddCameraRotation(D3DXVECTOR3(x, y, z));
    }

    void Camera::AddViewRotationY(float y)
    {
        if (Restriction(y, 0.78f))
            AddViewRotation(D3DXVECTOR3(0, y, 0));
    }

    void Camera::AddCameraRotationY(float y)
    {
        if (Restriction(y, 0.78f))
            AddCameraRotation(D3DXVECTOR3(0, y, 0));
    }

    void Camera::AddViewRotationY(float y, float limit)
    {
        if (Restriction(y, limit))
            AddViewRotation(D3DXVECTOR3(0, y, 0));
    }

    void Camera::AddCameraRotationY(float y, float limit)
    {
        if (Restriction(y, limit))
            AddCameraRotation(D3DXVECTOR3(0, y, 0));
    }

    void Camera::AddPosition(const D3DXVECTOR3 & position)
    {
        m_Vector.Normalize();
        m_Vector.VectoeMultiply(m_Look1.eye, position);
        m_Vector.VectoeMultiply(m_Look1.at, position);
    }

    void Camera::AddPosition(float x, float y, float z)
    {
        AddPosition(D3DXVECTOR3(x, y, z));
    }

    //==========================================================================
    /**
    @brief 視点変更
    @param pVec [in] ベクトル
    @param pOut1 [out] 出力1
    @param pOut2 [out] 出力2
    @param pSpeed [in] 移動速度
    @return 戻り値は 距離
    */
    float Camera::ViewPos(D3DXVECTOR3 * pVec, D3DXVECTOR3 * pOut1, D3DXVECTOR3 * pOut2, const float * pSpeed)
    {
        m_Vector.CrossRight();
        D3DXVec3Normalize(pVec, pVec);
        *pOut1 += (*pVec)*(*pSpeed);

        float fDistance = powf
        (
            ((pOut1->x) - (pOut2->x))*
            ((pOut1->x) - (pOut2->x)) +
            ((pOut1->y) - (pOut2->y))*
            ((pOut1->y) - (pOut2->y)) +
            ((pOut1->z) - (pOut2->z))*
            ((pOut1->z) - (pOut2->z)),
            0.5f
        );

        return fDistance;
    }

    //==========================================================================
    /**
    @brief 内積
    @param pRot [in] 回転情報
    @param pRang [in] 回転速度
    @return 移動可能範囲なら true
    */
    bool Camera::Restriction(float axis, float limit)
    {
        D3DXMATRIX Rot;
        auto dir = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル

        auto vector = m_Vector;

        // ベクトルの座標変換
        vector.CrossRight();
        D3DXMatrixRotationAxis(&Rot, &vector.right, axis); // 回転
        vector.TransformNormal(Rot);
        float fVec3Dot = atanf(D3DXVec3Dot(&vector.front, &dir));

        // 内積
        if (-limit<fVec3Dot && limit>fVec3Dot) { return true; }

        return false;
    }

    //==========================================================================
    /**
    @brief 視点変更
    @param Distance [in] 入れた値が加算されます
    @return 視点とカメラの距離
    */
    float Camera::DistanceFromView(float Distance)
    {
        auto vec = m_Vector.front;

        return ViewPos(&vec, &m_Look1.eye, &m_Look1.at, &Distance);
    }

    //==========================================================================
    /**
    @brief カメラY軸回転情報
    @return 内積
    */
    float Camera::GetRestrictionY(void)
    {
        auto dir1 = D3DXVECTOR3(0, 1.0f, 0); // 単位ベクトル
        auto dir2 = D3DXVECTOR3(0, m_Vector.front.y, 0); // 単位ベクトル

        return atanf(D3DXVec3Dot(&dir2, &dir1)); // 内積
    }

    //==========================================================================
    /**
    @brief カメラX軸回転情報
    @return 内積
    */
    float Camera::GetRestrictionX(void)
    {
        auto dir1 = D3DXVECTOR3(1.0f, 0, 0); // 単位ベクトル
        auto dir2 = D3DXVECTOR3(m_Vector.front.x, 0, 0); // 単位ベクトル

        return atanf(D3DXVec3Dot(&dir2, &dir1)); // 内積
    }

    //==========================================================================
    /**
    @brief カメラZ軸回転情報
    @return 内積
    */
    float Camera::GetRestrictionZ(void)
    {
        auto dir1 = D3DXVECTOR3(0, 0, 1.0f); // 単位ベクトル
        auto dir2 = D3DXVECTOR3(0, 0, m_Vector.front.z); // 単位ベクトル

        return atanf(D3DXVec3Dot(&dir2, &dir1)); // 内積
    }

    //==========================================================================
    /**
    @brief カメラ座標をセット
    @param Eye [in] 注視点
    @param At [in] カメラ座標
    @param Up [in] ベクター
    */
    void Camera::SetCameraPos(const D3DXVECTOR3 & Eye, const D3DXVECTOR3 & At, const D3DXVECTOR3 & Up)
    {
        m_Look1(Eye, At, Up);
    }

    //==========================================================================
    /**
    @brief カメラ座標
    @param At [in] カメラ座標
    */
    void Camera::SetAt(const D3DXVECTOR3 & At)
    {
        m_Look2.at = At;
    }

    //==========================================================================
    /**
    @brief 注視点
    @param Eye [in] 注視点
    */
    void Camera::SetEye(const D3DXVECTOR3 & Eye)
    {
        m_Look2.eye = Eye;
    }

    const Look Camera::GetLook1() const
    {
        return m_Look1;
    }

    const Look Camera::GetLook2() const
    {
        return m_Look2;
    }

    const Vector Camera::GetVector() const
    {
        return m_Vector;
    }

    void Camera::IsMainCamera()
    {
        m_MainCamera = this;
    }

    Camera & Camera::operator=(const Camera & obj)
    {
        m_ViewMatrix = obj.m_ViewMatrix;
        m_Look1 = obj.m_Look1;
        m_Look2 = obj.m_Look2;
        m_Vector = obj.m_Vector;
 
        return *this;
    }
    texture::TextureReference & Camera::GetScreenImg()
    {
        return m_BlurTexture;
    }
}

namespace camera_develop
{
    std::list<Camera*> Camera::m_AllCamera; // 全カメラ
    Camera * Camera::m_MainCamera = nullptr; // メインのカメラ

    Camera::Camera()
    {
        SetComponentName("Camera");

        m_CameraPosition = AddComponent<transform::Transform>();
        m_ViewingPosition = m_CameraPosition->AddComponent<transform::Transform>();

        m_CameraPosition->SetComponentName("CameraPosition");
        m_ViewingPosition->SetComponentName("ViewingPosition");

        D3DXMatrixIdentity(&m_ViewMatrix);
        D3DXMatrixIdentity(&m_ProjectionMatrix);

        m_CameraPosition->AddPosition(0.0f, 3.0f, -10.0f);
        m_ViewingPosition->AddPosition(0.0f, 1.0f, 0.0f);

        m_CameraPosition->CreateWorldMatrix();
        m_ViewingPosition->CreateWorldMatrix();

        m_target = nullptr;

        m_AllCamera.push_back(this);
    }
    Camera::~Camera()
    {
        auto itr = std::find(m_AllCamera.begin(), m_AllCamera.end(), this);
        if (itr != m_AllCamera.end())
            m_AllCamera.erase(itr);

        if (m_MainCamera == this)
            m_MainCamera = nullptr;
    }
    transform::Transform & Camera::CameraPosition()
    {
        return *m_CameraPosition;
    }
    transform::Transform & Camera::ViewingPosition()
    {
        return *m_ViewingPosition;
    }
    void Camera::UpdateAll(const int2 & win_size, LPDIRECT3DDEVICE9 device)
    {
        if (m_MainCamera == nullptr)return;

        // プロジェクション行列の作成
        // ズームイン、ズームアウトのような物
        D3DXMatrixPerspectiveFovLH(&m_MainCamera->m_ProjectionMatrix, D3DXToRadian(60)/*D3DX_PI/3*/, (float)win_size.x / (float)win_size.y, 0.1f/*ニヤ*/, 1000.0f/*ファー*/);
        device->SetTransform(D3DTS_VIEW, m_MainCamera->CreateView());
        device->SetTransform(D3DTS_PROJECTION, &m_MainCamera->m_ProjectionMatrix);
    }
    void Camera::IsMainCamera()
    {
        m_MainCamera = this;
    }
    D3DXMATRIX * Camera::GetViewMatrix()
    {
        return &m_ViewMatrix;
    }
    D3DXMATRIX * Camera::GetProjectionMatrix()
    {
        return &m_ProjectionMatrix;
    }
    void Camera::LockOn(transform::Transform * obj)
    {
        m_target = obj;
    }
    D3DXMATRIX * Camera::CreateView()
    {
        // ビューカメラの向きを更新
        auto angle = m_CameraPosition->ToSee(*m_ViewingPosition);
        m_ViewingPosition->GetLook()->eye = -angle;
        m_ViewingPosition->GetVector()->front = angle;
        m_ViewingPosition->GetVector()->Normalize();

        // カメラの向きを更新
        m_CameraPosition->GetLook()->eye = -angle;
        m_CameraPosition->GetVector()->front = angle;
        m_CameraPosition->GetVector()->Normalize();

        // ビューカメラの更新フラグが有効
        if (m_ViewingPosition->UpdateKey())
        {
            auto mat = m_ViewingPosition->GetWorldMatrix();
            auto distance = collider::Distance(m_CameraPosition, m_ViewingPosition);

            m_CameraPosition->SetPosition(mat->_41, mat->_42, mat->_43);
            m_CameraPosition->AddPosition(0, 0, -distance);
        }

        // 全行列の更新
        auto mat1 = m_CameraPosition->GetWorldMatrix();
        auto mat2 = m_ViewingPosition->GetWorldMatrix();
        auto eye = D3DXVECTOR3(mat1->_41, mat1->_42, mat1->_43);
        auto at = D3DXVECTOR3(mat2->_41, mat2->_42, mat2->_43);
        auto look = m_CameraPosition->GetLook();

        if (m_target != nullptr)
        {
            eye = eye + *m_target->GetPosition();
        }

        // 更新のロック
        m_CameraPosition->LockUpdate();
        m_ViewingPosition->LockUpdate();

        // ビュー変換行列 ,(LH = 左手座標 ,LR = 右手座標)
        return D3DXMatrixLookAtLH(&m_ViewMatrix, &eye, &at, &look->up);
    }
}
_MSLIB_END