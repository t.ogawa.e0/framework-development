//==========================================================================
// マネージャー[Manager.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "mslib.hpp"
#include "Component.h"
#include "Device.h"
#include "Texture.h"
#include "XModel.h"
#include "Cube.h"
#include "Sphere.h"
#include "Billboard.h"
#include "Mesh.h"
#include "MeshField.h"
#include "Effect.h"
#include "Dinput.h"
#include "DinputController.h"
#include "DinputKeyboard.h"
#include "DinputMouse.h"
#include "XAudio2.h"
#include "MsEffekseer.h"

_MSLIB_BEGIN

namespace manager
{
    class Manager 
    {
    public:
        Manager();
        ~Manager();

        /**
        @brief 親コンポーネントを設定する
        @param pComponent [in] 親となるコンポーネント
        */
        static void SetManagerComponent(component::Component * pComponent);

        /**
        @brief テクスチャ読込クラスを取得する。
        */
        static texture::TextureLoader * GetTextureLoader();

        /**
        @brief Xモデル読込クラスを取得する。
        */
        static xmodel::XModelLoader * GetXModelLoader();

        /**
        @brief キューブの生成クラスを取得する。
        */
        static cube::CreateCube * GetCreateCube();

        /**
        @brief 球体の生成クラスを取得する。
        */
        static sphere::CreateSphere * GetCreateSphere();

        /**
        @brief ビルボードの生成クラスを取得する。
        */
        static billboard::CreateBillboard * GetCreateBillboard();

        /**
        @brief メッシュの生成クラスを取得する。
        */
        static mesh::CreateMesh * GetCreateMesh();

        /**
        @brief メッシュフィールドの読込クラスを取得する。
        */
        static meshfield::MeshFieldLoader * GetMeshFieldLoader();

        /**
        @brief メッシュフィールドの保存クラスを取得する。
        */
        static meshfield::MeshFieldSaver * GetMeshFieldSaver();

        /**
        @brief メッシュフィールドの生成クラスを取得する。
        */
        static meshfield::CreateMeshField * GetCreateMeshField();

        /**
        @brief エフェクト読み込みクラスを取得する。
        */
        static effect::EffectLoader * GetEffectLoader();

        /**
        @brief デバイスを取得する。
        */
        static device::Device * GetDevice();

        /**
        @brief DirectInputコントローラーを取得する。
        */
        static dinput_controller::Controller * GetDInputController();

        /**
        @brief DirectInputキーボードを取得する。
        */
        static dinput_keyboard::Keyboard * GetDInputKeyboard();

        /**
        @brief DirectInputマウスを取得する。
        */
        static dinput_mouse::Mouse * GetDInputMouse();

        /**
        @brief XAudio2Deviceを取得する。
        */
        static xaudio2::XAudio2Device * CreateXAudio2Device();

        /**
        @brief エフェクシア読み込み機能を取得する
        */
        static MsEffekseer::EffekseerLoader * GetEffekseerLoader();

        /**
        @brief DirectInputの更新
        */
        static void DInputUpdate();

        /**
        @brief マネージャーの更新
        */
        static void UpdateAll();

        /**
        @brief マネージャーでの一括描画
        */
        static bool DrawAll();
    public:
        using KeyboardButton = dinput_keyboard::Button;
        using ControllerButton = dinput_controller::Button;
        using ControllerKey = dinput_controller::ControllerKey;
        using MouseButton = dinput_mouse::Button;
    private:
        static component::Component * m_ManagerComponent; // コンポーネント
        static texture::TextureLoader * m_TextureLoader; // テクスチャの読み込み機能
        static xmodel::XModelLoader * m_XModelLoader; // Xモデルの読み込み機能
        static cube::CreateCube * m_CreateCube; // キューブ生成機能
        static sphere::CreateSphere * m_CreateSphere; // 球体の生成機能
        static billboard::CreateBillboard * m_CreateBillboard; // ビルボードの生成
        static meshfield::MeshFieldLoader * m_MeshFieldLoader; // メッシュフィールドの読み込み機能
        static meshfield::MeshFieldSaver * m_MeshFieldSaver; // メッシュフィールドの保存機能
        static meshfield::CreateMeshField * m_CreateMeshField; // メッシュフィールドの生成機能
        static mesh::CreateMesh * m_CreateMesh; // メッシュの生成
        static device::Device * m_Device; // デバイス
        static dinput_controller::Controller * m_Controller; // DInputコントローラー
        static dinput_keyboard::Keyboard * m_Keyboard; // DInputキーボード
        static dinput_mouse::Mouse * m_Mouse; // マウス
        static xaudio2::XAudio2Device * m_XAudio2; // XAudio2Device
        static effect::EffectLoader * m_EffectLoader; // エフェクト読み込み機能
        static MsEffekseer::EffekseerLoader * m_EffekseerLoader; // エフェクト読み込み機能
    };
}

_MSLIB_END