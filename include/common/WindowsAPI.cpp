//==========================================================================
// WindowsAPI[WindowsAPI.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "WindowsAPI.h"

_MSLIB_BEGIN

WindowsAPI::WindowsAPI(const std::string class_name, const std::string window_name)
{
    SetComponentName("WindowsAPI");
    m_class_name = class_name;
    m_window_name = window_name;
    m_hWnd = nullptr;
    m_msg = MSG();
    m_wcex = WNDCLASSEX();
    m_rect = RECT();
}

WindowsAPI::~WindowsAPI()
{
    m_class_name.clear();
    m_window_name.clear();

    UnregisterClass(m_wcex.lpszClassName, m_wcex.hInstance);
}

//==========================================================================
/**
@brief ウィンドウクラスとセット
@param style [in] ウィンドウのスタイル
@param __WndProc [in] WndProc
@param hIcon [in] アイコンのハンドル
@param hIconSm [in] 16×16の小さいサイズのアイコンのハンドル
@param lpszMenuName [in] デフォルトメニュー
@param hInstance [in] インスタンスハンドル
@return BUGBUG - might want to remove this from minwin
*/
ATOM WindowsAPI::MyClass(UINT style, WNDPROC __WndProc, LPCSTR hIcon, LPCSTR hIconSm, LPCSTR lpszMenuName, HINSTANCE hInstance)
{
    m_wcex.cbSize = sizeof(m_wcex);                 // 構造体のサイズ
    m_wcex.style = style;                                 // ウインドウスタイル
    m_wcex.lpfnWndProc = __WndProc;                       // そのウインドウのメッセージを処理するコールバック関数へのポインタ
    m_wcex.cbClsExtra = 0L;                               // ウインドウクラス構造体の後ろに割り当てる補足バイト数．普通0．
    m_wcex.cbWndExtra = 0L;                               // ウインドウインスタンスの後ろに割り当てる補足バイト数．普通0．
    m_wcex.hInstance = hInstance;                         // このクラスのためのウインドウプロシージャがあるインスタンスハンドル．
    m_wcex.hIcon = LoadIcon(hInstance, hIcon);            // アイコンのハンドル
    m_wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);		// マウスカーソルのハンドル．LoadCursor(nullptr, IDC_ARROW )とか．
    m_wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);    // ウインドウ背景色(背景描画用ブラシのハンドル)．
    m_wcex.lpszMenuName = lpszMenuName;					// デフォルトメニュー名(MAKEINTRESOURCE(メニューID))
    m_wcex.lpszClassName = m_class_name.c_str();	// このウインドウクラスにつける名前
    m_wcex.hIconSm = LoadIcon(hInstance, hIconSm);        // 16×16の小さいサイズのアイコン

    return RegisterClassEx(&m_wcex);
}

//==========================================================================
/**
@brief ウィンドウの生成
@param dwStyle [in] ウィンドウスタイル
@param rect [in] ウィンドウサイズ
@param hWndParent [in] 親ウィンドウまたはオーナーウィンドウのハンドル
@param hMenu [in] メニューハンドルまたは子ウィンドウ ID
@param lpParam [in] ウィンドウ作成データ
@param bMenu [in] メニューがあるかどうかの判定
@param nCmdShow [in] ShowWindow
@return 失敗時に true が返ります
*/
bool WindowsAPI::Create(DWORD dwStyle, RECT rect, HWND hWndParent, HMENU hMenu, LPVOID lpParam, int nCmdShow)
{
    m_rect = rect;
    m_hWnd = CreateWindow(m_class_name.c_str(), m_window_name.c_str(), dwStyle, m_rect.left, m_rect.top, m_rect.right, m_rect.bottom, hWndParent, hMenu, m_wcex.hInstance, lpParam);

    if (m_hWnd == nullptr)
    {
        MessageBox(m_hWnd, "ウィンドウが生成されませんでした。", "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
        return true;
    }

    ShowWindow(m_hWnd, nCmdShow);
    UpdateWindow(m_hWnd);

    return false;
}

//==========================================================================
/**
@brief メッセージのゲッター
@return Message
*/
MSG & WindowsAPI::GetMSG(void)
{
    return m_msg;
}

//==========================================================================
/**
@brief ウィンドウハンドル
@return hwnd
*/
HWND WindowsAPI::GetHWND(void)
{
    return m_hWnd;
}

/**
@brief ウィンドウRECT
@param Width [in] ウィンドウ幅
@param Height [in] ウィンドウ高さ
@param bMenu [in] メニューがあるかどうかの判定
@return rect
*/
RECT WindowsAPI::GetWindowRECT(DWORD dwStyle, int Width, int Height, bool bMenu)
{
    m_rect = { 0,0,Width ,Height };

    // ウィンドウの情報
    AdjustWindowRect(&m_rect, dwStyle, bMenu);
    m_rect.right = (m_rect.right - m_rect.left);
    m_rect.bottom = (m_rect.bottom - m_rect.top);
    m_rect.left = 0;
    m_rect.top = 0;

    return m_rect;
}

_MSLIB_END