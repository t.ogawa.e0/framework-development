//==========================================================================
// タイマー[Timer.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "Component.h"

_MSLIB_BEGIN

namespace timer
{
    //==========================================================================
    // リスト
    //==========================================================================
    class TimerData
    {
    public:
        TimerData();
        ~TimerData();

        /**
        @brief セット
        @param Count [in] 初期カウンタ
        @param Limit [in] カウンタ上限
        */
        void Set(int Count, int Limit);
    public:
        int m_Count; // カウンタ
        int m_Limit; // 制限
        int m_Defalt; // デフォルトの値
    };

    //==========================================================================
    //
    // class  : Timer
    // Content: タイマー
    //
    //==========================================================================
    class Timer : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        Timer &operator=(Timer&&) = delete;
    public:
        Timer();
        Timer(const Timer& timer);
        ~Timer();

        /**
        @brief 初期化
        @param Time [in] 秒数を入れてください
        @param Comma [in] コンマの値を入れてください 0〜60
        */
        void Init(int Time, int Comma);

        /**
        @brief カウントダウン処理
        @return カウンタの終了時に true が返ります
        */
        bool Countdown(void);

        /**
        @brief カウント処理
        */
        void Count(void);

        /**
        @brief 時間の取得
        @return 現在の時間
        */
        int GetTime(void);

        /**
        @brief コンマの取得
        @return 現在のコンマ
        */
        int GetComma(void);
    public: // operator
        Timer &operator =(const Timer & timer);
    private:
        TimerData m_Comma; // コンマ
        TimerData m_Time; // 秒
    };
}

_MSLIB_END