//==========================================================================
// キューブ[Cube.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Cube.h"

_MSLIB_BEGIN

namespace cube
{
    constexpr int nBite = 256; // バイト数
    constexpr int nMaxIndex = 36; // インデックス数
    constexpr int nNumVertex = 24; // 頂点数
    constexpr int nNumTriangle = 12; // 三角形の数
    constexpr float VCRCT = 0.5f;
    constexpr int NumDfaltIndex = 6;

    //==========================================================================
    //
    // class  : CubeData
    // Content: キューブデータ
    //
    //==========================================================================
    CubeData::CubeData()
    {
        Direction1 = 0; 
        Direction2 = 0;
        Pattern = 0; 
        CreatePtr = nullptr;   
        info.MaxIndex = nMaxIndex;
        info.NumVertex = nNumVertex;
        info.NumTriangle = nNumTriangle;
    }

    CubeData::~CubeData()
    {
        if (GetRef() == 0)
        {
            asset.Release();
            CreatePtr = nullptr;
        }
    }

    //==========================================================================
    //
    // class  : CubeReference
    // Content: 参照用
    //
    //==========================================================================
    CubeReference::CubeReference()
    {
        m_data = nullptr;
    }
    CubeReference::~CubeReference()
    {
        Release();
    }
    void CubeReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->CreatePtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->CreatePtr->Delete(m_data);
        m_data = nullptr;
    }

    const CubeInfo & CubeReference::Info()
    {
        return m_data->info;
    }

    //==========================================================================
    //
    // class  : CreateCube
    // Content: キューブ生成機能
    //
    //==========================================================================
    CreateCube::CreateCube()
    {
        SetComponentName("CreateCube");
        m_device = nullptr;
        m_hwnd = nullptr;
        m_CubeData = nullptr;
    }
    CreateCube::CreateCube(LPDIRECT3DDEVICE9 device, HWND hWnd)
    {
        m_device = device;
        m_hwnd = hWnd;
        m_CubeData = nullptr;
        SetComponentName("CreateCube");
    }
    CreateCube::~CreateCube()
    {
        AllDestroyComponent();
        if (m_CubeData != nullptr)
        {
            m_CubeData->asset.Release();
            delete m_CubeData;
            m_CubeData = nullptr;
        }
    }

    //==========================================================================
    /**
    @brief キューブを生成する。
    @param data [in] データ
    */
    CubeReference CreateCube::Create()
    {
        if (m_CubeData == nullptr)
        {
            floatUV aUv[6]; // 各面のUV
            VERTEX_4* pPseudo = nullptr;// 頂点バッファのロック
            WORD* pIndex = nullptr;

            m_CubeData = new CubeData;

            m_CubeData->Direction1 = 4;
            m_CubeData->Direction2 = 4;
            m_CubeData->Pattern = 6;

            // 画像データの格納
            m_CubeData->texsize = floatTexvec(0, 0, 1.0f, 1.0f);
            m_CubeData->texcutsize = floatTexvec(0, 0, 0, 0);
            m_CubeData->texcutsize.w = m_CubeData->texsize.w / m_CubeData->Direction1;
            m_CubeData->texcutsize.h = m_CubeData->texsize.h / m_CubeData->Direction2;

            for (int s = 0; s < m_CubeData->Pattern; s++)
            {
                CreateUV(m_CubeData, &aUv[s], s);
            }

            if (m_CubeData->asset.VertexBuffer == nullptr)
            {
                CreateVertexBuffer(m_device, m_hwnd, sizeof(VERTEX_4) * nNumVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &m_CubeData->asset.VertexBuffer, nullptr);
                m_CubeData->asset.VertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
                CreateVertex(pPseudo, aUv);
                m_CubeData->asset.VertexBuffer->Unlock();	// ロック解除
            }

            if (m_CubeData->asset.IndexBuffer == nullptr)
            {
                CreateIndexBuffer(m_device, m_hwnd, sizeof(WORD) * nMaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_CubeData->asset.IndexBuffer, nullptr);
                m_CubeData->asset.IndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
                CreateIndex(pIndex, nMaxIndex / NumDfaltIndex);
                m_CubeData->asset.IndexBuffer->Unlock();	// ロック解除
            }
            m_CubeData->CreatePtr = this;
        }

        return m_CubeData;
    }

    //==========================================================================
    /**
    @brief キューブを破棄する。
    @param data [in] データ
    */
    void CreateCube::Delete(CubeData * data)
    {
        if (data == nullptr)return;
        if (data->CreatePtr != this)return;
        if (data != m_CubeData)return;
        delete data;
        data = nullptr;
        m_CubeData = nullptr;
    }

    //==========================================================================
    /**
    @brief キューブ生成
    @param Output [out] 頂点情報
    @param _this [in] UV座標
    */
    void CreateCube::CreateVertex(VERTEX_4 * Output, const floatUV * _this)
    {
        VERTEX_4 vVertex[] =
        {
            // 手前
            { D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v0) }, // 左上
            { D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v0) }, // 右上
            { D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u1,_this[0].v1) }, // 右下
            { D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,0,-1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[0].u0,_this[0].v1) }, // 左下

            // 奥
            { D3DXVECTOR3(-VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v0) }, // 左上
            { D3DXVECTOR3(VCRCT,-VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v0) }, // 右上
            { D3DXVECTOR3(VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u1,_this[1].v1) }, // 右下
            { D3DXVECTOR3(-VCRCT, VCRCT,VCRCT),D3DXVECTOR3(0,0,1),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[1].u0,_this[1].v1) }, // 左下

            // 右
            { D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v0) }, // 左上
            { D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v0) }, // 右上
            { D3DXVECTOR3(-VCRCT, VCRCT, VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u1,_this[2].v1) }, // 右下
            { D3DXVECTOR3(-VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(-1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[2].u0,_this[2].v1) }, // 左下

            // 左
            { D3DXVECTOR3(VCRCT, VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v0) }, // 左上
            { D3DXVECTOR3(VCRCT, VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v0) }, // 右上
            { D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u1,_this[3].v1) }, // 右下
            { D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(1,0,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[3].u0,_this[3].v1) }, // 左下

            // 上
            { D3DXVECTOR3(-VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v0) }, // 左上
            { D3DXVECTOR3(VCRCT,VCRCT, VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v0) }, // 右上
            { D3DXVECTOR3(VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u1,_this[4].v1) }, // 右下
            { D3DXVECTOR3(-VCRCT,VCRCT,-VCRCT),D3DXVECTOR3(0,1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[4].u0,_this[4].v1) }, // 左下

            // 下
            { D3DXVECTOR3(-VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v0) }, // 左上
            { D3DXVECTOR3(VCRCT,-VCRCT,-VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v0) }, // 右上
            { D3DXVECTOR3(VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u1,_this[5].v1) }, // 右下
            { D3DXVECTOR3(-VCRCT,-VCRCT, VCRCT),D3DXVECTOR3(0,-1,0),D3DCOLOR_RGBA(255,255,255,255),D3DXVECTOR2(_this[5].u0,_this[5].v1) }, // 左下
        };

        for (int i = 0; i < (int)(sizeof(vVertex) / sizeof(VERTEX_4)); i++) { Output[i] = vVertex[i]; }
    }

    //==========================================================================
    /**
    @brief Indexの生成
    @param Output [in/out] 受け取り
    @param NumRectangle [in] 数
    */
    void CreateCube::CreateIndex(WORD * Output, int NumRectangle)
    {
        for (int i = 0, s = 0, ncount = 0; i < NumDfaltIndex*NumRectangle; i++, s++, ncount++)
        {
            switch (ncount)
            {
            case 3:
                s -= 3;
                break;
            case 4:
                s += 1;
                break;
            case 6:
                ncount = 0;
                break;
            default:
                break;
            }
            Output[i] = (WORD)s;
        }
    }

    //==========================================================================
    /**
    @brief UV生成
    @param Input [in] 面情報
    @param Output [in] UV情報
    @param nNum [in] 現在の面
    */
    void CreateCube::CreateUV(const CubeData * Input, floatUV * Output, const int nNum)
    {
        int patternNum = (nNum / 1) % Input->Pattern; //2フレームに１回	%10=パターン数
        int patternV = patternNum % Input->Direction1; //横方向のパターン
        int patternH = patternNum / Input->Direction2; //縦方向のパターン
        float fTcx = patternV * Input->texcutsize.w; //切り取りサイズ
        float fTcy = patternH * Input->texcutsize.h; //切り取りサイズ

        Output->u0 = fTcx;// 左
        Output->v0 = fTcy;// 上
        Output->u1 = fTcx + Input->texcutsize.w;// 右
        Output->v1 = fTcy + Input->texcutsize.h;//下
    }

    //==========================================================================
    //
    // class  : SetCube
    // Content: キューブ登録クラス
    //
    //==========================================================================
    SetCube::SetCube() {}
    SetCube::~SetCube() {}

    //==========================================================================
    /**
    @brief キューブの登録
    @param data [in] キューブ
    */
    void SetCube::SetCubeData(const CubeReference & data)
    {
        m_CubeData = data;
    }

    //==========================================================================
    /**
    @brief キューブの取得
    */
    CubeReference & SetCube::GetCubeData()
    {
        return m_CubeData;
    }
}

_MSLIB_END