//==========================================================================
// ライト[Light.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

//==========================================================================
// lib include
//==========================================================================

_MSLIB_BEGIN

//==========================================================================
//
// class  : Light
// Content: ライト 
//
//==========================================================================
class Light : public component::Component
{
private:
    Light() = delete;
    Light &operator=(Light&&) = delete;
public:
    Light(const Light& light);
    Light(LPDIRECT3DDEVICE9 pDevice);
	~Light();

    /**
    @brief 初期化
    @param aVecDir [in] 光のベクトルを入れるところ
    */
	void Init(const D3DXVECTOR3 & aVecDir);
public: // operator
    Light &operator =(const Light & timer);
private:
	D3DLIGHT9 m_aLight; // ライト
    LPDIRECT3DDEVICE9 m_Device;
};

_MSLIB_END