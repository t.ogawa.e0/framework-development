//==========================================================================
// Initializer [Initializer.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Initializer.h"

_MSLIB_BEGIN

namespace initializer
{
    std::list<Initializer*> Initializer::m_initializer_list; // 初期化リスト

    Initializer::Initializer()
    {
        m_initializer_list.push_back(this);
    }

    Initializer::~Initializer()
    {
        auto itr = std::find(m_initializer_list.begin(), m_initializer_list.end(), this);
        if (itr != m_initializer_list.end())
            m_initializer_list.erase(itr);
    }

    void Initializer::InitAll()
    {
        for (auto & itr : m_initializer_list)
        {
            itr->Init();
        }
        m_initializer_list.clear();
    }
    void Initializer::Init()
    {
    }
}

_MSLIB_END