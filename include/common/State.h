//==========================================================================
// State パターン [State.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib include
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

namespace state
{
    class StateBasic;

    //==========================================================================
    //
    // class  : State
    // Content: Stateパターン
    //
    //==========================================================================
    class State : public component::Component
    {
    public:
        State();
        virtual ~State();

        /**
        @brief ステートの変更。StateBasic を継承していない場合エラーが出ます。
        @return ステートの変更
        */
        template <typename _Ty, bool isExtended = std::is_base_of<StateBasic, _Ty>::value>
        _Ty* ChangeState() {
            // StateBasicが継承されていない場合に出力されます。
            static_assert(isExtended, "ChangeState<> : _Ty is not inherited from StateBasic Class");

            GetParent()->DestroyComponent(m_state);
            _Ty *component = GetParent()->AddComponent<_Ty>();
            m_state = component;
            return component;
        }

        /**
        @brief ステートの変更。StateBasic を継承していない場合エラーが出ます。
        @return ステートの変更
        */
        template <typename _Ty, bool isExtended = std::is_base_of<StateBasic, _Ty>::value, typename... _Valty>
        _Ty* ChangeState(_Valty&&... _Val) {
            // StateBasicが継承されていない場合に出力されます。
            static_assert(isExtended, "ChangeState<> : _Ty is not inherited from StateBasic Class");

            GetParent()->DestroyComponent(m_state);
            _Ty *component = GetParent()->AddComponent<_Ty>((_Val)...);
            m_state = component;
            return component;
        }

        /**
        @brief ステートの取得
        @return ステート
        */
        StateBasic * GetState();

        /**
        @brief ステートの更新
        @return 変更
        */
        void Update();

        /**
        @brief ステートが登録済みかどうかチェック
        @return 登録済みの場合 true が返ります
        */
        bool Trigger();

        /**
        @brief ステートを解放します
        */
        void ReleaseState();

        /**
        @brief ステートを解放します
        */
        bool ReleaseState(StateBasic * this_);

        using component::Component::SetComponentName;
    protected:
        StateBasic * m_state;
    };

    //==========================================================================
    //
    // class  : StateBasic
    // Content: ステート機能
    //
    //==========================================================================
    class StateBasic : public component::Component
    {
    public:
        StateBasic();
        virtual ~StateBasic();
        virtual void Update(State* this_) = 0;
    };
}

_MSLIB_END