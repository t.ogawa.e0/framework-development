//==========================================================================
// メッシュ [Mesh.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <string>
#include <unordered_map>

#include "mslib.hpp"
#include "Component.h"
#include "CreateBuffer.h"
#include "DX9_Vertex3D.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace mesh
{
    class CreateMesh;

    //==========================================================================
    // メッシュ情報
    //==========================================================================
    struct MeshInfo
    {
        int NumMeshX = 0; // 面の数
        int NumMeshZ = 0; // 面の数
        int VertexOverlap = 0; // 重複する頂点数
        int	NumXVertexWey = 0; // 視覚化されている1列の頂点数
        int	NumZVertex = 0; // 基礎頂点数
        int	NumXVertex = 0; // 基礎頂点数
        int	NumMeshVertex = 0; // 視覚化されている全体の頂点数
        int	MaxPrimitive = 0; // プリミティブ数
        int	MaxIndex = 0; // 最大Index数
        int	MaxVertex = 0; // 最大頂点数
    };

    //==========================================================================
    //
    // class  : MeshData
    // Content: メッシュデータ
    //
    //==========================================================================
    class MeshData : public ireference::ReferenceData<create_buffer::Buffer>
    {
    public:
        MeshData();
        ~MeshData();
    public:
        MeshInfo Info;
        int64_t createID; // 生成ID
        CreateMesh * CreatePtr; // 生成元のポインタ
    };

    //==========================================================================
    //
    // class  : MeshReference
    // Content: 参照用
    //
    //==========================================================================
    class MeshReference : public ireference::ReferenceOperator<MeshData>
    {
    public:
        MeshReference();
        ~MeshReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        const MeshInfo & Info();
    };

    //==========================================================================
    //
    // class  : CreateMesh
    // Content: メッシュ生成
    //
    //==========================================================================
    class CreateMesh : public component::Component, public create_buffer::CreateBuffer, private DX9_VERTEX_3D
    {
    private:
        // コピー禁止 (C++11)
        CreateMesh(const CreateMesh &) = delete;
        CreateMesh &operator=(const CreateMesh &) = delete;
        CreateMesh &operator=(CreateMesh&&) = delete;
    public:
        CreateMesh();
        CreateMesh(LPDIRECT3DDEVICE9 device, HWND hWnd);
        ~CreateMesh();

        /**
        @brief メッシュを生成する。
        */
        MeshReference Create(int x, int z);

        /**
        @brief メッシュを破棄する。
        @param data [in] データ
        */
        void Delete(MeshData * data);
    private:
        /**
        @brief メッシュ情報の生成
        @param info [out] データの出力
        @param x [in] X軸の面の数
        @param y [in] Z軸の面の数
        */
        void CreateMeshInfo(MeshInfo & info, int x, int z);

        /**
        @brief インデックス情報の生成
        @param Output [out] データの出力
        @param Input [in] メッシュデータ
        */
        void CreateIndex(LPWORD * Output, const MeshInfo & Input);

        /**
        @brief バーテックス情報の生成
        @param Output [out] データの出力
        @param Input [in] バーテックス
        */
        void CreateVertex(VERTEX_4 * Output, const MeshInfo & Input);
    private:
        std::unordered_map<std::string, MeshData> m_data; // メッシュデータ
        int64_t createIDCount; // カウンタ
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
    };

    //==========================================================================
    //
    // class  : SetMesh
    // Content: メッシュ登録クラス
    //
    //==========================================================================
    class SetMesh
    {
    public:
        SetMesh();
        virtual ~SetMesh();

        /**
        @brief メッシュの登録
        @param data [in] メッシュ
        */
        void SetMeshData(const MeshReference & data);

        /**
        @brief メッシュデータの取得
        */
        MeshReference & GetMeshData();
    protected:
        MeshReference m_MeshData; // メッシュデータ
    };

    const std::string create_key(int x, int z);
}
_MSLIB_END