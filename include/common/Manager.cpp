//==========================================================================
// マネージャー[Manager.h]
// author: tatsuya ogawa
//==========================================================================
#include "Manager.h"
#include "Sprite.h" // スプライト
#include "Renderer.h" // 描画
#include "FunctionComponent.h" // 機能
#include "Collider.h" // コリジョン
#include "Camera.h"
#include "MsImGui.h"

_MSLIB_BEGIN

namespace manager
{
    component::Component * Manager::m_ManagerComponent = nullptr; // コンポーネント
    texture::TextureLoader * Manager::m_TextureLoader = nullptr; // テクスチャの読み込み機能
    xmodel::XModelLoader * Manager::m_XModelLoader = nullptr; // Xモデルの読み込み機能
    cube::CreateCube * Manager::m_CreateCube = nullptr; // キューブ生成機能
    sphere::CreateSphere * Manager::m_CreateSphere = nullptr; // 球体の生成機能
    billboard::CreateBillboard * Manager::m_CreateBillboard = nullptr; // ビルボードの生成
    meshfield::MeshFieldLoader * Manager::m_MeshFieldLoader = nullptr; // メッシュフィールドの読み込み機能
    meshfield::MeshFieldSaver * Manager::m_MeshFieldSaver = nullptr; // メッシュフィールドの保存機能
    meshfield::CreateMeshField * Manager::m_CreateMeshField = nullptr; // メッシュフィールドの生成機能
    mesh::CreateMesh * Manager::m_CreateMesh = nullptr; // メッシュの生成
    device::Device * Manager::m_Device = nullptr; // デバイス
    dinput_controller::Controller * Manager::m_Controller = nullptr; // DInputコントローラー
    dinput_keyboard::Keyboard * Manager::m_Keyboard = nullptr; // DInputキーボード
    dinput_mouse::Mouse * Manager::m_Mouse = nullptr; // マウス
    xaudio2::XAudio2Device * Manager::m_XAudio2 = nullptr; // XAudio2Device
    effect::EffectLoader * Manager::m_EffectLoader = nullptr; // エフェクト読み込み機能
    MsEffekseer::EffekseerLoader * Manager::m_EffekseerLoader = nullptr; // エフェクト読み込み機能

    Manager::Manager()
    {
    }
    Manager::~Manager()
    {
    }

    void Manager::SetManagerComponent(component::Component * pComponent)
    {
        if (m_ManagerComponent == nullptr)
        {
            m_ManagerComponent = pComponent;
        }
    }

    texture::TextureLoader * Manager::GetTextureLoader()
    {
        if (m_TextureLoader == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            auto winsize = m_Device->GetWindowsSize();
            auto ratio = m_Device->ScreenBuffScale();
            m_TextureLoader = m_Device->AddComponent<texture::TextureLoader>(device, hwnd, winsize, ratio);
        }
        return m_TextureLoader;
    }

    xmodel::XModelLoader * Manager::GetXModelLoader()
    {
        if (m_XModelLoader == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_XModelLoader = m_Device->AddComponent<xmodel::XModelLoader>(device, hwnd, m_TextureLoader);
        }
        return m_XModelLoader;
    }

    cube::CreateCube * Manager::GetCreateCube()
    {
        if (m_CreateCube == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_CreateCube = m_Device->AddComponent<cube::CreateCube>(device, hwnd);
        }
        return m_CreateCube;
    }

    sphere::CreateSphere * Manager::GetCreateSphere()
    {
        if (m_CreateSphere == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_CreateSphere = m_Device->AddComponent<sphere::CreateSphere>(device, hwnd);
        }
        return m_CreateSphere;
    }

    billboard::CreateBillboard * Manager::GetCreateBillboard()
    {
        if (m_CreateBillboard == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_CreateBillboard = m_Device->AddComponent<billboard::CreateBillboard>(device, hwnd);
        }
        return m_CreateBillboard;
    }

    mesh::CreateMesh * Manager::GetCreateMesh()
    {
        if (m_CreateMesh == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_CreateMesh = m_Device->AddComponent<mesh::CreateMesh>(device, hwnd);
        }
        return m_CreateMesh;
    }

    meshfield::MeshFieldLoader * Manager::GetMeshFieldLoader()
    {
        if (m_MeshFieldLoader == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_MeshFieldLoader = m_Device->AddComponent<meshfield::MeshFieldLoader>(device, hwnd);
        }
        return m_MeshFieldLoader;
    }

    meshfield::MeshFieldSaver * Manager::GetMeshFieldSaver()
    {
        if (m_MeshFieldSaver == nullptr&&m_Device != nullptr)
        {
            m_MeshFieldSaver = m_Device->AddComponent<meshfield::MeshFieldSaver>();
        }
        return m_MeshFieldSaver;
    }

    meshfield::CreateMeshField * Manager::GetCreateMeshField()
    {
        if (m_CreateMeshField == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            auto hwnd = m_Device->GetHwnd();
            m_CreateMeshField = m_Device->AddComponent<meshfield::CreateMeshField>(device, hwnd);
        }
        return m_CreateMeshField;
    }

    effect::EffectLoader * Manager::GetEffectLoader()
    {
        if (m_EffectLoader == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            m_EffectLoader = m_Device->AddComponent<effect::EffectLoader>(device);
        }
        return m_EffectLoader;
    }

    device::Device * Manager::GetDevice()
    {
        if (m_Device == nullptr&&m_ManagerComponent != nullptr)
        {
            m_Device = m_ManagerComponent->AddComponent<device::Device>();
        }
        return m_Device;
    }

    dinput_controller::Controller * Manager::GetDInputController()
    {
        if (m_Controller == nullptr&&m_Device != nullptr)
        {
            m_Controller = m_Device->AddComponent<dinput_controller::Controller>();
        }
        return m_Controller;
    }

    dinput_keyboard::Keyboard * Manager::GetDInputKeyboard()
    {
        if (m_Keyboard == nullptr&&m_Device != nullptr)
        {
            m_Keyboard = m_Device->AddComponent<dinput_keyboard::Keyboard>();
        }
        return m_Keyboard;
    }

    dinput_mouse::Mouse * Manager::GetDInputMouse()
    {
        if (m_Mouse == nullptr&&m_Device != nullptr)
        {
            m_Mouse = m_Device->AddComponent<dinput_mouse::Mouse>();
        }
        return m_Mouse;
    }

    xaudio2::XAudio2Device * Manager::CreateXAudio2Device()
    {
        if (m_XAudio2 == nullptr&&m_Device != nullptr)
        {
            m_XAudio2 = m_Device->AddComponent<xaudio2::XAudio2Device>();
        }
        return m_XAudio2;
    }

    MsEffekseer::EffekseerLoader * Manager::GetEffekseerLoader()
    {
        if (m_EffekseerLoader == nullptr&&m_Device != nullptr)
        {
            auto device = m_Device->GetD3DDevice();
            m_EffekseerLoader = m_Device->AddComponent<MsEffekseer::EffekseerLoader>(device);
        }
        return m_EffekseerLoader;
    }

    void Manager::DInputUpdate()
    {
        dinput::Dinput::UpdateAll();
    }
    void Manager::UpdateAll()
    {
        sprite::Sprite::UpdateAll();
        function::FunctionComponent::UpdateAll();
        renderer::Renderer::UpdateAll();
        collider::Collider::UpdateAll();
    }
    bool Manager::DrawAll()
    {
        camera::Camera::Rendering(m_Device->GetWindowsSize(), m_Device->GetD3DDevice());

        auto flag = m_Device->DrawBegin();
        if (flag)
        {
            auto device = m_Device->GetD3DDevice();
            camera::Camera::UpdateAll(m_Device->GetWindowsSize(), m_Device->GetD3DDevice());
            renderer::Renderer::DrawAll(device);
            if (renderer::Renderer::GetAllDebugActivity())
                collider::Collider::DrawAll(device);
            sprite::Sprite::DrawAll(device);
        }

        return flag;
    }
}

_MSLIB_END