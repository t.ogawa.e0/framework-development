//==========================================================================
// WindowsAPI[WindowsAPI.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// #include
//==========================================================================
#include <windows.h>
#include <string>
#include <memory> // c++ メモリ
#include <cstdio> // 

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "Component.h"

_MSLIB_BEGIN

//==========================================================================
//
// class  : WindowsAPI
// Content: WindowsAPI
//
//=========================================================================
class WindowsAPI : public component::Component
{
public:
    // Class styles
    enum class styles
    {
        VREDRAW = CS_VREDRAW,
        HREDRAW = CS_HREDRAW,
        DBLCLKS = CS_DBLCLKS,
        OWNDC = CS_OWNDC,
        CLASSDC = CS_CLASSDC,
        PARENTDC = CS_PARENTDC,
        NOCLOSE = CS_NOCLOSE,
        SAVEBITS = CS_SAVEBITS,
        BYTEALIGNCLIENT = CS_BYTEALIGNCLIENT,
        BYTEALIGNWINDOW = CS_BYTEALIGNWINDOW,
        GLOBALCLAS = CS_GLOBALCLASS,
        VREDRAW_HREDRAW = CS_VREDRAW | CS_HREDRAW,
    };
private:
    // デフォルトコンストラクタ禁止
    WindowsAPI() = delete;
    // コピー禁止 (C++11)
    WindowsAPI(const WindowsAPI &) = delete;
    WindowsAPI &operator=(const WindowsAPI &) = delete;
    WindowsAPI &operator=(WindowsAPI&&) = delete;
public:
    WindowsAPI(const std::string class_name, const std::string window_name);
    ~WindowsAPI();

    /**
    @brief ウィンドウクラスとセット
    @param style [in] ウィンドウのスタイル
    @param __WndProc [in] WndProc
    @param hIcon [in] アイコンのハンドル
    @param hIconSm [in] 16×16の小さいサイズのアイコンのハンドル
    @param lpszMenuName [in] デフォルトメニュー
    @param hInstance [in] インスタンスハンドル
    @return BUGBUG - might want to remove this from minwin
    */
    ATOM MyClass(UINT style, WNDPROC __WndProc, LPCSTR hIcon, LPCSTR hIconSm, LPCSTR lpszMenuName, HINSTANCE hInstance);

    /**
    @brief ウィンドウの生成
    @param dwStyle [in] ウィンドウスタイル
    @param rect [in] ウィンドウサイズ
    @param hWndParent [in] 親ウィンドウまたはオーナーウィンドウのハンドル
    @param hMenu [in] メニューハンドルまたは子ウィンドウ ID
    @param lpParam [in] ウィンドウ作成データ
    @param nCmdShow [in] ShowWindow
    @return 失敗時に true が返ります
    */
    bool Create(DWORD dwStyle, RECT rect, HWND hWndParent, HMENU hMenu, LPVOID lpParam, int nCmdShow);

    /**
    @brief メッセージのゲッター
    @return Message
    */
    MSG & GetMSG(void);

    /**
    @brief ウィンドウハンドル
    @return hwnd
    */
    HWND GetHWND(void);

    /**
    @brief ウィンドウRECT
    @return rect
    */
    RECT GetWindowRECT(DWORD dwStyle, int Width, int Height, bool bMenu);

    /**
    @brief エラーメッセージ
    @param Width [in] ウィンドウ幅
    @param Height [in] ウィンドウ高さ
    @param bMenu [in] メニューがあるかどうかの判定
    @return hwnd
    */
    template <typename ... Args>
    void ErrorMessage(const char * text, const Args & ... args);
private:
    std::string m_class_name; // クラス名
    std::string m_window_name; // ウィンドウ名
    RECT m_rect; // ウィンドウRECT
    HWND m_hWnd; // ウィンドウハンドル
    MSG m_msg; // メッセージ
    WNDCLASSEX m_wcex; // ウィンドウクラス
};

//==========================================================================
/**
@brief エラーメッセージ
@return hwnd
*/
template<typename ...Args>
inline void WindowsAPI::ErrorMessage(const char * text, const Args & ... args)
{
    size_t size = snprintf(nullptr, 0, text, args ...) + 1; // Extra space for '\0'
    std::unique_ptr<char[]> buf(new char[size]);
    snprintf(buf.get(), size, text, args ...);

    MessageBox(m_hWnd, std::string(buf.get(), buf.get() + size - 1).c_str(), "ERROR", MB_OK | MB_ICONEXCLAMATION | MB_ICONWARNING);
}

_MSLIB_END