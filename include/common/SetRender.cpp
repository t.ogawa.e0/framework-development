//==========================================================================
// セットレンダー[DX9_SetRender.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SetRender.h"

_MSLIB_BEGIN

SetRender::SetRender()
{
}

SetRender::~SetRender()
{
}

//==========================================================================
/**
@brief 減算処理
@param pDevice [in] デバイス
*/
void SetRender::SetRenderREVSUBTRACT(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); //アルファブレンディングの有効化
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA); // αソースカラーの指定
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_REVSUBTRACT); // ブレンディング処理
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); //アルファテストの無効化
}

//==========================================================================
/**
@brief 半透明処理
@param pDevice [in] デバイス
*/
void SetRender::SetRenderADD(LPDIRECT3DDEVICE9 pDevice)
{
	// 半加算合成
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); //アルファブレンディングの有効化
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD); //ブレンディングオプション加算
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA); //SRCの設定
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);	//DESTの設定
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); //アルファテストの無効化
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_TRUE);
}

//==========================================================================
/**
@brief アルファテスト
@param pDevice [in] デバイス
@param Power [in] パワー
*/
void SetRender::SetRenderALPHAREF_START(LPDIRECT3DDEVICE9 pDevice, int Power)
{
	// αテストによる透明領域の切り抜き

	//pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_TRUE); //アルファテストの有効化
	//pDevice->SetRenderState(D3DRS_ALPHAREF, Power); //アルファ参照値
	//pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL); //アルファテスト合格基準
	//pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_FALSE); //アルファブレンディングの無効化

    pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_TRUE); //アルファテストの有効化
    pDevice->SetRenderState(D3DRS_ALPHAREF, Power); //アルファ参照値
    pDevice->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_GREATEREQUAL); //アルファテスト合格基準
}

//==========================================================================
/**
@brief アルファテスト_終わり
@param pDevice [in] デバイス
*/
void SetRender::SetRenderALPHAREF_END(LPDIRECT3DDEVICE9 pDevice)
{
	//pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); // αブレンドを許可
	//pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); // αテスト

    pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); // αテスト
}

//==========================================================================
/**
@brief 加算合成
@param pDevice [in] デバイス
*/
void SetRender::SetRenderSUB(LPDIRECT3DDEVICE9 pDevice)
{
	// 全加算合成
	pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, D3DZB_TRUE); //アルファブレンディングの有効化
	pDevice->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD); //ブレンディングオプション加算
	pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);	//SRCの設定
	pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE); //DESTの設定
	pDevice->SetRenderState(D3DRS_ALPHATESTENABLE, D3DZB_FALSE); //アルファテストの無効化
}

//==========================================================================
/**
@brief 描画時にZバッファを参照するか否か
@param pDevice [in] デバイス
*/
void SetRender::SetRenderZENABLE_START(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
}

//==========================================================================
/**
@brief 描画時にZバッファを参照するか否か_終わり
@param pDevice [in] デバイス
*/
void SetRender::SetRenderZENABLE_END(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);
}

//==========================================================================
/**
@brief Zバッファを描画するか否か
@param pDevice [in] デバイス
*/
void SetRender::SetRenderZWRITEENABLE_START(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_FALSE);
}

//==========================================================================
/**
@brief Zバッファを描画するか否か_終わり
@param pDevice [in] デバイス
*/
void SetRender::SetRenderZWRITEENABLE_END(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_ZWRITEENABLE, D3DZB_TRUE);
}

//==========================================================================
/**
@brief ワイヤフレームで描画します
@param pDevice [in] デバイス
*/
void SetRender::SetRenderWIREFRAME(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
}

//==========================================================================
/**
@brief 塗りつぶします
@param pDevice [in] デバイス
*/
void SetRender::SetRenderSOLID(LPDIRECT3DDEVICE9 pDevice)
{
	pDevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
}

_MSLIB_END