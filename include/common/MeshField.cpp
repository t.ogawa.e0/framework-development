//==========================================================================
// メッシュフィールド [MeshField.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "MeshField.h"

_MSLIB_BEGIN

namespace meshfield
{
    constexpr float CorrectionValue = 0.5f;
    constexpr int DefaultSize = 8; // バフ
    constexpr int exprInt2 = 2;

    //==========================================================================
    //
    // class  : MeshFieldData
    // Content: フィールドデータ
    //
    //==========================================================================
    MeshFieldData::MeshFieldData()
    {
        LoaderPtr = nullptr;
        CreatePtr = nullptr;
    }

    MeshFieldData::~MeshFieldData()
    {
        if (GetRef() == 0)
        {
            asset.Release();
            CreatePtr = nullptr;
            LoaderPtr = nullptr;
        }
    }

    //==========================================================================
    //
    // class  : MeshFieldReference
    // Content: 参照用
    //
    //==========================================================================
    MeshFieldReference::MeshFieldReference()
    {
        m_data = nullptr;
    }

    MeshFieldReference::~MeshFieldReference()
    {
        Release();
    }

    void MeshFieldReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->CreatePtr != nullptr)
        {
            if (m_data->Release())
                m_data->CreatePtr->Delete(m_data);
            m_data = nullptr;
        }
        else if (m_data->LoaderPtr != nullptr)
        {
            if (m_data->Release())
                m_data->LoaderPtr->Unload(m_data);
            m_data = nullptr;
        }
    }

    const MeshInfo & MeshFieldReference::Info()
    {
        return m_data->Info;
    }

    const std::unordered_map<std::string, FieldInfo*> & MeshFieldReference::MismatchMainData()
    {
        return m_data->MismatchMainData;
    }

    const std::unordered_map<std::string, FieldInfo> & MeshFieldReference::MismatchSubData()
    {
        return m_data->MismatchSubData;
    }

    const std::vector<int> & MeshFieldReference::GroupID()
    {
        return m_data->GroupID;
    }

    wrapper::vector<wrapper::vector<FieldID>> & MeshFieldReference::ArrayID()
    {
        return m_data->ArrayID;
    }

    Field & MeshFieldReference::FieldData()
    {
        return m_data->FieldData;
    }

    const std::string & MeshFieldReference::Tag()
    {
        return m_data->tag;
    }

    //==========================================================================
    //
    // class  : Common
    // Content: メッシュフィールド共通処理
    //
    //==========================================================================
    Common::Common()
    {
        SetComponentName("CommonMeshField");
        m_device = nullptr;
        m_hwnd = nullptr;
    }

    Common::~Common()
    {
        AllDestroyComponent();
        for (auto &itr : m_data)
        {
            Destroy(&itr.second);
        }
        m_data.clear();
    }

    //==========================================================================
    /**
    @brief メッシュ情報の生成
    @param info [out] インデックス情報の取得
    @param numX [in] X軸への幅
    @param numZ [in] Z軸への幅
    @param buff [in] 何分岐設定
    */
    void Common::CreateMeshInfo(MeshInfo & info, int x, int z, int buff)
    {
        info.NumMeshX = x;
        info.NumMeshZ = z;
        info.BoostMeshX = (x * buff);
        info.BoostMeshZ = (z * buff);
        info.NumXVertex = (x * buff) + 1; // 基礎頂点数
        info.NumZVertex = (z * buff) + 1; // 基礎頂点数
        info.MaxVertex = info.NumXVertex * info.NumZVertex; // 最大頂点数
        info.NumXVertexWey = exprInt2 * info.NumXVertex; // 視覚化されている1列の頂点数
        info.VertexOverlap = exprInt2 * ((z * buff) - 1); // 重複する頂点数
        info.NumMeshVertex = info.NumXVertexWey * (z * buff); // 視覚化されている全体の頂点数
        info.MaxIndex = info.NumMeshVertex + info.VertexOverlap; // 最大Index数
        info.MaxPrimitive = (((x * buff) * (z * buff)) * exprInt2) + (info.VertexOverlap * exprInt2); // プリミティブ数
    }

    //==========================================================================
    /**
    @brief 空間分割の生成
    @param data [in] メッシュ情報
    @param buff [in] 分割範囲
    */
    void Common::CreateSpaceDivision(MeshFieldReference & data, int buff)
    {
        auto pGroupID = const_cast<std::vector<int>*>(&data.GroupID());
        auto pArrayID = &data.ArrayID();

        pGroupID->clear();
        pArrayID->Release();

        // 空間分割と境目の定義
        int GroupID = 0;
        for (int iZ = buff, iZCount = 0; iZ < data.Info().NumZVertex; iZ += buff, iZCount += buff)
        {
            for (int iX = buff, iXCount = 0; iX < data.Info().NumXVertex; iX += buff, iXCount += buff)
            {
                FieldID * pMapArray = nullptr;
                pGroupID->emplace_back(GroupID);
                auto * pArray = pArrayID->Create();

                // 左上
                pMapArray = pArray->Create();
                pMapArray->x = iXCount;
                pMapArray->z = iZCount;

                // 右上
                pMapArray = pArray->Create();
                pMapArray->x = iX;
                pMapArray->z = iZCount;

                // 左下
                pMapArray = pArray->Create();
                pMapArray->x = iXCount;
                pMapArray->z = iZ;

                // 右下
                pMapArray = pArray->Create();
                pMapArray->x = iX;
                pMapArray->z = iZ;

                GroupID++;
            }
        }
    }

    //==========================================================================
    /**
    @brief バッファの生成
    @param Input [in] フィールドデータ
    @param data [in] メッシュ情報
    */
    void Common::CreateBuffer(Field & Input, MeshFieldReference & data)
    {
        auto pMainData = const_cast<std::unordered_map<std::string, FieldInfo*>*>(&data.MismatchMainData());
        bool bZ = false;
        bool bX = false;
        float fPosZ = data.Info().BoostMeshZ*CorrectionValue;
        float fPosX = 0.0f;
        int nIDZ = (int)(data.Info().BoostMeshZ*CorrectionValue);
        int nIDX = 0;

        pMainData->clear();

        for (int iZ = 0; iZ < data.Info().NumZVertex; iZ++, fPosZ--, nIDZ--)
        {
            bX = true;
            fPosX = -(data.Info().BoostMeshX*CorrectionValue);
            nIDX = -(int)(data.Info().BoostMeshX*CorrectionValue);
            Input.Create();
            for (int iX = 0; iX < data.Info().NumXVertex; iX++, fPosX++, nIDX++)
            {
                auto * vec = Input.Get(iZ)->Create();
                vec->m_NameID = std::to_string(nIDZ);
                vec->m_NameID += ":";
                vec->m_NameID += std::to_string(nIDX);
                vec->vertex.pos = D3DXVECTOR3(fPosX, 0.0f, fPosZ);
                vec->vertex.color = vec->m_Color.get();
                vec->vertex.Tex = D3DXVECTOR2((FLOAT)bX, (FLOAT)bZ);
                vec->vertex.Normal = D3DXVECTOR3(0, 0, 0);
                (*pMainData)[vec->m_NameID] = vec;
                bX = bX ^ 1;
            }
            bZ = bZ ^ 1;
        }
    }

    //==========================================================================
    /**
    @brief データの整合性
    @param Input [in] メッシュの情報
    @param data [in] メッシュ情報
    */
    void Common::ConsistencyCheck(Field & Input, MeshFieldReference & data)
    {
        auto pMainData = const_cast<std::unordered_map<std::string, FieldInfo*>*>(&data.MismatchMainData());
        auto pSubData = const_cast<std::unordered_map<std::string, FieldInfo>*>(&data.MismatchSubData());
        auto pFieldData = &data.FieldData();

        // 編集されたデータの流し込み
        for (auto &itr : (*pSubData))
        {
            // 要素の探索
            auto itr2 = pMainData->find(itr.first);
            if (itr2 != pMainData->end())
            {
                auto & vec4_1 = itr.second;
                auto * vec4_2 = itr2->second;

                vec4_2->vertex = vec4_1.vertex;
                vec4_2->m_Color = vec4_1.m_Color;
            }
        }

        pFieldData->Release();

        // アクセス
        for (int iZ = 0; iZ < Input.Size(); iZ++)
        {
            pFieldData->Create();
            for (int iX = 0; iX < Input.Get(iZ)->Size(); iX++)
            {
                auto * vec = pFieldData->Get(iZ)->Create();

                vec->m_Color = Input.Get(iZ)->Get(iX)->m_Color;
                vec->m_NameID = Input.Get(iZ)->Get(iX)->m_NameID;
                vec->vertex.pos = Input.Get(iZ)->Get(iX)->vertex.pos;
                vec->vertex.Normal = Input.Get(iZ)->Get(iX)->vertex.Normal;
                vec->vertex.color = Input.Get(iZ)->Get(iX)->vertex.color;
                vec->vertex.Tex = Input.Get(iZ)->Get(iX)->vertex.Tex;
            }
        }
    }

    //==========================================================================
    /**
    @brief ロックアンロック系処理
    @param data [in] セットするデータ
    */
    void Common::LockUnlock(MeshFieldReference & data)
    {
        VERTEX_4* pPseudo = nullptr;
        LPWORD* pIndex = nullptr;

        SetNormal(data.FieldData());

        (&data).VertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
        CreateVertex(pPseudo, data.FieldData());
        (&data).VertexBuffer->Unlock();	// ロック解除

        (&data).IndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
        CreateIndex(pIndex, data.Info());
        (&data).IndexBuffer->Unlock();	// ロック解除
    }

    //==========================================================================
    /**
    @brief 法線の生成
    @param Input [in] セットするパラメータ
    */
    void Common::SetNormal(Field & Input)
    {
        // 法線の設定
        for (int iZ = 1; iZ < Input.Size() - 1; iZ++)
        {
            for (int iX = 1; iX < Input.Get(iZ)->Size() - 1; iX++)
            {
                auto n = D3DXVECTOR3(0, 0, 0);
                auto nx = D3DXVECTOR3(0, 0, 0);
                auto nz = D3DXVECTOR3(0, 0, 0);
                auto vx = Input.Get(iZ)->Get(iX + 1)->vertex.pos - Input.Get(iZ)->Get(iX - 1)->vertex.pos;
                nx.x = vx.y; // 垂直なベクトル
                nx.y = vx.x;
                nx.z = 0.0f;

                vx = Input.Get(iZ - 1)->Get(iX)->vertex.pos - Input.Get(iZ + 1)->Get(iX)->vertex.pos;
                nz.x = 0.0f; // 垂直なベクトル
                nz.y = vx.z;
                nz.z = -vx.y;

                n = nx + nz;
                D3DXVec3Normalize(&n, &n);
                Input.Get(iZ)->Get(iX)->vertex.Normal = n;
            }
        }
    }

    //==========================================================================
    /**
    @brief バーテックス情報の生成
    @param Output [out] バーテックス情報
    @param Input [in] セットするパラメータ
    */
    void Common::CreateVertex(VERTEX_4 * Output, Field & Input)
    {
        // バッファに流し込む
        for (int iZ = 0; iZ < Input.Size(); iZ++)
        {
            for (int iX = 0; iX < Input.Get(iZ)->Size(); iX++, Output++)
            {
                auto * vec = Input.Get(iZ)->Get(iX);
                Output->pos = vec->vertex.pos;
                Output->color = vec->vertex.color = vec->m_Color.get();
                Output->Tex = vec->vertex.Tex;
                Output->Normal = vec->vertex.Normal;
            }
        }
    }

    //==========================================================================
    /**
    @brief インデックス情報の生成
    @param out [out] インデックス情報の取得
    @param data [in] メッシュデータ
    */
    void Common::CreateIndex(LPWORD * out, const MeshInfo & data)
    {
        for (int i = 0, Index1 = 0, Index2 = data.NumXVertex, Overlap = 0; i < data.MaxIndex; Index1++, Index2++)
        {
            // 通常頂点
            out[i] = (LPWORD)Index2;
            i++;

            // 重複点
            if (Overlap == data.NumXVertexWey&&i < data.MaxIndex)
            {
                out[i] = (LPWORD)Index2;
                i++;
                Overlap = 0;
            }

            // 通常頂点
            out[i] = (LPWORD)Index1;
            i++;

            Overlap += exprInt2;

            // 重複点
            if (Overlap == data.NumXVertexWey&&i < data.MaxIndex)
            {
                out[i] = (LPWORD)Index1;
                i++;
            }
        }
    }

    //==========================================================================
    /**
    @brief 破棄
    @param data [in] 破棄対象
    */
    void Common::Destroy(MeshFieldData * data)
    {
        if (data == nullptr)return;
        // バッファ解放
        data->asset.Release();
        data->MismatchSubData.clear();
        data->MismatchMainData.clear();
        data->FieldData.Release();
        data->GroupID.clear();
        data->ArrayID.Release();
    }

    //==========================================================================
    /**
    @brief リサイズ
    @param data [in] ポリゴン情報
    @param x [in] 横幅
    @param z [in] 奥行
    @return 失敗時 true が返ります
    */
    void Common::Resize(MeshFieldReference data, int x, int z)
    {
        Field field;

        //MeshFieldReference sdda = data;

        CreateMeshInfo(*const_cast<MeshInfo*>(&data.Info()), x, z, DefaultSize);
        CreateSpaceDivision(data, DefaultSize);
        CreateBuffer(field, data);
        ConsistencyCheck(field, data);
        CreateVertexBuffer(m_device, m_hwnd, sizeof(VERTEX_4) * data.Info().MaxVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &(&data).VertexBuffer, nullptr);
        CreateIndexBuffer(m_device, m_hwnd, sizeof(LPWORD) * data.Info().MaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &(&data).IndexBuffer, nullptr);
        LockUnlock(data);
    }

    CreateMeshField::CreateMeshField()
    {
        SetComponentName("CreateMeshField");
        m_device = nullptr;
        m_hwnd = nullptr;
    }
    CreateMeshField::CreateMeshField(LPDIRECT3DDEVICE9 device, HWND hWnd)
    {
        SetComponentName("CreateMeshField");
        m_device = device;
        m_hwnd = hWnd;
    }
    CreateMeshField::~CreateMeshField()
    {
    }

    //==========================================================================
    /**
    @brief フィールドを生成する。
    */
    MeshFieldReference CreateMeshField::Create(int x, int z)
    {
        auto strkey = create_key(x, z);
        auto itr = m_data.find(strkey);

        // 存在しない場合は処理を開始する
        if (itr == m_data.end())
        {
            auto &data = m_data[strkey];
            // 自動解放回避のカウントアップ
            data.AddRef();
            Resize(data, x, z);
            // 自動解放回避終了のカウントダウン
            data.Release();
            data.CreatePtr = this;
            data.tag = strkey;
        }
        return &m_data[strkey];
    }

    //==========================================================================
    /**
    @brief フィールドを破棄する。
    @param data [in] データ
    */
    void CreateMeshField::Delete(MeshFieldData * data)
    {
        if (data == nullptr)return;
        if (data->CreatePtr != this)return;

        // データの検索
        auto itr = m_data.find(data->tag);

        // データが存在する
        if (itr != m_data.end())
        {
            Destroy(data);
            m_data.erase(itr);
        }
    }
    MeshFieldLoader::MeshFieldLoader()
    {
        SetComponentName("MeshFieldLoader");
        m_device = nullptr;
        m_hwnd = nullptr;
    }
    MeshFieldLoader::MeshFieldLoader(LPDIRECT3DDEVICE9 device, HWND hWnd)
    {
        SetComponentName("MeshFieldLoader");
        m_device = device;
        m_hwnd = hWnd;
    }
    MeshFieldLoader::~MeshFieldLoader()
    {
    }

    //==========================================================================
    /**
    @brief フィールドを読み込む。
    @param path [in] 読み込みパス
    @return フィールドのポインタ
    */
    MeshFieldReference MeshFieldLoader::Load(const std::string & path)
    {
        auto itr = m_data.find(path);

        // 存在しない場合は処理を開始する
        if (itr == m_data.end())
        {
            auto *file = fopen(path.c_str(), "rb");
            if (file)
            {
                auto &data = m_data[path];

                data.tag = path;

                int mapsize = 0;
                fread(&mapsize, sizeof(mapsize), 1, file);

                // マップの広さ取得
                fread(&data.Info.NumMeshX, sizeof(data.Info.NumMeshX), 1, file);
                fread(&data.Info.NumMeshZ, sizeof(data.Info.NumMeshZ), 1, file);

                // 変化データの取得
                for (int i = 0; i < mapsize; i++)
                {
                    char bffer[512] = { 0 };

                    // 識別キーの取得
                    fread(&bffer, sizeof(bffer), 1, file);

                    // 識別キーを元にデータの登録
                    auto & Input = data.MismatchSubData[bffer];
                    fread(&Input.m_Color, sizeof(Input.m_Color), 1, file);
                    fread(&Input.vertex.color, sizeof(Input.vertex.color), 1, file);
                    fread(&Input.vertex.Normal, sizeof(Input.vertex.Normal), 1, file);
                    fread(&Input.vertex.pos, sizeof(Input.vertex.pos), 1, file);
                    fread(&Input.vertex.Tex, sizeof(Input.vertex.Tex), 1, file);
                }

                // 自動解放回避のカウントアップ
                data.AddRef();
                Resize(data, data.Info.NumMeshX, data.Info.NumMeshZ);
                // 自動解放回避終了のカウントダウン
                data.Release();
                data.LoaderPtr = this;

                fclose(file);
            }
        }
        return &m_data[path];
    }

    //==========================================================================
    /**
    @brief フィールドを破棄する。
    @param data [in] データ
    */
    void MeshFieldLoader::Unload(MeshFieldData * data)
    {
        if (data == nullptr)return;
        if (data->LoaderPtr != this)return;

        // データの検索
        auto itr = m_data.find(data->tag);

        // データが存在する
        if (itr != m_data.end())
        {
            Destroy(data);
            m_data.erase(itr);
        }
    }

    //==========================================================================
    /**
    @brief フィールドを保存。
    @param path [in] 保存名
    @param data [in] 保存データ
    */
    MeshFieldSaver::MeshFieldSaver()
    {
        SetComponentName("MeshFieldSaver");
    }

    MeshFieldSaver::~MeshFieldSaver()
    {
    }

    void MeshFieldSaver::Save(const std::string & path, const MeshFieldReference & data)
    {
        auto *file = fopen(path.c_str(), "wb");
        if (file)
        {
            auto pData = data;
            auto pMainData = const_cast<std::unordered_map<std::string, FieldInfo*>*>(&pData.MismatchMainData());
            auto pSubData = const_cast<std::unordered_map<std::string, FieldInfo>*>(&pData.MismatchSubData());

            // 各マップ毎の変化データ数
            int mapsize = pMainData->size();
            fwrite(&mapsize, sizeof(mapsize), 1, file);

            // 各マップの広さ
            fwrite(&pData.Info().NumMeshX, sizeof(pData.Info().NumMeshX), 1, file);
            fwrite(&pData.Info().NumMeshZ, sizeof(pData.Info().NumMeshZ), 1, file);

            // map内のデータを全て呼び出し
            for (auto &itr : (*pSubData))
            {
                // 要素の探索
                char cBuffer[512] = { 0 };

                // バッファに変換
                sprintf(cBuffer, "%s", itr.first.c_str());

                // 変化を与えるのに必要不可欠なデータ
                fwrite(&cBuffer, sizeof(cBuffer), 1, file);

                fwrite(&itr.second.m_Color, sizeof(itr.second.m_Color), 1, file);
                fwrite(&itr.second.vertex.color, sizeof(itr.second.vertex.color), 1, file);
                fwrite(&itr.second.vertex.Normal, sizeof(itr.second.vertex.Normal), 1, file);
                fwrite(&itr.second.vertex.pos, sizeof(itr.second.vertex.pos), 1, file);
                fwrite(&itr.second.vertex.Tex, sizeof(itr.second.vertex.Tex), 1, file);
            }
            fclose(file);
        }
    }

    //==========================================================================
    //
    // class  : SetMeshField
    // Content: メッシュフィールド登録クラス
    //
    //==========================================================================
    SetMeshField::SetMeshField() {}
    SetMeshField::~SetMeshField() {}

    //==========================================================================
    /**
    @brief メッシュフィールドの登録
    @param data [in] メッシュフィールド
    */
    void SetMeshField::SetMeshFieldData(const MeshFieldReference & data)
    {
        m_MeshFieldData = data;
    }

    //==========================================================================
    /**
    @brief メッシュフィールドの取得
    */
    MeshFieldReference & SetMeshField::GetMeshFieldData()
    {
        return m_MeshFieldData;
    }

    const std::string create_key(int x, int z)
    {
        return std::to_string(x) + ":" + std::to_string(z);
    }
}

_MSLIB_END