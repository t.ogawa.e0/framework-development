//==========================================================================
// シーン遷移[SceneManager.h]
// author: tatuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// Include
//==========================================================================
#include <Windows.h>
#include <string>

//==========================================================================
// lib include
//==========================================================================
#include "mslib.hpp"
#include "ObjectManager.h"
#include "Component.h" // コンポーネント

_MSLIB_BEGIN

namespace scene_manager
{
    //==========================================================================
    //
    // class  : BaseScene
    // Content: ベースとなる継承用クラス
    //
    //==========================================================================
    class BaseScene : public component::Component, public manager::Manager
    {
    public:
        BaseScene(const std::string & SceneName, const std::string & file_name);
        virtual ~BaseScene();

        /**
        @brief 初期化
        @return 初期化終了時にtrueが返ります。
        */
        bool Init();

        /**
        @brief 更新
        */
        void Update();

        /**
        @brief シーン名取得
        @return シーン名
        */
		const std::string & GetSceneName() const;

		/**
		@brief シーン保存用ファイルパスの取得
		@return ファイルパス
		*/
		const std::string & GetFileName() const;
    private:
        /**
        @brief 全初期化
        @return 全初期化終了時にtrueが返ります。
        */
        bool InitAll();

        /**
        @brief 全更新
        */
        void UpdateAll();

        /**
        @brief 時間の取得
        @return 時間
        */
        float GetTimeSec();

        /**
        @brief システム
        */
        void System();

        /**
        @brief 保存と読み取り
        */
        void SaveLoad();
    private:
        MsImGui m_Imgui; // ImGui
        std::list<ObjectManager*> m_object; // 親オブジェクト管理機能
        std::string m_SceneName; // シーン名
        std::string m_file_name; // ファイル名
#if defined(_MSLIB_DEBUG)
        boollist m_system; // システム
#endif
    };

    //==========================================================================
    //
    // class  : SceneManager
    // Content: 全てのシーンの管理
    //
    //==========================================================================
    class SceneManager : public component::Component
    {
    public:
        SceneManager();
        ~SceneManager();

        /**
        @brief 初期化
        @return 初期化終了時にtrueが返ります。
        */
        bool Init(void);

        /**
        @brief 解放
        */
        void Uninit(void);

        /**
        @brief 更新
        */
        void Update(void);

        /**
        @brief シーン切り替え関数
        @param scene [in] シーンポインタ
        */
        void ChangeScene(BaseScene * scene);

        /**
        @brief シーン名取得
        @return シーン名が返ります
        */
        const std::string GetSceneName(void) const;
    private:
        std::string m_SceneName;
        BaseScene* m_scene; // シーン
    };
}

_MSLIB_END