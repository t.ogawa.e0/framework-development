//==========================================================================
// メッシュ [Mesh.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Mesh.h"

_MSLIB_BEGIN

namespace mesh
{
    //==========================================================================
    //
    // class  : MeshData
    // Content: メッシュデータ
    //
    //==========================================================================
    MeshData::MeshData()
    {
        Info = MeshInfo();
        createID = (int64_t)0;
        CreatePtr = nullptr;
    }

    MeshData::~MeshData()
    {
        if (GetRef() == 0)
        {
            asset.Release();
            CreatePtr = nullptr;
            createID = 0;
        }
    }

    //==========================================================================
    //
    // class  : MeshReference
    // Content: 参照用
    //
    //==========================================================================
    MeshReference::MeshReference()
    {
        m_data = nullptr;
    }

    MeshReference::~MeshReference()
    {
        Release();
    }

    void MeshReference::Release()
    {
        if (m_data == nullptr)return;
        if (m_data->CreatePtr == nullptr)nullptr;
        if (m_data->Release())
            m_data->CreatePtr->Delete(m_data);
        m_data = nullptr;
    }

    const MeshInfo & MeshReference::Info()
    {
        return m_data->Info;
    }
    //==========================================================================
    //
    // class  : CreateMesh
    // Content: メッシュ生成
    //
    //==========================================================================
    CreateMesh::CreateMesh()
    {
        SetComponentName("CreateMesh");
        m_device = nullptr;
        m_hwnd = nullptr;
        createIDCount = (int64_t)0;
    }
    CreateMesh::CreateMesh(LPDIRECT3DDEVICE9 device, HWND hWnd)
    {
        SetComponentName("CreateMesh");
        m_device = device;
        m_hwnd = hWnd;
        createIDCount = (int64_t)0;
    }
    CreateMesh::~CreateMesh()
    {
        AllDestroyComponent();
        for (auto &itr : m_data)
        {
            itr.second.asset.Release();
        }
        m_data.clear();
    }

    //==========================================================================
    /**
    @brief メッシュを生成する。
    */
    MeshReference CreateMesh::Create(int x, int z)
    {
        auto strkey = create_key(x, z);
        auto itr = m_data.find(strkey);

        // 存在しない場合は処理を開始する
        if (itr == m_data.end())
        {
            VERTEX_4* pPseudo = nullptr;
            LPWORD* pIndex = nullptr;
            auto &data = m_data[strkey];

            createIDCount++;

            CreateMeshInfo(data.Info, x, z);
            CreateVertexBuffer(m_device, m_hwnd, sizeof(VERTEX_4) * data.Info.MaxVertex, D3DUSAGE_WRITEONLY, FVF_VERTEX_4, D3DPOOL_MANAGED, &data.asset.VertexBuffer, nullptr);
            CreateIndexBuffer(m_device, m_hwnd, sizeof(LPWORD) * data.Info.MaxIndex, D3DUSAGE_WRITEONLY, D3DFMT_INDEX32, D3DPOOL_MANAGED, &data.asset.IndexBuffer, nullptr);

            data.asset.VertexBuffer->Lock(0, 0, (void**)&pPseudo, D3DLOCK_DISCARD);
            CreateVertex(pPseudo, data.Info);
            data.asset.VertexBuffer->Unlock();

            data.asset.IndexBuffer->Lock(0, 0, (void**)&pIndex, D3DLOCK_DISCARD);
            CreateIndex(pIndex, data.Info);
            data.asset.IndexBuffer->Unlock();

            data.createID = createIDCount;
            data.CreatePtr = this;
        }
        return &m_data[strkey];
    }

    //==========================================================================
    /**
    @brief メッシュを破棄する。
    @param data [in] データ
    */
    void CreateMesh::Delete(MeshData * data)
    {
        if (data == nullptr)return;
        if (data->CreatePtr != this)return;
        auto itr = m_data.find(create_key(data->Info.NumMeshX, data->Info.NumMeshZ));
        if (itr == m_data.end())return;
        m_data.erase(itr);
    }

    //==========================================================================
    /**
    @brief メッシュ情報の生成
    @param info [out] データの出力
    @param x [in] X軸の面の数
    @param y [in] Z軸の面の数
    */
    void CreateMesh::CreateMeshInfo(MeshInfo & info, int x, int z)
    {
        info.NumMeshX = x;
        info.NumMeshZ = z;
        info.NumXVertex = x + 1; // 基礎頂点数
        info.NumZVertex = z + 1; // 基礎頂点数
        info.MaxVertex = info.NumXVertex * info.NumZVertex; // 最大頂点数
        info.NumXVertexWey = 2 * info.NumXVertex; // 視覚化されている1列の頂点数
        info.VertexOverlap = 2 * (z - 1); // 重複する頂点数
        info.NumMeshVertex = info.NumXVertexWey * z; // 視覚化されている全体の頂点数
        info.MaxIndex = info.NumMeshVertex + info.VertexOverlap; // 最大Index数
        info.MaxPrimitive = ((x * z) * 2) + (info.VertexOverlap * 2); // プリミティブ数
    }

    //==========================================================================
    /**
    @brief インデックス情報の生成
    @param Output [out] データの出力
    @param Input [in] メッシュデータ
    */
    void CreateMesh::CreateIndex(LPWORD * Output, const MeshInfo & Input)
    {
        for (int i = 0, Index1 = 0, Index2 = Input.NumXVertex, Overlap = 0; i < Input.MaxIndex; Index1++, Index2++)
        {
            // 通常頂点
            Output[i] = (LPWORD)Index2;
            i++;

            // 重複点
            if (Overlap == Input.NumXVertexWey&&i < Input.MaxIndex)
            {
                Output[i] = (LPWORD)Index2;
                i++;
                Overlap = 0;
            }

            // 通常頂点
            Output[i] = (LPWORD)Index1;
            i++;

            Overlap += 2;

            // 重複点
            if (Overlap == Input.NumXVertexWey&&i < Input.MaxIndex)
            {
                Output[i] = (LPWORD)Index1;
                i++;
            }
        }
    }

    //==========================================================================
    /**
    @brief バーテックス情報の生成
    @param Output [out] データの出力
    @param Input [in] バーテックス
    */
    void CreateMesh::CreateVertex(VERTEX_4 * Output, const MeshInfo & Input)
    {
        bool bZ = false;
        bool bX = false;
        float fPosZ = Input.NumMeshZ*0.5f;
        float fPosX = 0.0f;

        for (int iZ = 0; iZ < Input.NumZVertex; iZ++, fPosZ--)
        {
            bX = true;
            fPosX = -(Input.NumMeshX*0.5f);
            for (int iX = 0; iX < Input.NumXVertex; iX++, fPosX++, Output++)
            {
                Output->pos = D3DXVECTOR3(fPosX, 0.0f, fPosZ);
                Output->color = D3DCOLOR_RGBA(255, 255, 255, 255);
                Output->Tex = D3DXVECTOR2((FLOAT)bX, (FLOAT)bZ);
                Output->Normal = D3DXVECTOR3(0, 1, 0);
                bX = bX ^ 1;
            }
            bZ = bZ ^ 1;
        }
    }

    //==========================================================================
    //
    // class  : SetMesh
    // Content: メッシュ
    //
    //==========================================================================
    SetMesh::SetMesh() {}
    SetMesh::~SetMesh() {}

    //==========================================================================
    /**
    @brief メッシュの登録
    @param data [in] メッシュ
    */
    void SetMesh::SetMeshData(const MeshReference & data)
    {
        m_MeshData = data;
    }

    //==========================================================================
    /**
    @brief メッシュデータの取得
    */
    MeshReference & SetMesh::GetMeshData()
    {
        return m_MeshData;
    }

    const std::string create_key(int x, int z)
    {
        return std::to_string(x) + ":" + std::to_string(z);
    }
}

_MSLIB_END