//==========================================================================
// vector [vector.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <vector>
#include <algorithm>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "Component.h"

_MSLIB_BEGIN

namespace wrapper
{
    //==========================================================================
    //
    // class  : vector
    // Content: std::vector >> vector
    //
    //==========================================================================
    template <typename _Ty>
    class vector : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        vector(const vector &) = delete;
        vector &operator=(const vector &) = delete;
        vector &operator=(vector&&) = delete;
    public:
        vector() {
            SetComponentName("vector<>");
        }
        ~vector() {
            Release();
        }

        /**
        @brief インスタンスの生成
        @return インスタンス
        */
        _Ty * Create() {
            _Ty * Object = nullptr;
            m_list.emplace_back(New(Object));
            return Object;
        }

        /**
        @brief 解放
        */
        void Release() {
            for (auto &itr : m_list) {
                Delete(itr);
            }
            m_list.clear();
        }

        /**
        @brief 特定のObjectの破棄
        @param Object [in] 破棄するオブジェクトのアドレスを入れてください
        */
        void Release(_Ty * Object) {
            auto itr1 = std::find(m_list.begin(), m_list.end(), Object);
            if (itr1 != m_list.end()) {
                Delete((*itr1));
                m_list.erase(itr1);
            }
        }

        /**
        @brief 特定のObjectの破棄
        @param Object [in] 破棄する管理IDを入れてください
        */
        void Release(int label) {
            Release(Get(label));
        }

        /**
        @brief 管理しているObject数
        @return データ数
        */
        int Size() {
            return (int)m_list.size();
        }

        /**
        @brief Objectの取得
        @param label [in] 管理IDを入れてください
        @return 取得したい情報
        */
        _Ty * Get(int label) {
            return 0 <= label && label < Size() ? m_list[label] : nullptr;
        }

        /**
        @brief 現在のキャパシティーの取得
        @return キャパシティー
        */
        int GetCapacity() {
            return m_list.capacity();
        }

        /**
        @brief 領域予約
        @param _Newcapacity [in] 予約数
        */
        void Reserve(int _Newcapacity) {
            m_list.reserve(_Newcapacity);
        }
    private:
        /**
        @brief インスタンス生成
        @param Object [in] インスタンス生成対象
        @return インスタンス
        */
        _Ty * New(_Ty *& Object) {
            Object = new _Ty;
            return Object;
        }

        /**
        @brief インスタンスの破棄
        @param Object [in] インスタンス破棄対象
        @return nullptr が返ります
        */
        _Ty * Delete(_Ty *& Object) {
            // 破棄メモリがある時
            if (Object != nullptr) {
                delete Object;
                Object = nullptr;
            }
            return Object;
        }
    private:
        std::vector<_Ty*>m_list; // メインメモリ(アドレスで登録)
    };

    //==========================================================================
    //
    // class  : vector_component
    // Content: std::vector >> vector_component
    //
    //==========================================================================
    template <typename _Ty, bool isExtended = std::is_base_of<component::Component, _Ty>::value>
    class vector_component : public component::Component
    {
        // コンポーネントが継承されていない場合に出力されます。
        static_assert(isExtended, "vector_component<> : _Ty is not inherited from Component Class");
    private:
        // コピー禁止 (C++11)
        vector_component(const vector_component &) = delete;
        vector_component &operator=(const vector_component &) = delete;
        vector_component &operator=(vector_component&&) = delete;
    public:
        vector_component() {
            SetComponentName("vector_component<>");
        }
        ~vector_component() {
            Release();
        }

        /**
        @brief インスタンスの生成
        @return インスタンス
        */
        _Ty * Create() {
            auto obj = AddComponent<_Ty>();
            m_list.emplace_back(obj);
            return obj;
        }

        /**
        @brief 解放
        */
        void Release() {
            AllDestroyComponent();
            m_list.clear();
        }

        /**
        @brief 特定のObjectの破棄
        @param Object [in] 破棄するオブジェクトのアドレスを入れてください
        */
        void Release(_Ty * Object) {
            auto itr1 = std::find(m_list.begin(), m_list.end(), Object);
            if (itr1 != m_list.end()) {
                m_list.erase(itr1);
            }
            DestroyComponent(Object);
        }

        /**
        @brief 特定のObjectの破棄
        @param Object [in] 破棄する管理IDを入れてください
        */
        void Release(int label) {
            Release(Get(label));
        }

        /**
        @brief 管理しているObject数
        @return データ数
        */
        int Size() {
            return (int)m_list.size();
        }

        /**
        @brief Objectの取得
        @param label [in] 管理IDを入れてください
        @return 取得したい情報
        */
        _Ty * Get(int label) {
            return 0 <= label && label < Size() ? m_list[label] : nullptr;
        }

        /**
        @brief 現在のキャパシティーの取得
        @return キャパシティー
        */
        int GetCapacity() {
            return m_list.capacity();
        }

        /**
        @brief 領域予約
        @param _Newcapacity [in] 予約数
        */
        void Reserve(int _Newcapacity) {
            m_list.reserve(_Newcapacity);
        }
    private:
        std::vector<_Ty*>m_list; // ポインタ記憶用
    };
}

_MSLIB_END