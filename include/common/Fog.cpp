//==========================================================================
// フォグ[Fog.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Fog.h"

_MSLIB_BEGIN

Fog::Fog(LPDIRECT3DDEVICE9 pDevice)
{
    SetComponentName("Fog");
    m_Device = pDevice;
    m_color = 255;
    m_VertexMode = D3DFOGMODE::D3DFOG_NONE;
    m_TableMode = D3DFOGMODE::D3DFOG_NONE;
    m_EffectRange = EffectRangePos(0, 0);
    ZeroMemory(&m_caps, sizeof(D3DCAPS9));    //初期化
}

Fog::~Fog()
{
    OFF(); //フォグ：off
}

//==========================================================================
/**
@brief 初期化
@param color [in] 色
@param start_pos [in] フォグ影響開始地点
@param end_pos [in] フォグ完全影響地点
@param VertexMode [in] 頂点のモード指定
@param TableMode [in] テーブルモード設定
*/
void Fog::Init(const intColor & color, float start_pos, float end_pos, D3DFOGMODE VertexMode, D3DFOGMODE TableMode)
{
    m_color = color;
    m_EffectRange = EffectRangePos(start_pos, end_pos);
    m_VertexMode = VertexMode;
    m_TableMode = TableMode;

    ON();
    m_Device->GetDeviceCaps(&m_caps);
    m_Device->SetRenderState(D3DRS_FOGCOLOR, m_color.get()); //白色で不透明
    m_Device->SetRenderState(D3DRS_FOGVERTEXMODE, m_VertexMode); //頂点モード
    m_Device->SetRenderState(D3DRS_FOGTABLEMODE, m_TableMode); //テーブルモード
    m_Device->SetRenderState(D3DRS_FOGSTART, *(DWORD*)(&m_EffectRange.start)); //開始位置
    m_Device->SetRenderState(D3DRS_FOGEND, *(DWORD*)(&m_EffectRange.end)); //終了位置
}

//==========================================================================
/**
@brief フォクの有効化
*/
void Fog::ON(void)
{
    m_Device->SetRenderState(D3DRS_FOGENABLE, D3DZB_TRUE);
}

//==========================================================================
/**
@brief フォクの無効化
*/
void Fog::OFF(void)
{
    m_Device->SetRenderState(D3DRS_FOGENABLE, D3DZB_FALSE);
}

//==========================================================================
/**
@brief テーブルモード設定
@param TableMode [in] テーブルモード
*/
void Fog::SetTableMode(D3DFOGMODE TableMode)
{
    m_TableMode = TableMode;
}

//==========================================================================
/**
@brief テーブルモードの取得
@return モード情報
*/
D3DFOGMODE Fog::GetTableMode(void)
{
    return m_TableMode;
}

//==========================================================================
/**
@brief 頂点モード設定
@param VertexMode [in] 頂点モード
*/
void Fog::SetVertexMode(D3DFOGMODE VertexMode)
{
    m_VertexMode = VertexMode;
}

//==========================================================================
/**
@brief 頂点モードの取得
@return モード情報
*/
D3DFOGMODE Fog::GetVertexMode(void)
{
    return m_VertexMode;
}

//==========================================================================
/**
@brief 効果範囲の設定
@param start_pos [in] フォグ影響開始地点
@param end_pos [in]　フォグ完全影響地点
*/
void Fog::SetEffectRange(float start_pos, float end_pos)
{
    m_EffectRange = EffectRangePos(start_pos, end_pos);
}

//==========================================================================
/**
@brief 効果範囲の取得
@return 効果範囲
*/
const Fog::EffectRangePos * Fog::GetEffectRange(void)
{
    return &m_EffectRange;
}

//==========================================================================
/**
@brief 色の設定
@param color [in] 色
*/
void Fog::SetColor(const intColor & color)
{
    m_color = color;
}

//==========================================================================
/**
@brief 色の取得
@return 色
*/
const intColor * Fog::GetColor(void)
{
    return &m_color;
}

_MSLIB_END