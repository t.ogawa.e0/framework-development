//==========================================================================
// 仮想コントローラー [VirtualController.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "VirtualController.h"

_MSLIB_BEGIN

namespace controller
{
    VirtualController::VirtualController()
    {
        SetComponentName("VirtualController");
        m_input = false;
    }

    VirtualController::~VirtualController()
    {
    }
    void VirtualController::Update()
    {
        auto obj = GetWarrantyParent<transform::Transform>();

        if (obj == nullptr)return;
        if (!m_input)return;

        m_virtual.CreateWorldMatrix();
        obj->AddRotationX(obj->ToSee(m_virtual), 1.0f);
        m_input = false;
    }
    void VirtualController::Input(const D3DXVECTOR3 & position, const D3DXVECTOR3 &DirectionVector)
    {
        m_input = true;

        auto obj = GetWarrantyParent<transform::Transform>();
        if (obj == nullptr)return;

        m_virtual.SetPosition({ obj->GetWorldMatrix()->_41 ,obj->GetWorldMatrix()->_42 ,obj->GetWorldMatrix()->_43 });
        m_virtual.AddRotationX(DirectionVector, 1.0f);
        m_virtual.AddPosition(position);
    }
    const D3DXMATRIX & VirtualController::GetWorldMatrix()
    {
        return *m_virtual.GetWorldMatrix();
    }
    const D3DXMATRIX & VirtualController::GetLocalMatrix()
    {
        return *m_virtual.GetLocalMatrix();
    }
}
_MSLIB_END