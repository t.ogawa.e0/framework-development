//==========================================================================
// 距離のコライダー [DistanceCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DistanceCollider.h"

_MSLIB_BEGIN

namespace collider
{
    constexpr const char * __vertex_c__ = "vertex";
    constexpr const float __the_size_of_a_circle__ = 0.05f;
    constexpr const float __determination_distance__ = 1.0f;
    constexpr const int __num_line__ = 30;
    constexpr const int __decision_diagram__ = 4;
    constexpr const float __line_length__ = 0.2f;

	D3DXVECTOR3 MyToSee(transform::Transform & my, transform::Transform & target)
	{
		auto mat1 = my.GetWorldMatrix();
		auto mat2 = target.GetWorldMatrix();
		return D3DXVECTOR3(mat2->_41, mat2->_42, mat2->_43) -
			D3DXVECTOR3(mat1->_41, mat1->_42, mat1->_43);
	}

    struct VectorDrawData
    {
        D3DXVECTOR3 pos = D3DXVECTOR3(0, 0, 0); // 座標変換が必要
        D3DCOLOR color = D3DCOLOR_RGBA(255, 255, 255, 255); // ポリゴンの色
    };

    constexpr float ToMinus(float plus)
    {
        if (((float)0) < plus)
            return -plus;

        return plus;
    }
    constexpr float ToPlus(float minus)
    {
        if (minus < ((float)0))
            return -minus;

        return minus;
    }

    DistanceCollider::DistanceCollider()
    {
        SetComponentName("DistanceCollider");
        m_transform.reserve(1);
        m_distance = nullptr;
    }

    DistanceCollider::~DistanceCollider()
    {
        m_myself_matrix.clear();
        m_distance_matrix.clear();
    }

    void DistanceCollider::Init()
    {
        if (m_transform.size() != 0)return;

        auto obj = AddComponent<transform::Transform>();
        m_transform.push_back(obj);
        obj->SetComponentName("CircleObject by DistanceCollider");

        m_distance = obj->AddComponent<transform::Transform>();
        m_distance->SetComponentName("DistanceObject by DistanceCollider");
        m_distance->SetPosition(0, 0, ToMinus(__determination_distance__));
    }

    void DistanceCollider::Update()
    {
        auto target1 = GetWarrantyParent<transform::Transform>();

        // 描画を行うかのチェック
        if (target1 == nullptr)return;
        if (m_transform.size() == 0)return;

        auto itr = m_transform.begin();
        auto mat = target1->GetWorldMatrix();

#if defined(_MSLIB_DEBUG)
        m_myself_matrix.clear();
        m_distance_matrix.clear();
#endif

        // コリジョン実行前に座標更新
        (*itr)->SetPosition(mat->_41, mat->_42, mat->_43);

        // コリジョン処理
        for (auto & itr2 : m_target_collider)
        {
            auto target2 = itr2->GetWarrantyParent<transform::Transform>();
            if (target2 == nullptr)continue;
            auto angle = MyToSee(*(*itr), *target2);

            // 各種対象に向きを変える
            (*itr)->GetLook()->eye = -angle;
            (*itr)->GetVector()->front = angle;
            (*itr)->GetVector()->Normalize();

            // 行列を生成
            (*itr)->CreateWorldMatrix();
            m_distance->CreateWorldMatrix();

            // 距離で判定を出す処理
            auto hitkey = Sphere(target2, (*itr), ToPlus(m_distance->GetPosition()->z));
            if (!m_collision)
                m_collision = hitkey;
#if defined(_MSLIB_DEBUG)
            m_myself_matrix.push_back(D3DXMATRIX_BOOL(*(*itr)->GetWorldMatrix(), hitkey));
            m_distance_matrix.push_back(D3DXMATRIX_BOOL(*m_distance->GetWorldMatrix(), hitkey));
#endif
        }
    }

    void DistanceCollider::Draw(LPDIRECT3DDEVICE9 device)
    {
        // 描画するか
        if (m_transform.size() == 0)return;
        if (m_target_collider.size() == 0)return;

        float C_R = D3DX_PI * 2 / __num_line__;	// 円周率
        auto *circle = new VectorDrawData[__num_line__ + 1];
        auto *decision = new VectorDrawData[__decision_diagram__ + 1];
        auto color = D3DCOLOR_RGBA(0, 255, 176, 255);

        // 円の生成
        for (int i = 0; i < (__num_line__ + 1); i++)
        {
            circle[i].pos.x = ((0.0f + (cosf(C_R * i)*__the_size_of_a_circle__)));
            circle[i].pos.y = 0.0f;
            circle[i].pos.z = ((0.0f + (sinf(C_R * i)*__the_size_of_a_circle__)));
            circle[i].color = color;
        }

        // コリジョンの描画
        for (int i = 0; i < (__decision_diagram__ + 1); i++)
        {
            decision[i].pos = *m_distance->GetPosition();
            decision[i].color = color;
        }
        decision[0].pos = D3DXVECTOR3(0, 0, 0);
        decision[2].pos.x -= __line_length__;
        decision[3].pos.x += __line_length__;
        decision[__decision_diagram__] = decision[3];

        device->SetFVF(D3DFVF_XYZ | D3DFVF_DIFFUSE);
        device->SetTexture(0, nullptr);

        for (auto & itr : m_myself_matrix)
        {
            D3DCOLOR color_itr = D3DCOLOR();

            if (itr.hit)
                color_itr = D3DCOLOR_RGBA(255, 0, 0, 255);
            if (!itr.hit)
                color_itr = color;
            
            for (int i = 0; i < (__num_line__ + 1); i++)
            {
                circle[i].color = color_itr;
            }
            for (int i = 0; i < (__decision_diagram__ + 1); i++)
            {
                decision[i].color = color_itr;
            }
            device->SetTransform(D3DTS_WORLD, &itr.mat);
            device->DrawPrimitiveUP(D3DPT_LINESTRIP, __num_line__, circle, sizeof(VectorDrawData));
            device->DrawPrimitiveUP(D3DPT_LINESTRIP, __decision_diagram__, decision, sizeof(VectorDrawData));
        }

        for (auto & itr : m_distance_matrix)
        {
            D3DCOLOR color_itr = D3DCOLOR();

            if (itr.hit)
                color_itr = D3DCOLOR_RGBA(255, 0, 0, 255);
            if (!itr.hit)
                color_itr = color;

            for (int i = 0; i < (__num_line__ + 1); i++)
            {
                circle[i].color = color_itr;
            }
            for (int i = 0; i < (__decision_diagram__ + 1); i++)
            {
                decision[i].color = color_itr;
            }
            device->SetTransform(D3DTS_WORLD, &itr.mat);
            device->DrawPrimitiveUP(D3DPT_LINESTRIP, __num_line__, circle, sizeof(VectorDrawData));
        }

        delete[]circle;
        delete[]decision;
    }

    void DistanceCollider::DeterminationDistance(float distance)
    {
        if (m_distance != nullptr)
            m_distance->SetPosition(0, 0, ToMinus(distance));
    }
}

_MSLIB_END