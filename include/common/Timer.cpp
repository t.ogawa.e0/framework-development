//==========================================================================
// タイマー[Timer.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Timer.h"

_MSLIB_BEGIN

namespace timer
{
    Timer::Timer()
    {
        SetComponentName("Timer");
    }

    Timer::Timer(const Timer & timer)
    {
        *this = timer;
        SetComponentName("Timer");
    }

    Timer::~Timer()
    {
    }

    //==========================================================================
    /**
    @brief 初期化
    @param Time [in] 秒数を入れてください
    @param Comma [in] コンマの値を入れてください 0〜60
    */
    void Timer::Init(int Time, int Comma)
    {
        m_Time.Set(Time, 0);
        m_Comma.Set(Comma, 0);
    }

    //==========================================================================
    /**
    @brief カウントダウン処理
    @return カウンタの終了時に true が返ります
    */
    bool Timer::Countdown(void)
    {
        if ((m_Time.m_Count != 0) || (m_Comma.m_Count != 0))
        {
            if (m_Time.m_Limit <= m_Time.m_Count)
            {
                if (m_Comma.m_Count <= m_Comma.m_Limit)
                {
                    m_Time.m_Count--;
                    m_Comma.m_Count = m_Comma.m_Defalt;
                }
                m_Comma.m_Count--;
            }
        }
        else if ((m_Time.m_Count == 0) && (m_Comma.m_Count == 0))
        {
            return true;
        }

        return false;
    }

    //==========================================================================
    /**
    @brief カウント処理
    */
    void Timer::Count(void)
    {
        m_Comma.m_Count++;
        if (m_Comma.m_Limit <= m_Comma.m_Count)
        {
            m_Time.m_Count++;
            m_Comma.m_Count = 0;
        }
    }

    //==========================================================================
    /**
    @brief 時間の取得
    @return 現在の時間
    */
    int Timer::GetTime(void)
    {
        return m_Time.m_Count;
    }

    //==========================================================================
    /**
    @brief コンマの取得
    @return 現在のコンマ
    */
    int Timer::GetComma(void)
    {
        return m_Comma.m_Count;
    }

    Timer & Timer::operator=(const Timer & timer)
    {
        m_Comma = timer.m_Comma;
        m_Time = timer.m_Time;
        return *this;
    }

    TimerData::TimerData()
    {
        m_Count = 0;
        m_Limit = 0;
        m_Defalt = 0;
    }

    TimerData::~TimerData()
    {
    }

    //==========================================================================
    /**
    @brief セット
    @param Count [in] 初期カウンタ
    @param Limit [in] カウンタ上限
    */
    void TimerData::Set(int Count, int Limit)
    {
        if (Count == 0)
        {
            m_Count = Count;
            m_Limit = 60;
        }
        else
        {
            m_Count = Count;
            m_Limit = Limit;
        }
        m_Defalt = m_Count;
    }
}

_MSLIB_END