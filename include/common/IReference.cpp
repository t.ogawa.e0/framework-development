//==========================================================================
// 参照カウンタ [IReference.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "IReference.h"

_MSLIB_BEGIN

namespace ireference
{
    IReference::IReference() : m_reference(0)
    {
    }
    IReference::~IReference()
    {

    }

    //==========================================================================
    /**
    @brief	参照カウンタを加算する。
    @return	加算後の参照カウンタ
    */
    int IReference::AddRef()
    {
        std::atomic_fetch_add_explicit(&m_reference, 1, std::memory_order_consume);

        return m_reference;
    }

    //==========================================================================
    /**
    @brief	参照カウンタを取得する。
    @return	参照カウンタ
    */
    int IReference::GetRef()
    {
        return m_reference;
    }

    //==========================================================================
    /**
    @brief	参照カウンタを減算する。0になった時、消去可能フラグが立つ
    @return	消去可能フラグ
    */
    bool IReference::Release()
    {
        return std::atomic_fetch_sub_explicit(&m_reference, 1, std::memory_order_consume) == 1;
    }
}

_MSLIB_END