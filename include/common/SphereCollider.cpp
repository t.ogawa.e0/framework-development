//==========================================================================
// 球体形状の衝突プリミティブ [SphereCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "SphereCollider.h"

_MSLIB_BEGIN

namespace collider
{
    constexpr const char * __vertex_c__ = "vertex";
    SphereCollider::SphereCollider() : m_scale(1)
    {
        SetComponentName("SphereCollider");
    }

    SphereCollider::~SphereCollider()
    {
    }

    void SphereCollider::Init()
    {
    }

    void SphereCollider::Update()
    {
        auto * target = GetWarrantyParent<transform::Transform>();

        if (target == nullptr) return;

        for (auto & itr : m_target)
        {
            if (itr == nullptr)continue;
            auto * target2 = itr->GetWarrantyParent<transform::Transform>();
            if (target2 == nullptr)continue;
            m_collision = Sphere(target, target2, m_scale);
        }
    }

    void SphereCollider::Draw(LPDIRECT3DDEVICE9 device)
    {
        device;
    }

    float SphereCollider::GetSize() 
    {
        return m_scale;
    }

    void SphereCollider::SetSize(float scale)
    { 
        m_scale = scale; 
    }
}
_MSLIB_END