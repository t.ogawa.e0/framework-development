//==========================================================================
// デバイス[Device.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Device.h"

_MSLIB_BEGIN

namespace device
{
    Device::Device()
    {
        m_lpd3d = nullptr;
        m_lpd3ddevice = nullptr;
        m_hwnd = nullptr;
        m_ratio = 1.0f;
        m_adapter = D3DADAPTER_DEFAULT;
        SetComponentName("Device");

    }
    Device::~Device()
    {
        this->Release();
    }

    //==========================================================================
    /**
    @brief 初期化
    @return 失敗時に true が返ります
    */
    void Device::Init()
    {
        // Direct3D9インターフェース取得
        m_lpd3d = Direct3DCreate9(D3D_SDK_VERSION);
    }

    //==========================================================================
    /**
    @brief 解放
    */
    void Device::Release()
    {
        AllDestroyComponent();
        //デバイスの開放
        if (m_lpd3ddevice != nullptr)
        {
            m_lpd3ddevice->Release();
            m_lpd3ddevice = nullptr;
        }

        //Direct3Dオブジェクトの開放
        if (m_lpd3d != nullptr)
        {
            m_lpd3d->Release();
            m_lpd3d = nullptr;
        }
        m_d3dspMode.clear();
    }

    //==========================================================================
    /**
    @brief ウィンドウモードの生成
    @param size [in] ウィンドウサイズ
    @param Mode [in] ウィンドウモード true でフルスクリーン
    */
    bool Device::CreateWindowMode(const int2 & size, bool Mode)
    {
        int nCount = 0;
        float2 w1percent;
        float2 wErrorpercent;
        D3DDISPLAYMODE d3dspMode;
        int num = 0;

        ZeroMemory(&m_d3dpp, sizeof(m_d3dpp));

        // 現在のディスプレイモードを取得する
        if (FAILED(m_lpd3d->GetAdapterDisplayMode(m_adapter, &m_d3dpm))) return true;

        // 何種類ディスプレイモードあるかを調べる
        num = m_lpd3d->GetAdapterModeCount(m_adapter, m_d3dpm.Format);

        // ディスプレイモードを調べる
        for (int i = 0; i < num; i++)
        {
            // ディスプレイモードの検索
            if (FAILED(m_lpd3d->EnumAdapterModes(m_adapter, m_d3dpm.Format, i, &d3dspMode)))
            {
                ErrorMessage("ディスプレイモードの検索に失敗しました");
                return true;
            }

            // ディスプレイモードを記憶
            m_d3dspMode.emplace_back(d3dspMode);
        }

        // 最高のバッファを登録
        m_d3dpm = m_d3dspMode[m_d3dspMode.size() - 1];

        if (Mode == false)
        {
            m_d3dpm.Width = size.x;
            m_d3dpm.Height = size.y;
            m_d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
            m_d3dpp.Windowed = true; // ウィンドウモード
        }
        else if (Mode == true)
        {
            //フルスクリーンのとき、リフレッシュレートを変えられる
            m_d3dpp.FullScreen_RefreshRateInHz = m_d3dpm.RefreshRate;
            m_d3dpp.Windowed = false; // ウィンドウモード
        }

        // 推奨ウィンドウサイズの1%のウィンドウサイズを算出
        w1percent.x = (float)((float)1920 / 100.0f);
        w1percent.y = (float)((float)1080 / 100.0f);
        nCount = 0;

        // 動作環境の最大解像度になるまで繰り返す
        for (;;)
        {
            wErrorpercent.x += w1percent.x;
            wErrorpercent.y += w1percent.y;
            nCount++;
            if (m_d3dpm.Width <= wErrorpercent.x)break;
            if (m_d3dpm.Height <= wErrorpercent.y)break;
        }

        // 補正値算出
        m_ratio = nCount * 0.01f;

        //デバイスのプレゼンテーションパラメータ(デバイスの設定値)
        m_d3dpp.BackBufferWidth = m_d3dpm.Width;		//バックバッファの幅
        m_d3dpp.BackBufferHeight = m_d3dpm.Height;	//バックバッファの高さ
        m_d3dpp.BackBufferFormat = m_d3dpm.Format;    //バックバッファフォーマット
        m_d3dpp.BackBufferCount = 1;					//バッファの数
        m_d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;		//垂直？
        m_d3dpp.EnableAutoDepthStencil = TRUE;			//3Dの描画に必要
        //m_d3dpp.AutoDepthStencilFormat = D3DFMT_D16;	// 16Bit Zバッファ作成
        m_d3dpp.AutoDepthStencilFormat = D3DFMT_D24S8;	// 8Bit ステンシルシャドウ
        m_d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_DEFAULT;

        // ウィンドウサイズ確定
        m_WindowsSize.x = m_d3dpm.Width;
        m_WindowsSize.y = m_d3dpm.Height;

        return false;
    }

    //==========================================================================
    /**
    @brief デバイスの生成
    @return 失敗時に true が返ります
    */
    bool Device::CreateDevice()
    {
        HRESULT	hr;

        //デバイスオブジェクトを生成
        //[デバイス作成制御]<描画>と<頂点処理>
        hr = m_lpd3d->CreateDevice(m_adapter,
            D3DDEVTYPE_HAL,
            m_hwnd,
            D3DCREATE_HARDWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
            &m_d3dpp,
            &m_lpd3ddevice);
        if (FAILED(hr)) {
            hr = m_lpd3d->CreateDevice(m_adapter,
                D3DDEVTYPE_HAL,
                m_hwnd,
                D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
                &m_d3dpp,
                &m_lpd3ddevice);
            if (FAILED(hr)) {
                hr = m_lpd3d->CreateDevice(m_adapter,
                    D3DDEVTYPE_REF,
                    m_hwnd,
                    D3DCREATE_SOFTWARE_VERTEXPROCESSING | D3DCREATE_MULTITHREADED,
                    &m_d3dpp,
                    &m_lpd3ddevice);
            }
        }
        if (FAILED(hr))
        {
            ErrorMessage("デバイスの生成に失敗しました。");
            return true;
        }

        // ビューポートの設定
        D3DVIEWPORT9 vp;
        vp.X = 0;
        vp.Y = 0;
        vp.Width = m_d3dpp.BackBufferWidth;
        vp.Height = m_d3dpp.BackBufferHeight;
        vp.MinZ = 0.0f;
        vp.MaxZ = 1.0f;

        if (FAILED(m_lpd3ddevice->SetViewport(&vp)))
        {
            ErrorMessage("ビューポートの設定に失敗しました。");
            return true;
        }

        // レンダーステートの設定(α値の設定)
        m_lpd3ddevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE); // αブレンドを許可
        m_lpd3ddevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA); // αソースカラーの設定
        m_lpd3ddevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA); // α

        // テクスチャステージの設定
        m_lpd3ddevice->SetTextureStageState(0, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
        m_lpd3ddevice->SetTextureStageState(0, D3DTSS_ALPHAARG2, D3DTA_DIFFUSE);
        m_lpd3ddevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

        // サンプラーステートの設定(UV値を変えると増えるようになる)
        // WRAP...		反復する
        // CLAMP...　	引き伸ばす
        // MIRROR...　	鏡状態
        m_lpd3ddevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
        m_lpd3ddevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
        m_lpd3ddevice->SetSamplerState(0, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

        // フィルタリング
        m_lpd3ddevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR); // 小さくなった時に白枠
        m_lpd3ddevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR); // 常に白枠
        m_lpd3ddevice->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR); // 元のサイズより小さい時綺麗にする

        m_lpd3ddevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW); // 背景をカリング

        return false;
    }

    //==========================================================================
    /**
    @brief デバイスの習得
    @return デバイス
    */
    LPDIRECT3DDEVICE9 Device::GetD3DDevice() const
    {
        return m_lpd3ddevice;
    }

    //==========================================================================
    /**
    @brief デバイスの習得
    @return デバイス
    */
    D3DPRESENT_PARAMETERS & Device::Getd3dpp()
    {
        return m_d3dpp;
    }

    //==========================================================================
    /**
    @brief ウィンドウサイズの習得
    @return ウィンドウサイズ
    */
    const int2 & Device::GetWindowsSize() const
    {
        return m_WindowsSize;
    }

    //==========================================================================
    /**
    @brief ウィンドウハンドルの入力
    @param hWnd [in] ウィンドウハンドル
    */
    void Device::SetHwnd(HWND hWnd)
    {
        m_hwnd = hWnd;
    }

    //==========================================================================
    /**
    @brief ウィンドウハンドルの取得
    @return ウィンドウハンドル
    */
    HWND Device::GetHwnd() const
    {
        return m_hwnd;
    }

    //==========================================================================
    /**
    @brief 推奨ウィンドウサイズとの誤差の取得
    @return 誤差割合が返ります
    */
    float Device::ScreenBuffScale(void) const
    {
        return m_ratio;
    }

    //==========================================================================
    /**
    @brief 描画開始
    @return 描画可能な際に true が返ります
    */
    bool Device::DrawBegin(void)
    {
        //m_lpd3ddevice->Clear(0, nullptr, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER), D3DCOLOR_RGBA(50, 50, 50, 255), 1.0f, 0);
        m_lpd3ddevice->Clear(0, nullptr, (D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER | D3DCLEAR_STENCIL), D3DCOLOR_RGBA(50, 50, 50, 255), 1.0f, 0);

        //Direct3Dによる描画の開始
        if (SUCCEEDED(m_lpd3ddevice->BeginScene()))
        {
            return true;
        }
        return false;
    }

    //==========================================================================
    /**
    @brief 描画終了
    @param SourceRect [in]
    @param DestRect [in]
    @param hDestWindowOverride [in]
    @param DirtyRegion [in]
    @return Component Object Model defines, and macros
    */
    HRESULT __stdcall Device::DrawEnd(const RECT * SourceRect, const RECT * DestRect, HWND hDestWindowOverride, const RGNDATA * DirtyRegion)
    {
        //Direct3Dによる描画の終了
        m_lpd3ddevice->EndScene();
        return m_lpd3ddevice->Present(SourceRect, DestRect, hDestWindowOverride, DirtyRegion);
    }

    //==========================================================================
    /**
    @brief 描画終了
    @return Component Object Model defines, and macros
    */
    HRESULT __stdcall Device::DrawEnd(void)
    {
        return DrawEnd(nullptr, nullptr, nullptr, nullptr);
    }
}

_MSLIB_END