//==========================================================================
// バーテックス[DX9_Vertex3D.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : DX9_VERTEX_3D
// Content: バーテックス
//
//==========================================================================
class DX9_VERTEX_3D // バーテックス
{
protected:
    struct VERTEX_4
	{
		D3DXVECTOR3 pos; // 座標
		D3DXVECTOR3 Normal; // 法線
		D3DCOLOR color;  // 色
		D3DXVECTOR2 Tex; // 頂点
	};

    struct VERTEX_3
	{
		D3DXVECTOR4 pos; // 座標
		D3DCOLOR color;  // 色
		D3DXVECTOR2 Tex; // 頂点
	};

    struct VERTEX_2
	{
		D3DXVECTOR3 pos; // 座標変換が必要
		D3DCOLOR color; // ポリゴンの色
	};
protected:
    const DWORD FVF_VERTEX_4 = (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_DIFFUSE | D3DFVF_TEX1);
    const DWORD FVF_VERTEX_3 = (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1);
    const DWORD FVF_VERTEX_2 = (D3DFVF_XYZ | D3DFVF_DIFFUSE);
};

_MSLIB_END