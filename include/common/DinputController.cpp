//==========================================================================
// コントローラー[DinputController.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "DinputController.h"

_MSLIB_BEGIN

namespace dinput_controller
{
    Stick::Stick()
    {
        m_LeftRight = (LONG)0;
        m_UpUnder = (LONG)0;
    }

    Stick::Stick(const Stick & obj)
    {
        *this = obj;
    }

    Stick::~Stick()
    {
    }

    Stick & Stick::operator=(const Stick & obj)
    {
        m_LeftRight = obj.m_LeftRight;
        m_UpUnder = obj.m_UpUnder;
        return *this;
    }

    Controller::Controller()
    {
        SetComponentName("Controller");
    }

    Controller::~Controller()
    {
    }

    //==========================================================================
    /**
    @brief 初期化
    @param hInstance [in] インスタンスハンドル
    @param hWnd [in] ウィンドウハンドル
    @return 失敗時に true が返ります
    */
    bool Controller::Init(HINSTANCE hInstance, HWND hWnd)
    {
        HRESULT hr;
        DIDEVCAPS diDevCaps; // デバイス機能

        if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_DInput, nullptr)))
        {
            MessageBox(hWnd, "DirectInputオブジェクトが作れませんでした", "警告", MB_OK);
            return true;
        }

        hr = m_DInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, nullptr, DIEDFL_FORCEFEEDBACK | DIEDFL_ATTACHEDONLY);
        if (FAILED(hr) || m_DIDevice == nullptr)
        {
            m_DInput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, nullptr, DIEDFL_ATTACHEDONLY);
        }

        if (m_DIDevice != nullptr)
        {
            if (FAILED(m_DIDevice->SetDataFormat(&c_dfDIJoystick)))
            {
                MessageBox(hWnd, "コントローラーの初期化に失敗.", "警告", MB_OK);
                return true;
            }

            if (FAILED(m_DIDevice->SetCooperativeLevel(hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND)))
            {
                MessageBox(hWnd, "協調モードを設定できません", "警告", MB_OK);
                return true;
            }

            diDevCaps.dwSize = sizeof(DIDEVCAPS);
            if (FAILED(m_DIDevice->GetCapabilities(&diDevCaps)))
            {
                MessageBox(hWnd, "コントローラー機能を作成できません", "警告", MB_OK);
                return true;
            }

            if (FAILED(m_DIDevice->EnumObjects(EnumAxesCallback, (void*)hWnd, DIDFT_AXIS)))
            {
                MessageBox(hWnd, "プロパティを設定できません", "警告", MB_OK);
                return true;
            }

            if (FAILED(m_DIDevice->Poll()))
            {
                hr = m_DIDevice->Acquire();
                while (hr == DIERR_INPUTLOST)
                {
                    hr = m_DIDevice->Acquire();
                }
            }
        }

        return false;
    }

    //==========================================================================
    /**
    @brief 更新
    */
    void Controller::Update(void)
    {
        DIJOYSTATE aState;
        BYTE aPOVState[(int)ControllerKey::MAX];

        if (m_DIDevice == nullptr)
        {
            return;
        }

        if (SUCCEEDED(m_DIDevice->GetDeviceState(sizeof(aState), &aState)))
        {
            // ボタン
            for (int i = 0; i < (int)sizeof(DIJOYSTATE::rgbButtons); i++)
            {
                // トリガー・リリース情報を生成
                m_StateTrigger[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & aState.rgbButtons[i];
                m_StateRelease[i] = (m_State.rgbButtons[i] ^ aState.rgbButtons[i]) & m_State.rgbButtons[i];

                // リピート情報を生成
                if (aState.rgbButtons[i])
                {
                    if (m_StateRepeatCnt[i] < 20)
                    {
                        m_StateRepeatCnt[i]++;
                        if (m_StateRepeatCnt[i] == 1 || m_StateRepeatCnt[i] >= 20)
                        {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                            m_StateRepeat[i] = aState.rgbButtons[i];
                        }
                        else
                        {
                            m_StateRepeat[i] = 0;
                        }
                    }
                }
                else
                {
                    m_StateRepeatCnt[i] = 0;
                    m_StateRepeat[i] = 0;
                }
                // プレス情報を保存
                m_State.rgbButtons[i] = aState.rgbButtons[i];
            }

            // 方向キー
            for (int i = 0; i < (int)ControllerKey::MAX; i++)
            {
                // 入力情報生成
                if (((aState.rgdwPOV[0] / (DWORD)4500) + (DWORD)1) == (DWORD)(i + 1))
                {
                    aPOVState[i] = 0x80;
                }
                else
                {
                    aPOVState[i] = 0x00;
                }

                // トリガー・リリース情報を生成
                m_POVTrigger[i] = (m_POVState[i] ^ aPOVState[i]) & aPOVState[i];
                m_POVRelease[i] = (m_POVState[i] ^ aPOVState[i]) & m_POVState[i];

                // リピート情報を生成
                if (aPOVState[i])
                {
                    if (m_POVRepeatCnt[i] < 20)
                    {
                        m_POVRepeatCnt[i]++;
                        if (m_POVRepeatCnt[i] == 1 || m_POVRepeatCnt[i] >= 20)
                        {// 押し始めた最初のフレーム、または一定時間経過したらリピート情報ON
                            m_POVRepeat[i] = aPOVState[i];
                        }
                        else
                        {
                            m_POVRepeat[i] = 0x00;
                        }
                    }
                }
                else
                {
                    m_POVRepeatCnt[i] = 0x00;
                    m_POVRepeat[i] = 0x00;
                }
                // プレス情報を保存
            }

            m_State.lX = aState.lX;
            m_State.lY = aState.lY;
            m_State.lZ = aState.lZ;
            m_State.lRx = aState.lRx;
            m_State.lRy = aState.lRy;
            m_State.lRz = aState.lRz;
        }
        else
        {
            // アクセス権を取得
            m_DIDevice->Acquire();
        }
    }

    //------------------------------------------------------------------------------
    // ジョイスティックのコールバック
    BOOL CALLBACK Controller::EnumJoysticksCallback(const DIDEVICEINSTANCE *pdidInstance, void *pContext)
    {
        DIDEVCAPS diDevCaps;			// デバイス情報
        auto *pThis = reinterpret_cast<Controller*>(pContext);

        if (pThis == nullptr)
        {
            return DIENUM_CONTINUE; // 列挙を続ける
        }

        // ジョイスティック用デバイスオブジェクトを作成
        if (FAILED(pThis->m_DInput->CreateDevice(pdidInstance->guidInstance, &pThis->m_DIDevice, nullptr)))
        {
            return DIENUM_CONTINUE; // 列挙を続ける
        }

        // ジョイスティックの能力を調べる
        diDevCaps.dwSize = sizeof(DIDEVCAPS);
        if (FAILED(pThis->m_DIDevice->GetCapabilities(&diDevCaps)))
        {
            if (pThis->m_DIDevice)
            {
                pThis->m_DIDevice->Release();
                pThis->m_DIDevice = nullptr;
            }
            return DIENUM_CONTINUE;		// 列挙を続ける
        }

        return DIENUM_STOP; // このデバイスを使うので列挙を終了する
    }

    //------------------------------------------------------------------------------
    // 軸のコールバック
    BOOL CALLBACK Controller::EnumAxesCallback(const DIDEVICEOBJECTINSTANCE *pdidoi, void *pContext)
    {
        auto *pThis = reinterpret_cast<Controller*>(pContext);

        // 軸の値の範囲を設定（-1000〜1000）
        DIPROPRANGE diprg;
        ZeroMemory(&diprg, sizeof(diprg));
        diprg.diph.dwSize = sizeof(diprg);
        diprg.diph.dwHeaderSize = sizeof(diprg.diph);
        diprg.diph.dwObj = pdidoi->dwType;
        diprg.diph.dwHow = DIPH_BYID;
        diprg.lMin = -1000;
        diprg.lMax = +1000;

        if (FAILED(pThis->m_DIDevice->SetProperty(DIPROP_RANGE, &diprg.diph)))
        {
            return DIENUM_STOP;
        }

        return DIENUM_CONTINUE;
    }

    //==========================================================================
    /**
    @brief 左スティックの状態取得
    @return ステックの情報
    */
    Stick Controller::LeftStick(void)
    {
        return stick(m_State.lX, m_State.lY);
    }

    //==========================================================================
    /**
    @brief 右スティックの状態取得
    @return ステックの情報
    */
    Stick Controller::RightStick(void)
    {
        return stick(m_State.lZ, m_State.lRz);
    }

    //==========================================================================
    /**
    @brief L2ボタンの状態取得
    @return L2ボタンの情報
    */
    LONG Controller::L2(void)
    {
        if (m_DIDevice == nullptr)
        {
            return (LONG)0;
        }

        return m_State.lRx;
    }

    //==========================================================================
    /**
    @brief R2ボタンの状態取得
    @return R2ボタンの情報
    */
    LONG Controller::R2(void)
    {
        if (m_DIDevice == nullptr)
        {
            return (LONG)0;
        }

        return m_State.lRy;
    }

    //==========================================================================
    /**
    @brief PS4 方向キー
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::DirectionKey(ControllerKey Key)
    {
        int Num = 0;

        if (m_DIDevice == nullptr)
        {
            return false;
        }

        if ((int)(sizeof(m_State.rgdwPOV) / 4) < Num)
        {
            return false;
        }

        if ((LONG)4294967295 == m_State.rgdwPOV[Num])
        {
            return false;
        }

        if ((ControllerKey)((m_State.rgdwPOV[Num] / (LONG)4500) + (LONG)1) == Key)
        {
            return true;
        }

        return false;
    }

    //==========================================================================
    /**
    @brief PS4 入力誤差修正
    @param Set [in] 入力
    @return 値が返ります
    */
    LONG Controller::Adjustment(LONG Set)
    {
        LONG Stick;

        if (Set >= (LONG)50 || -(LONG)50 >= Set)
        {
            Stick = Set;
        }
        else
        {
            Stick = (LONG)0;
        }

        return Stick;
    }

    //==========================================================================
    /**
    @brief PS4 ステック入力制御
    @param Stick1 [in] 入力
    @param Stick2 [in] 入力
    @return 値が返ります
    */
    Stick Controller::stick(LONG Stick1, LONG Stick2)
    {
        Stick _stick;

        if (m_DIDevice != nullptr)
        {
            _stick.m_LeftRight = Adjustment(Stick1);
            _stick.m_UpUnder = Adjustment(Stick2);
        }
        else
        {
            _stick.m_LeftRight = (LONG)0;
            _stick.m_UpUnder = (LONG)0;
        }

        return _stick;
    }

    //==========================================================================
    /**
    @brief PS4 ボタン プレス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Press(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_State.rgbButtons[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 ボタン トリガー
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Trigger(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateTrigger[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 ボタン リピート
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Repeat(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateRelease[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 ボタン リリ−ス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Release(Button key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_StateRelease[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 方向キー プレス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Press(ControllerKey key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_POVState[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 方向キー トリガー
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Trigger(ControllerKey key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_POVTrigger[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 方向キー リピート
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Repeat(ControllerKey key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_POVRepeat[(int)key] & 0x80) ? true : false;
    }

    //==========================================================================
    /**
    @brief PS4 方向キー リリ−ス
    @param key [in] 使用キーの指定
    @return 入力されている場合 true が返ります
    */
    bool Controller::Release(ControllerKey key)
    {
        if (m_DIDevice == nullptr)
        {
            return false;
        }

        return (m_POVRelease[(int)key] & 0x80) ? true : false;
    }
}
_MSLIB_END