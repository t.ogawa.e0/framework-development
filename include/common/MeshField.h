//==========================================================================
// メッシュフィールド [MeshField.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <unordered_map>
#include <string>
#include <vector>
#include <list>

#include "mslib.hpp"
#include "mslib_struct.h"
#include "Component.h"
#include "vector_wrapper.h"
#include "DX9_Vertex3D.h"
#include "CreateBuffer.h"
#include "IReference.h"

_MSLIB_BEGIN

namespace meshfield
{
    //==========================================================================
    // メッシュ情報
    //==========================================================================
    struct MeshInfo
    {
        int NumMeshX = 0; // 面の数
        int NumMeshZ = 0; // 面の数
        int VertexOverlap = 0; // 重複する頂点数
        int	NumXVertexWey = 0; // 視覚化されている1列の頂点数
        int	NumZVertex = 0; // 基礎頂点数
        int	NumXVertex = 0; // 基礎頂点数
        int	NumMeshVertex = 0; // 視覚化されている全体の頂点数
        int	MaxPrimitive = 0; // プリミティブ数
        int	MaxIndex = 0; // 最大Index数
        int	MaxVertex = 0; // 最大頂点数
        int BoostMeshX = 0; // 面の数を倍した値
        int BoostMeshZ = 0; // 面の数を倍した値
    };

    //==========================================================================
    // 配列番号
    //==========================================================================
    struct FieldID
    {
        int x = 0, z = 0; // 配列番号
    };

    //==========================================================================
    // フィールド情報
    //==========================================================================
    struct FieldInfo : private  DX9_VERTEX_3D
    {
        std::string m_NameID; // IDネーム
        VERTEX_4 vertex;
        intColor m_Color = intColor(255, 255, 255, 255); // 色
    };

    using Field = wrapper::vector<wrapper::vector<FieldInfo>>;

    class MeshFieldLoader;
    class CreateMeshField;

    //==========================================================================
    //
    // class  : MeshFieldData
    // Content: フィールドデータ
    //
    //==========================================================================
    class MeshFieldData : public ireference::ReferenceData<create_buffer::Buffer>
    {
    public:
        MeshFieldData();
        ~MeshFieldData();
    public:        
        MeshInfo Info; // 情報
        Field FieldData; // フィールド
        std::unordered_map<std::string, FieldInfo*> MismatchMainData; // 基礎データのmap
        std::unordered_map<std::string, FieldInfo> MismatchSubData; // y軸に変更の入ったmap
        std::vector<int> GroupID; // GroupID
        wrapper::vector<wrapper::vector<FieldID>> ArrayID; // 空間分割ID
        MeshFieldLoader * LoaderPtr = nullptr; // 読み込み元のポインタ
        CreateMeshField * CreatePtr = nullptr; // 生成元のポインタ
        std::string tag;
    };

    //==========================================================================
    //
    // class  : MeshFieldReference
    // Content: 参照用
    //
    //==========================================================================
    class MeshFieldReference : public ireference::ReferenceOperator<MeshFieldData>
    {
    public:
        MeshFieldReference();
        ~MeshFieldReference();
        using ReferenceOperator::ReferenceOperator;
        using ReferenceOperator::operator=;
        using ReferenceOperator::operator&;
        using ReferenceOperator::operator->;
        virtual void Release() override final;
        const MeshInfo & Info();
        const std::unordered_map<std::string, FieldInfo*> & MismatchMainData();
        const std::unordered_map<std::string, FieldInfo> & MismatchSubData();
        const std::vector<int> & GroupID();
        wrapper::vector<wrapper::vector<FieldID>> & ArrayID();
        Field & FieldData();
        const std::string & Tag();
    };

    //==========================================================================
    //
    // class  : Common
    // Content: メッシュフィールド共通処理
    //
    //==========================================================================
    class Common : public component::Component, public create_buffer::CreateBuffer, public DX9_VERTEX_3D
    {
    public:
        Common();
        virtual ~Common();
    public:
        /**
        @brief リサイズ
        @param data [in] ポリゴン情報
        @param x [in] 横幅
        @param z [in] 奥行
        @return 失敗時 true が返ります
        */
        void Resize(MeshFieldReference data, int x, int z);
    protected:
        /**
        @brief 破棄
        @param data [in] 破棄対象
        */
        void Destroy(MeshFieldData * data);
    private:
        /**
        @brief メッシュ情報の生成
        @param info [out] インデックス情報の取得
        @param numX [in] X軸への幅
        @param numZ [in] Z軸への幅
        @param buff [in] 何分岐設定
        */
        void CreateMeshInfo(MeshInfo & info, int x, int z, int buff);

        /**
        @brief 空間分割の生成
        @param data [in] メッシュ情報
        @param buff [in] 分割範囲
        */
        void CreateSpaceDivision(MeshFieldReference & data, int buff);

        /**
        @brief バッファの生成
        @param Input [in] フィールドデータ
        @param data [in] メッシュ情報
        */
        void CreateBuffer(Field & Input, MeshFieldReference & data);

        /**
        @brief データの整合性
        @param Input [in] メッシュの情報
        @param data [in] メッシュ情報
        */
        void ConsistencyCheck(Field & Input, MeshFieldReference & data);

        /**
        @brief ロックアンロック系処理
        @param data [in] セットするデータ
        */
        void LockUnlock(MeshFieldReference & data);

        /**
        @brief 法線の生成
        @param Input [in] セットするパラメータ
        */
        void SetNormal(Field & Input);

        /**
        @brief バーテックス情報の生成
        @param Output [out] バーテックス情報
        @param Input [in] セットするパラメータ
        */
        void CreateVertex(VERTEX_4 * Output, Field & Input);

        /**
        @brief インデックス情報の生成
        @param out [out] インデックス情報の取得
        @param data [in] メッシュデータ
        */
        void CreateIndex(LPWORD * out, const MeshInfo & data);
    protected:
        std::unordered_map<std::string, MeshFieldData> m_data; // データ
        LPDIRECT3DDEVICE9 m_device; // デバイス
        HWND m_hwnd; // ウィンドウハンドル
    };

    //==========================================================================
    //
    // class  : CreateMeshField
    // Content: メッシュフィールドの生成
    //
    //==========================================================================
    class CreateMeshField : public Common
    {
    private:
        // コピー禁止 (C++11)
        CreateMeshField(const CreateMeshField &) = delete;
        CreateMeshField &operator=(const CreateMeshField &) = delete;
        CreateMeshField &operator=(CreateMeshField&&) = delete;
    public:
        CreateMeshField();
        CreateMeshField(LPDIRECT3DDEVICE9 device, HWND hWnd);
        ~CreateMeshField();
        /**
        @brief フィールドを生成する。
        */
        MeshFieldReference Create(int x, int z);

        /**
        @brief フィールドを破棄する。
        @param data [in] データ
        */
        void Delete(MeshFieldData * data);
    };

    //==========================================================================
    //
    // class  : MeshFieldLoader
    // Content: メッシュフィールドの読み込み
    //
    //==========================================================================
    class MeshFieldLoader : public Common
    {
    private:
        // コピー禁止 (C++11)
        MeshFieldLoader(const MeshFieldLoader &) = delete;
        MeshFieldLoader &operator=(const MeshFieldLoader &) = delete;
        MeshFieldLoader &operator=(MeshFieldLoader&&) = delete;
    public:
        MeshFieldLoader();
        MeshFieldLoader(LPDIRECT3DDEVICE9 device, HWND hWnd);
        ~MeshFieldLoader();
        /**
        @brief フィールドを読み込む。
        @param path [in] 読み込みパス
        @return フィールドのポインタ
        */
        MeshFieldReference Load(const std::string & path);

        /**
        @brief フィールドを破棄する。
        @param data [in] データ
        */
        void Unload(MeshFieldData * data);
    };

    //==========================================================================
    //
    // class  : MeshFieldSaver
    // Content: メッシュフィールドの保存
    //
    //==========================================================================
    class MeshFieldSaver : public component::Component
    {
    private:
        // コピー禁止 (C++11)
        MeshFieldSaver(const MeshFieldSaver &) = delete;
        MeshFieldSaver &operator=(const MeshFieldSaver &) = delete;
        MeshFieldSaver &operator=(MeshFieldSaver&&) = delete;
    public:
        MeshFieldSaver();
        ~MeshFieldSaver();
        /**
        @brief フィールドを保存。
        @param path [in] 保存名
        @param data [in] 保存データ
        */
        void Save(const std::string & path, const MeshFieldReference & data);
    };

    //==========================================================================
    //
    // class  : SetMeshField
    // Content: メッシュフィールド登録クラス
    //
    //==========================================================================
    class SetMeshField
    {
    public:
        SetMeshField();
        virtual ~SetMeshField();

        /**
        @brief メッシュフィールドの登録
        @param data [in] メッシュフィールド
        */
        void SetMeshFieldData(const MeshFieldReference & data);

        /**
        @brief メッシュフィールドの取得
        */
        MeshFieldReference & GetMeshFieldData();
    protected:
        MeshFieldReference m_MeshFieldData; // メッシュフィールド
    };

    const std::string create_key(int x, int z);
}

_MSLIB_END