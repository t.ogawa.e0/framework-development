//==========================================================================
// 重力 [Gravity.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <d3d9.h>
#include <d3dx9.h>
#include "mslib.hpp"
#include "FunctionComponent.h"

_MSLIB_BEGIN

namespace gravity
{
    //==========================================================================
    //
    // class  : Gravity
    // Content: 重力処理
    //
    //=========================================================================
    class Gravity : public function::FunctionComponent
    {
    public:
        Gravity();
        ~Gravity();
        // 更新
        void Update() override;
        bool Trigger();
    private:
        D3DXVECTOR3 m_position;
        float m_power; // パワー
        bool m_flag;
    };
}

_MSLIB_END