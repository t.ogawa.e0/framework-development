//==========================================================================
// 画像の品質調整[SetSampler.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"

_MSLIB_BEGIN

//==========================================================================
//
// class  : SetSampler
// Content: 画像の品質を調整するクラス
//
//==========================================================================
class SetSampler
{
protected:
    SetSampler();
	~SetSampler();

    /**
    @brief 初期値
    @param pDevice [in] デバイス
    */
	static void SamplerFitteringNONE(LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief ぼや
    @param pDevice [in] デバイス
    */
    static void SamplerFitteringLINEAR(LPDIRECT3DDEVICE9 pDevice);

    /**
    @brief グラフィカル
    @param pDevice [in] デバイス
    */
    static void SamplerFitteringGraphical(LPDIRECT3DDEVICE9 pDevice);
};

_MSLIB_END