//==========================================================================
// 仮想コントローラー [VirtualController.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// mslib
//==========================================================================
#include "mslib.hpp"
#include "FunctionComponent.h"
#include "Transform.h"

_MSLIB_BEGIN

namespace controller
{
    class VirtualController : public function::FunctionComponent
    {
    public:
        VirtualController();
        ~VirtualController();

        /**
        @brief 更新(自動)
        */
        void Update()override;

        /**
        @brief 入力
        @param position [in] 方向
        @param DirectionVector [in] 向きベクトル
        */
        void Input(const D3DXVECTOR3 & position, const D3DXVECTOR3 &DirectionVector);

        /**
        @brief ワールドマトリクス
        */
        const D3DXMATRIX & GetWorldMatrix();

        /**
        @brief ローカルマトリクス
        */
        const D3DXMATRIX & GetLocalMatrix();
    protected:
        transform::Transform m_virtual;
        bool m_input;
    };
}

_MSLIB_END