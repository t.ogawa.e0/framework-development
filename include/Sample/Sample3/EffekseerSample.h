//==========================================================================
// EffekseerSample [EffekseerSample.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Sample3
{
    //==========================================================================
    //
    // class  : EffekseerSample
    // Content: EffekseerSample
    //
    //==========================================================================
    class EffekseerSample : public mslib::ObjectManager
    {
    public:
		EffekseerSample();
        ~EffekseerSample();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    private:
        Effekseer::Handle m_handle = -1;
        mslib::object::Effekseer * m_obj;
    };
}

