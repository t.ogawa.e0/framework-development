//==========================================================================
// テストプログラム [Scene.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "Scene.h"
#include "Editor/common/EditorCamera.h"
#include "Editor/common/EditorGrid.h"
#include "resource_list.h"
#include "config.h"

namespace Sample1
{

	Scene::Scene() : BaseScene("Sample1", SceneLevel::__Sample1__)
	{
		AddComponent<Editor::EditorCamera>();
		AddComponent<C3DXInput>();
		AddComponent<C3DCamera>();
		AddComponent<C3DGridData>();
		AddComponent<C3DMesh>();
		AddComponent<C3DCubeData>();
		AddComponent<C3DSphere>();
		AddComponent<C3DXmodel>();
		AddComponent<C3DBillboard>();
		AddComponent<C2DData>();
		AddComponent<C2DAnimData>();
		AddComponent<C3DText>();
		AddComponent<C3DFog>();
	}

	Scene::~Scene()
	{
	}

	//==========================================================================
	//
	// class  : CTest
	// Content: テスト用
	//
	//==========================================================================

	//==========================================================================
	//
	// class  : C2DData
	// Content: 2D描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C2DData::Init(void)
	{
		auto * obj = Sprite()->Create();
		obj->SetTextureData(this->GetTextureLoader()->Load(RESOURCE_NowLoading_DDS));

		for (int i = 0; i < Sprite()->Size(); i++)
		{
			Sprite()->Get(i)->SetComponentName("Sprite" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C2DData::Update(void)
	{
	}

	//==========================================================================
	// デバッグ
	void C2DData::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C2DAnimData
	// Content: 2Dアニメーション描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C2DAnimData::Init(void)
	{
		auto * obj = SpriteAnimation()->Create();

		obj->SetTextureData(this->GetTextureLoader()->Load(RESOURCE_NowLoading_DDS));
		obj->SetAnimationData(0, 60, 5, 5);

		for (int i = 0; i < SpriteAnimation()->Size(); i++)
		{
			SpriteAnimation()->Get(i)->SetComponentName("SpriteAnimation" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C2DAnimData::Update(void)
	{
		auto * obj = SpriteAnimation()->Get(0);

		obj->PlayAnimation(true);
		obj->AddAnimationCounter(1);
	}

	//==========================================================================
	// デバッグ
	void C2DAnimData::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DGridData
	// Content: グリッド描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DGridData::Init(void)
	{
		// グリッドの初期化 引数はグリッドのサイズ
		Grid_()->Init(20);
	}

	//==========================================================================
	// 更新
	void C3DGridData::Update(void)
	{
	}

	//==========================================================================
	// デバッグ
	void C3DGridData::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DCubeData
	// Content: キューブの描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DCubeData::Init(void)
	{
		auto * obj1 = Cube()->Create();
		auto * obj2 = Cube()->Create();

		obj1->AddPosition(-5, 0, 0);
		obj2->AddPosition(5, 0, 0);

		obj1->SetCubeData(GetCreateCube()->Create());
		obj2->SetCubeData(GetCreateCube()->Create());

		obj1->SetTextureData(GetTextureLoader()->Load(""));
		obj2->SetTextureData(GetTextureLoader()->Load(""));

		for (int i = 0; i < Cube()->Size(); i++)
		{
			Cube()->Get(i)->SetComponentName("Cube" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C3DCubeData::Update(void)
	{
		auto * obj1 = Cube()->Get(0);
		auto * obj2 = Cube()->Get(1);

		const auto radian = mslib::ToRadian(1.0f);
		obj1->AddRotation(radian, radian, radian);
		obj2->AddRotation(radian, radian, radian);
	}

	//==========================================================================
	// デバッグ
	void C3DCubeData::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DBillboard
	// Content: ビルボードの描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DBillboard::Init(void)
	{
		auto bill = Billboard()->Create();
		auto billanim = BillboardAnimation()->Create();
		bill->SetPosition(-2, 5, 0);
		billanim->SetPosition(2, 5, 0);

		bill->SetTextureData(GetTextureLoader()->Load(RESOURCE_NowLoading_DDS));
		bill->SetBillboardData(GetCreateBillboard()->Create());

		billanim->SetTextureData(GetTextureLoader()->Load(RESOURCE_NowLoading_DDS));
		billanim->SetBillboardData(GetCreateBillboard()->Create());
		billanim->SetAnimationData(0, 60, 5, 5);
		billanim->PlayAnimation(true);

		for (int i = 0; i < Billboard()->Size(); i++)
		{
			Billboard()->Get(i)->SetComponentName("Billboard" + std::to_string(i));
		}
		for (int i = 0; i < BillboardAnimation()->Size(); i++)
		{
			BillboardAnimation()->Get(i)->SetComponentName("BillboardAnimation" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C3DBillboard::Update(void)
	{
		auto bill = Billboard()->Get(0);
		auto billanim = BillboardAnimation()->Get(0);
		auto view = GetParent()->GetComponent<C3DCamera>()->Camera()->Get(0)->GetCamera()->GetViewMatrix();

		bill->SetMatrixView(view);
		billanim->SetMatrixView(view);
		billanim->AddAnimationCounter(1);
	}

	//==========================================================================
	// デバッグ
	void C3DBillboard::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DMesh
	// Content: メッシュの描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DMesh::Init(void)
	{
		auto * mesh = Mesh()->Create();

		mesh->SetMeshData(GetCreateMesh()->Create(1, 1));
		mesh->SetTextureData(GetTextureLoader()->Load(""));
		mesh->AddPosition(0, 0, -5);

		for (int i = 0; i < Mesh()->Size(); i++)
		{
			Mesh()->Get(i)->SetComponentName("Mesh" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C3DMesh::Update(void)
	{
	}

	//==========================================================================
	// デバッグ
	void C3DMesh::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DSphere
	// Content: 球体の描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DSphere::Init(void)
	{
		auto * obj = Sphere()->Create();

		obj->SetSphereData(GetCreateSphere()->Create(18));
		obj->SetTextureData(GetTextureLoader()->Load(""));
		obj->AddPosition(0, 0, 5);

		for (int i = 0; i < Sphere()->Size(); i++)
		{
			Sphere()->Get(i)->SetComponentName("Sphere" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C3DSphere::Update(void)
	{
	}

	//==========================================================================
	// デバッグ
	void C3DSphere::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DXmodel
	// Content: Xモデルの描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DXmodel::Init(void)
	{
		//==========================================================================
		// 可視化オブジェクト
		//==========================================================================
		auto * obj = XModel()->Create();

		obj->SetXModelData(GetXModelLoader()->Load(RESOURCE_Box_x));

		//==========================================================================
		// 不可視化視化オブジェクト
		//==========================================================================
		Object()->Create();

		//==========================================================================
		//==========================================================================
		m_XInputObj = GetParent()->GetComponent<C3DXInput>();
		m_CameraObj = GetParent()->GetComponent<C3DCamera>();

		for (int i = 0; i < XModel()->Size(); i++)
		{
			XModel()->Get(i)->SetComponentName("XModel" + std::to_string(i));
		}
	}

	//==========================================================================
	// 更新
	void C3DXmodel::Update(void)
	{
		auto *pXInput = (C3DXInput*)m_XInputObj;
		auto *pCamera = (C3DCamera*)m_CameraObj;
		auto * obj = Object()->Get(0);
		auto * xmodel = XModel()->Get(0);

		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_A))
		{
			obj->AddPosition(-0.05f, 0, 0);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_D))
		{
			obj->AddPosition(0.05f, 0, 0);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_W))
		{
			obj->AddPosition(0, 0, 0.05f);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_S))
		{
			obj->AddPosition(0, 0, -0.05f);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Q))
		{
			obj->AddRotation(-0.05f, 0, 0);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_E))
		{
			obj->AddRotation(0.05f, 0, 0);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_R))
		{
			obj->AddRotation(0, -0.05f, 0);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_F))
		{
			obj->AddRotation(0, 0.05f, 0);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Z))
		{
			obj->AddRotation(0, 0, 0.05f);
		}
		if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_C))
		{
			obj->AddRotation(0, 0, -0.05f);
		}
		// コントローラーのチェック
		if (mslib::nullptr_check(pXInput))
		{
			if (pXInput->GetXInput()->Check(0))
			{
				D3DXVECTOR3 vecRight;
				float f_buf = 0.1f; // 速度バフ

				if (pXInput->GetXInput()->AnalogL(0, vecRight))
				{
					obj->AddPosition(vecRight.x*f_buf, 0, vecRight.z*f_buf);
				}
			}
		}

		if (mslib::nullptr_check(pCamera))
		{
			auto v_vec_rot = pCamera->Camera()->Get(0)->GetVector().front;
			v_vec_rot.y = 0.0f;
			obj->AddRotationX(v_vec_rot, 0.5f);

			// サードパーソン 不可視化オブジェクトを渡す
			pCamera->SetTarget(0, obj, { 0.5f,0.5f,0.0f });
		}

		// 向き補正
		for (int i = 0; i < 5; i++)
		{
			xmodel->LockOn(*obj, 1.0f);
		}

		// 可視化オブジェクトと不可視化オブジェクトとの座標統合
		xmodel->SetPosition(*obj->GetPosition());
	}

	//==========================================================================
	// デバッグ
	void C3DXmodel::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DText
	// Content: テキストの描画
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DText::Init(void)
	{
		Text_()->Init(GetDevice()->GetWindowsSize().x, GetDevice()->GetWindowsSize().y, RESOURCE_meiryo_ttc);

	}

	//==========================================================================
	// 更新
	void C3DText::Update(void)
	{
	}

	//==========================================================================
	// デバッグ
	void C3DText::Debug(void)
	{
	}

	//==========================================================================
	//
	// class  : C3DCamera
	// Content: カメラの設定
	//
	//==========================================================================

	//==========================================================================
	// 初期化
	void C3DCamera::Init(void)
	{
		D3DXVECTOR3 Eye = D3DXVECTOR3(0.0f, 3.0f, -3.0f); // 注視点
		D3DXVECTOR3 At = D3DXVECTOR3(0.0f, 3.0f, 0.0f); // カメラ座標

		Camera()->Create()->Init(Eye, At);
		Object()->Create()->SetMatrixType(mslib::object::MatrixType::World);
		m_XInputObj = GetParent()->GetComponent<C3DXInput>();
	}

	//==========================================================================
	// 更新
	void C3DCamera::Update(void)
	{
		auto *pXInput = (C3DXInput*)m_XInputObj;

		// コントローラーのチェック
		if (pXInput->GetXInput()->Check(0) == true)
		{
			D3DXVECTOR3 vecRight;
			if (pXInput->GetXInput()->AnalogR(0, vecRight))
			{
				Camera()->Get(0)->AddViewRotation({ vecRight.x*0.05f, vecRight.z*0.02f, 0 });
			}
		}
		if (GetDInputKeyboard()->Press(KeyboardButton::KEY_UP))
		{
			Camera()->Get(0)->DistanceFromView(0.05f);
		}
		else if (GetDInputKeyboard()->Press(KeyboardButton::KEY_DOWN))
		{
			Camera()->Get(0)->DistanceFromView(-0.05f);
		}

		if (GetDInputKeyboard()->Press(KeyboardButton::KEY_LEFT))
		{
			Camera()->Get(0)->AddViewRotation({ 0.05f, 0, 0 });
		}
		else if (GetDInputKeyboard()->Press(KeyboardButton::KEY_RIGHT))
		{
			Camera()->Get(0)->AddViewRotation({ -0.05f, 0, 0 });
		}
		if (GetDInputKeyboard()->Press(KeyboardButton::KEY_LEFT) && GetDInputKeyboard()->Press(KeyboardButton::KEY_SPACE))
		{
			Camera()->Get(0)->AddViewRotation({ 0, 0, 0.05f });
		}
		else if (GetDInputKeyboard()->Press(KeyboardButton::KEY_RIGHT) && GetDInputKeyboard()->Press(KeyboardButton::KEY_SPACE))
		{
			Camera()->Get(0)->AddViewRotation({ 0, 0, -0.05f });
		}

		else
		{
			//m_camera.RotViewX(0.0025f);
		}
	}

	//==========================================================================
	// デバッグ
	void C3DCamera::Debug(void)
	{
	}

	void C3DCamera::SetTarget(int ID, mslib::transform::Transform *Input, D3DXVECTOR3 move_pos)
	{
		auto * pObj = Object()->Get(0);
		auto * p_camera = Camera()->Get(ID);

		if (mslib::nullptr_check(p_camera) && mslib::nullptr_check(pObj))
		{
			// オブジェクトの複製
			auto obj1 = *(mslib::transform::Transform*)pObj;
			auto obj2 = *Input;

			obj2.AddPosition(move_pos);

			obj1.SetPosition(obj1.GetPosition()->x, obj2.GetPosition()->y, obj1.GetPosition()->z);

			// 距離を速度に
			float f_speed = mslib::collider::Distance(&obj1, &obj2)*0.04f;

			// 対象に追従
			for (int i = 0; i < 10; i++)
			{
				pObj->AddRotationX(pObj->ToSee(obj2), 1.0f);
			}
			pObj->AddPosition(0, 0, f_speed);

			pObj->SetPosition(*pObj->GetPosition());
			p_camera->SetAt(*pObj->GetPosition());
			p_camera->SetEye(*pObj->GetPosition());
		}
	}

	//==========================================================================
	// 初期化
	void C3DXInput::Init(void)
	{
		XInput_()->Init(1);

	}

	//==========================================================================
	// 更新
	void C3DXInput::Update(void)
	{
		XInput_()->Update();
	}

	//==========================================================================
	// デバッグ
	void C3DXInput::Debug(void)
	{
	}

	mslib::xinput::XInput * C3DXInput::GetXInput(void)
	{
		return XInput_();
	}

	//==========================================================================
	// 初期化
	void C3DFog::Init(void)
	{
		Fog_()->Init({ 255,255,255,255 });

	}

	//==========================================================================
	// 更新
	void C3DFog::Update(void)
	{
	}

	//==========================================================================
	// デバッグ
	void C3DFog::Debug(void)
	{
	}
}