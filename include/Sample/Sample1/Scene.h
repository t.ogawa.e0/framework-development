//==========================================================================
// テストプログラム [Scene.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h" 

namespace Sample1
{
	//==========================================================================
	//
	// class  : CXInput
	// Content: CXInputの使い方
	//
	//==========================================================================
	class C3DXInput : public mslib::ObjectManager
	{
	public:
		C3DXInput() :ObjectManager("C3DXInput") {
		}
		~C3DXInput() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;

		mslib::xinput::XInput * GetXInput(void);
	private:
	};

	//==========================================================================
	//
	// class  : C3DCamera
	// Content: カメラの設定
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DCamera : public mslib::ObjectManager
	{
	public:
		C3DCamera() :ObjectManager("C3DCamera") {
		}
		~C3DCamera() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;

		// ターゲットをセット
		void SetTarget(int ID, mslib::transform::Transform *Input, D3DXVECTOR3 move_pos);

	private:
		void *m_XInputObj; // アクセス
	};

	//==========================================================================
	//
	// class  : C3DText
	// Content: テキストの描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DText : public mslib::ObjectManager
	{
	public:
		C3DText() :ObjectManager("テキストの描画") {
		}
		~C3DText() {}

		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
	};

	//==========================================================================
	//
	// class  : C3DXmodel
	// Content: Xモデルの描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DXmodel : public mslib::ObjectManager
	{
	public:
		enum class ObjectType
		{
			Begin,
			Visualization = Begin, // 可視化
			Invisible, // 不可視化
			End,
		};
	public:
		C3DXmodel() :ObjectManager("Xモデルの描画") {
			m_XInputObj = nullptr;
			m_CameraObj = nullptr;
		}
		~C3DXmodel() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
		void *m_XInputObj; // アクセス
		void *m_CameraObj;
	};

	//==========================================================================
	//
	// class  : C3DSphere
	// Content: 球体の描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DSphere : public mslib::ObjectManager
	{
	public:
		C3DSphere() :ObjectManager("球体の描画") {
		}
		~C3DSphere() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
	};

	//==========================================================================
	//
	// class  : C3DMesh
	// Content: メッシュの描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DMesh : public mslib::ObjectManager
	{
	public:
		C3DMesh() :ObjectManager("メッシュの描画") {
		}
		~C3DMesh() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
	};

	//==========================================================================
	//
	// class  : C3DBillboard
	// Content: ビルボードの描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DBillboard : public mslib::ObjectManager
	{
	public:
		C3DBillboard() :ObjectManager("ビルボードの描画") {
		}
		~C3DBillboard() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
		int m_animcount; // アニメーションのカウンタ
	};

	//==========================================================================
	//
	// class  : C3DCubeData
	// Content: キューブの描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DCubeData : public mslib::ObjectManager
	{
	public:
		C3DCubeData() :ObjectManager("キューブの描画") {
		}
		~C3DCubeData() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
	};

	//==========================================================================
	//
	// class  : C3DGridData
	// Content: グリッド描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C3DGridData : public mslib::ObjectManager
	{
	public:
		C3DGridData() :ObjectManager("グリッド描画") {
		}
		~C3DGridData() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
	};

	//==========================================================================
	//
	// class  : C2DAnimData
	// Content: 2Dアニメーション描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C2DAnimData : public mslib::ObjectManager
	{
	public:
		C2DAnimData() :ObjectManager("2Dアニメーション描画") {
		}
		~C2DAnimData() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
		int m_animcount; // アニメーションカウンタ
	};

	//==========================================================================
	//
	// class  : C2DData
	// Content: 2D描画
	//
	//==========================================================================
	// CObject クラスにこのクラスを認識させるための継承
	class C2DData : public mslib::ObjectManager
	{
	public:
		C2DData() :ObjectManager("2D描画") {
		}
		~C2DData() {}
		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;
	private:
	};

	//==========================================================================
	//
	// class  : C3DFog
	// Content: フォグ
	//
	//==========================================================================
	class C3DFog : public mslib::ObjectManager
	{
	public:
		C3DFog() : ObjectManager("C3DFog") {
		}
		~C3DFog() {
		}

		// 初期化
		void Init(void)override;

		// 更新
		void Update(void)override;

		// デバッグ
		void Debug(void)override;

	private:

	};

	//==========================================================================
	//
	// class  : Scene
	// Content: テスト用
	//
	//==========================================================================
	// CBaseScene CSceneManagerに認識させるための継承
	class Scene : public mslib::scene_manager::BaseScene
	{
	public:
		Scene();
		~Scene();
	};
}