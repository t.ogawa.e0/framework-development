//==========================================================================
// コリダーのチェック [CheckCollider.h]
// author: tatsuya ogawa
//==========================================================================
#pragma once

//==========================================================================
// include
//==========================================================================
#include "dxlib.h"

namespace Sample2
{
    class CheckCollider : public mslib::ObjectManager
    {
    public:
        CheckCollider();
        ~CheckCollider();

        /**
        @brief 初期化
        */
        void Init() override;

        /**
        @brief 更新
        */
        void Update() override;

        /**
        @brief デバッグ
        */
        void Debug() override;
    private:
        void ControlObject(int label);
	private:
		bool m_init;
    };
}