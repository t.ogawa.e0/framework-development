//==========================================================================
// コリダーのチェック [CheckCollider.cpp]
// author: tatsuya ogawa
//==========================================================================
#include "CheckCollider.h"

namespace Sample2
{
    CheckCollider::CheckCollider() : ObjectManager("CheckCollider")
    {
		m_init = false;
    }

    CheckCollider::~CheckCollider()
    {
    }

    void CheckCollider::Init()
    {
		auto obj = Object()->Create();
        auto collider1 = obj->AddComponent<mslib::collider::DistanceCollider>();
		obj->AddPosition(0, 0, 2);

		obj = Object()->Create();
        auto collider2 = obj->AddComponent<mslib::collider::PlaneCollider>();
		obj->AddPosition(0, 0.1f, 0);
		obj->AddRotation(0, mslib::ToRadian(120.0f), 0);

		obj = Object()->Create();
        auto collider3 = obj->AddComponent<mslib::collider::DistanceCollider>();
		obj->AddPosition(3, 0, 0);

		obj = Object()->Create();
        auto collider4 = obj->AddComponent<mslib::collider::BoxCollider>();
		obj->AddPosition(-3, 0, 0);
		obj->AddRotation(mslib::ToRadian(45.0f), mslib::ToRadian(45.0f), 0);

        collider2->AddCollider(collider1);
        collider1->AddCollider(collider2);
        collider1->AddCollider(collider3);
        collider3->AddCollider(collider1);
        collider4->AddCollider(collider1);

		for (int i = 0; i < Object()->Size(); i++)
		{
			Object()->Get(i)->SetComponentName("Object" + std::to_string(i));
		}
	}

    void CheckCollider::Update()
    {
		mslib::renderer::Renderer::AllDebugActivity(true);

		auto obj = Object()->Get(1);

        ControlObject(0);
        obj->AddRotation(0, mslib::ToRadian(90.0f / 60.0f), 0);
    }

    void CheckCollider::Debug()
    {

    }
    void CheckCollider::ControlObject(int label)
    {
		auto obj = Object()->Get(label);
		if (obj == nullptr)return;

        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_A))
        {
            obj->AddPosition(-0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_D))
        {
            obj->AddPosition(0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_W))
        {
            obj->AddPosition(0, 0, 0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_S))
        {
            obj->AddPosition(0, 0, -0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Q))
        {
            obj->AddRotation(-0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_E))
        {
            obj->AddRotation(0.05f, 0, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_R))
        {
            obj->AddRotation(0, -0.05f, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_F))
        {
            obj->AddRotation(0, 0.05f, 0);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_Z))
        {
            obj->AddRotation(0, 0, 0.05f);
        }
        if (this->GetDInputKeyboard()->Press(KeyboardButton::KEY_C))
        {
            obj->AddRotation(0, 0, -0.05f);
        }
    }
}